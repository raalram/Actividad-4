//Maya ASCII 2023 scene
//Name: actividad.ma
//Last modified: Sun, Jan 22, 2023 12:29:10 AM
//Codeset: UTF-8
requires maya "2023";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiStandardSurface"
		 -nodeType "aiStandardHair" -nodeType "aiShadowMatte" "mtoa" "5.2.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202208031415-1dee56799d";
fileInfo "osv" "Mac OS X 10.16";
fileInfo "UUID" "3D892633-DF4D-83D5-24C0-E39D1D6A8CFE";
createNode transform -s -n "persp";
	rename -uid "FF3488AF-9645-2E7F-8E04-668526A8E065";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 779.15380353890419 218.33742663839394 5775.9525404729093 ;
	setAttr ".r" -type "double3" -1075.5489313240855 9367.842750984486 5.0165398932947185e-17 ;
	setAttr ".rp" -type "double3" -2.2737367544323206e-13 1.1368683772161603e-13 0 ;
	setAttr ".rpt" -type "double3" 5.4785046476548691e-14 -9.3422643700293982e-15 1.410218719862506e-13 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "F827DDC7-A64E-14D0-D153-0D932BE7116D";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 6204.1871422188069;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 325.01836116767669 6.0918445790036913e-06 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "9CF3ED76-9249-D0FB-A599-D3A05569334A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.0218035499214748 671.16521613177463 1506.7042037454803 ;
	setAttr ".rpt" -type "double3" -4.4275356867563063e-14 -4.0005036457862577e-14 1.1526472146719325e-14 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "17E2EF7A-D642-AA78-BBDF-1285B2A45575";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 1403.0362412964664;
	setAttr ".ow" 5461.2694908334206;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 2.0218035499214304 671.16521613177463 103.66796244901388 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "236067BD-D341-46F8-BFB0-1F93EE4B30CE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1195.4712191010187 409.46063702308669 -1099.360411673651 ;
	setAttr ".r" -type "double3" 0 89.999999999999972 0 ;
	setAttr ".rp" -type "double3" 0 -5.6843418860808015e-14 0 ;
	setAttr ".rpt" -type "double3" -5.6397190124629376e-17 1.3436432371732314e-15 4.2948846140876524e-15 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "2BAAF717-274C-60BC-8F17-6FB4B7ECD645";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1000000000008;
	setAttr ".ow" 6465.9425248781636;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 195.37121910101791 409.46063702308669 -1099.3604116736512 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "36F09A67-0C44-EE57-2D4F-29BEE2EAC05C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 195.37121910101794 1409.5606370230885 -1099.360411673651 ;
	setAttr ".r" -type "double3" -89.999999999999972 89.999999999999972 0 ;
	setAttr ".rpt" -type "double3" -3.5661433287255378e-14 -4.0378439077890445e-14 3.1972184617766365e-14 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "11BB54B0-C547-5CA4-31C3-11AADBC70533";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1000000000017;
	setAttr ".ow" 9215.9313729507467;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 195.37121910101769 409.46063702308675 -1099.360411673651 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "House1";
	rename -uid "67F6A6ED-9142-5977-AE32-F097EBA22ED5";
	setAttr ".t" -type "double3" -337.35681957613059 251.13749736613346 273.80646006715659 ;
	setAttr ".s" -type "double3" 402.61454521928425 315.94569750215248 261.82520538848411 ;
createNode mesh -n "House1Shape" -p "House1";
	rename -uid "DAF2C5EE-A545-B180-99DA-0C9861A80926";
	setAttr -k off ".v";
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5625 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -0.19991197 1.5543122e-15 
		0.33221546 0.19991197 4.4408921e-15 0.33221546 -0.19991197 1.5543122e-15 0.33221546 
		0.19991197 4.4408921e-15 0.33221546 -0.19991197 -4.4408921e-15 -0.33221546 0.19991197 
		-1.5543122e-15 -0.33221546 -0.19991197 -4.4408921e-15 -0.33221546 0.19991197 -1.5543122e-15 
		-0.33221546 3.3078525e-15 2.9976022e-15 0.33221546 3.3078525e-15 0.82222462 0.33221546 
		1.1615275e-14 0.82222462 -0.33221546 1.1615275e-14 -2.9976022e-15 -0.33221546;
createNode transform -n "HouseRoof";
	rename -uid "EDCCD3CD-9C4A-6BA6-74BE-20A2F5A1D44D";
	setAttr ".t" -type "double3" -319.61929146253658 651.40742537156507 284.11750322017872 ;
	setAttr ".s" -type "double3" 239.64769146080641 239.64769146080641 239.64769146080641 ;
createNode mesh -n "HouseRoofShape" -p "HouseRoof";
	rename -uid "C59BD109-4D4F-525A-9AB5-589CFA9FD923";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.1875 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -4.6629367e-15 3.1086245e-15 
		0.14700346 -1.5543122e-15 0 0.14700346 -4.6629367e-15 0 0.14700346 -1.5543122e-15 
		-3.1086245e-15 0.14700346 -4.6629367e-15 0 -0.14700346 -1.5543122e-15 -3.1086245e-15 
		-0.14700346 -4.6629367e-15 3.1086245e-15 -0.14700346 -1.5543122e-15 0 -0.14700346 
		-3.1086245e-15 0.41597036 4.5770526e-15 -4.6629367e-15 3.1086245e-15 4.5770526e-15 
		-1.5543122e-15 0 2.4093888e-15 0 0.41597036 2.4093888e-15 0 0 -2.1175824e-22 0 0 
		4.2351647e-22 0 0 0 0 0 0;
createNode transform -n "Floor";
	rename -uid "F97F459F-ED47-BE31-CDCA-A2B419AA47B1";
	setAttr ".t" -type "double3" 336.27952910362183 0 -206.61132432596347 ;
	setAttr ".s" -type "double3" 3045.2611723116893 1901.8091942419278 3045.2611723116893 ;
createNode mesh -n "FloorShape" -p "Floor";
	rename -uid "8EE76686-554F-8E28-FBFD-AE8E691F0900";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.6 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Human";
	rename -uid "3C64D654-054C-9140-BBE0-FA9704161142";
	setAttr ".t" -type "double3" 2056.0119608249056 623.80341584370069 1698.2827893582717 ;
	setAttr ".s" -type "double3" 40 175 40 ;
createNode mesh -n "HumanShape" -p "Human";
	rename -uid "3DE68D0B-C942-C859-2E11-B6B96DE8A025";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Wood1";
	rename -uid "9D469B77-9E4D-4300-2890-EE9BD88A9E90";
	setAttr ".t" -type "double3" 676.09768228312009 135.23564081725269 697.56469474897062 ;
	setAttr ".r" -type "double3" 0 0 46.073136433351316 ;
	setAttr ".s" -type "double3" 40 175 40 ;
createNode transform -n "transform12" -p "Wood1";
	rename -uid "C18F73A0-C043-766B-0BC1-2AA07AE2E538";
	setAttr ".v" no;
createNode mesh -n "Wood1Shape" -p "transform12";
	rename -uid "6739BE3B-124F-6DDB-5C53-8882F5E3A564";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Wood2";
	rename -uid "6AD1B3A8-1B44-97F2-6B7A-33ADB7A53121";
	setAttr ".t" -type "double3" 676.09768228312009 135.23564081725269 697.56469474897062 ;
	setAttr ".r" -type "double3" 0 0 -53.319270555142644 ;
	setAttr ".s" -type "double3" 40 175 40 ;
createNode transform -n "transform11" -p "Wood2";
	rename -uid "DBBF4991-0C4E-23C1-618F-F29595DB4A9F";
	setAttr ".v" no;
createNode mesh -n "Wood2Shape" -p "transform11";
	rename -uid "778E0759-424D-07E7-1D3D-E998EC2BB65B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Wood3";
	rename -uid "ECFE4304-1C45-D44B-8436-9CA6A7646500";
	setAttr ".t" -type "double3" 676.09768228312009 135.23564081725269 399.03418018060216 ;
	setAttr ".r" -type "double3" 0 0 -53.319270555142644 ;
	setAttr ".s" -type "double3" 40 175 40 ;
createNode transform -n "transform14" -p "Wood3";
	rename -uid "D6834F1D-E442-4C31-3C98-AB96DEE1F52D";
	setAttr ".v" no;
createNode mesh -n "Wood3Shape" -p "transform14";
	rename -uid "EC8873ED-244E-C2DF-7601-DC8BBF4056D5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Wood4";
	rename -uid "7D411755-2B47-0B2E-EAC2-8E83A0B5642C";
	setAttr ".t" -type "double3" 676.09768228312009 135.23564081725269 399.03418018060216 ;
	setAttr ".r" -type "double3" 0 0 46.073136433351316 ;
	setAttr ".s" -type "double3" 40 175 40 ;
createNode transform -n "transform13" -p "Wood4";
	rename -uid "23A2C433-3547-18D5-06DE-5D939A54CFFC";
	setAttr ".v" no;
createNode mesh -n "Wood4Shape" -p "transform13";
	rename -uid "2FB33993-B249-F6E0-E15E-A094215145FE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Trunk";
	rename -uid "BA2894F1-3346-5FCB-4B78-84939123DA72";
	setAttr ".t" -type "double3" 681.69476035009473 195.17551387317729 553.68967466639515 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 29.047237315396952 219.68520080842268 29.047237315396952 ;
createNode transform -n "transform17" -p "Trunk";
	rename -uid "7AFE4413-8245-8CCF-FEE2-DEAB76B04D0D";
	setAttr ".v" no;
createNode mesh -n "TrunkShape" -p "transform17";
	rename -uid "DF93CD2E-DB4E-C04F-ADD3-608EED612BB1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Cover1";
	rename -uid "7B030DC3-EB43-D621-2176-2095A2D50909";
	setAttr ".t" -type "double3" -334.18063478280152 392.53751349393127 768.87125361577318 ;
	setAttr ".r" -type "double3" 19.986222750972622 0 0 ;
	setAttr ".s" -type "double3" 616.37209484680125 570.95744184308489 375.61775882348667 ;
createNode mesh -n "CoverShape1" -p "Cover1";
	rename -uid "ABE605F2-2F4A-6C00-BCB3-ACA1C617E25E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column2";
	rename -uid "CE0CD782-1843-555B-276D-F4B2678C0E78";
	setAttr ".t" -type "double3" -37.053183652378863 170.27541601398588 898.60596284785959 ;
	setAttr ".s" -type "double3" 40 327.53119030330782 40 ;
createNode mesh -n "Column2Shape" -p "Column2";
	rename -uid "FD48B4CE-7545-39CD-B07F-C3B72CFBDE68";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  0 -2.9802322e-08 0 0 -2.9802322e-08 
		0 0 -2.9802322e-08 0 0 -2.9802322e-08 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column1";
	rename -uid "E54BE2D4-FD40-69D8-3942-218733D5708D";
	setAttr ".t" -type "double3" -578.86776997556763 179.50708359117328 898.60596284785959 ;
	setAttr ".s" -type "double3" 40 310.14398971858196 40 ;
createNode mesh -n "Column1Shape" -p "Column1";
	rename -uid "01EFC95E-B347-6E71-8D0F-D79C024F747B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  0 -2.9802322e-08 0 0 -2.9802322e-08 
		0 0 -2.9802322e-08 0 0 -2.9802322e-08 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Floor2";
	rename -uid "97160BB7-9144-DE0D-2C8E-6B9CD34671F6";
	setAttr ".t" -type "double3" 807.73995049244377 179.30503985447359 -503.84773494821559 ;
	setAttr ".s" -type "double3" 477.47851021514646 413.92065807875696 483.74548442154378 ;
createNode mesh -n "FloorShape2" -p "Floor2";
	rename -uid "B3F60E27-8C40-AC49-B808-DD8E6F575DCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape7" -p "Floor2";
	rename -uid "697C12CE-E947-FA97-2E52-2DB2E9B5182E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window1";
	rename -uid "EEB9228E-6C48-546C-6C0D-D8B0F658E543";
	setAttr ".t" -type "double3" -196.0508811891483 726.58096585459202 283.40813388028982 ;
	setAttr ".s" -type "double3" 41.151281224598755 115.98442169280271 140.78885205848553 ;
createNode mesh -n "WindowShape1" -p "Window1";
	rename -uid "B4017A3A-FB42-689E-28B2-3C84453166F8";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[18:23]" -type "float3"  -0.27199844 -1.1368684e-13 
		0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 
		0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 0;
createNode mesh -n "polySurfaceShape6" -p "Window1";
	rename -uid "EC34E6ED-8C43-B7F2-AA08-DA94D8E20D5F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -4.6629367e-15 3.1086245e-15 
		0.14700346 -1.5543122e-15 0 0.14700346 -4.6629367e-15 0 0.14700346 -1.5543122e-15 
		-3.1086245e-15 0.14700346 -4.6629367e-15 0 -0.14700346 -1.5543122e-15 -3.1086245e-15 
		-0.14700346 -4.6629367e-15 3.1086245e-15 -0.14700346 -1.5543122e-15 0 -0.14700346 
		-3.1086245e-15 0.41597036 4.5770526e-15 -4.6629367e-15 3.1086245e-15 4.5770526e-15 
		-1.5543122e-15 0 2.4093888e-15 0 0.41597036 2.4093888e-15 0 0 -2.1175824e-22 0 0 
		4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window3";
	rename -uid "65B45D8F-7541-7F24-B2D8-048786E2B155";
	setAttr ".t" -type "double3" -443.24370882842635 726.58096585459202 283.40813388028727 ;
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr ".s" -type "double3" 41.151281224598755 115.98442169280271 140.78885205848553 ;
createNode mesh -n "WindowShape3" -p "Window3";
	rename -uid "C8812A93-3E42-F7DB-A7BE-EC99681AC21E";
	setAttr -k off ".v";
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "f[0:3]" "f[5:7]" "f[9:21]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[8]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 3 "f[4]" "f[8]" "f[10:21]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 34 ".uvst[0].uvsp[0:33]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25
		 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[18:23]" -type "float3"  -0.27199844 -1.1368684e-13 
		0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 
		0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 0;
	setAttr -s 24 ".vt[0:23]"  -0.5 -0.49999905 0.64700317 0.5 -0.49999905 0.64700317
		 -0.5 0.5 0.64700317 0.5 0.5 0.64700317 -0.5 0.5 -0.64700365 0.5 0.5 -0.64700365 -0.5 -0.49999905 -0.64700365
		 0.5 -0.49999905 -0.64700365 -0.5 0.91597033 0 -0.5 -0.49999905 0 0.5 -0.49999905 0
		 0.5 0.91597033 0 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963 0.5 0.71216202 0 0.5 0.41593742 0.46074963
		 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963
		 0.5 0.71216202 0 0.5 0.41593742 0.46074963 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998;
	setAttr -s 44 ".ed[0:43]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 11 8 1 10 12 0
		 1 13 0 12 13 0 11 14 0 3 15 0 15 14 0 13 15 0 7 16 0 16 12 0 5 17 0 17 16 0 14 17 0
		 12 18 0 13 19 0 18 19 0 14 20 0 18 20 1 15 21 0 21 20 0 19 21 0 16 22 0 22 18 0 17 23 0
		 23 22 0 20 23 0;
	setAttr -s 22 -ch 88 ".fc[0:21]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 18 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -34 35 -38 -39
		mu 0 4 28 29 30 31
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -36 -41 -43 -44
		mu 0 4 30 29 32 33
		f 4 -19 15 -3 -13
		mu 0 4 15 20 5 4
		f 4 -15 19 21 -21
		mu 0 4 1 19 23 22
		f 4 -8 23 24 -23
		mu 0 4 21 3 25 24
		f 4 -6 20 25 -24
		mu 0 4 3 1 22 25
		f 4 -12 26 27 -20
		mu 0 4 19 10 26 23
		f 4 -10 28 29 -27
		mu 0 4 10 11 27 26
		f 4 -16 22 30 -29
		mu 0 4 11 21 24 27
		f 4 -22 31 33 -33
		mu 0 4 22 23 29 28
		f 4 -25 36 37 -35
		mu 0 4 24 25 31 30
		f 4 -26 32 38 -37
		mu 0 4 25 22 28 31
		f 4 -28 39 40 -32
		mu 0 4 23 26 32 29
		f 4 -30 41 42 -40
		mu 0 4 26 27 33 32
		f 4 -31 34 43 -42
		mu 0 4 27 24 30 33;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape6" -p "Window3";
	rename -uid "AD2A0207-4149-C983-8E5E-8089ED6EC906";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -4.6629367e-15 3.1086245e-15 
		0.14700346 -1.5543122e-15 0 0.14700346 -4.6629367e-15 0 0.14700346 -1.5543122e-15 
		-3.1086245e-15 0.14700346 -4.6629367e-15 0 -0.14700346 -1.5543122e-15 -3.1086245e-15 
		-0.14700346 -4.6629367e-15 3.1086245e-15 -0.14700346 -1.5543122e-15 0 -0.14700346 
		-3.1086245e-15 0.41597036 4.5770526e-15 -4.6629367e-15 3.1086245e-15 4.5770526e-15 
		-1.5543122e-15 0 2.4093888e-15 0 0.41597036 2.4093888e-15 0 0 -2.1175824e-22 0 0 
		4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Cover2";
	rename -uid "073B1D2C-8547-5ABD-27A8-6AA54F080C7E";
	setAttr ".t" -type "double3" 136.16348682856321 356.71615139943822 79.850821575805682 ;
	setAttr ".r" -type "double3" -66.277738569043152 89.853822828008731 -93.73603868000113 ;
	setAttr ".s" -type "double3" 717.77415367054641 577.00977406438312 379.59942768006681 ;
createNode mesh -n "CoverShape2" -p "Cover2";
	rename -uid "3A20E702-5E48-92D2-997E-54B14032D7D3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0.34008172 0.49865967 -0.18760395 
		-0.33693022 0.50144517 -0.1759626 0.34018591 -0.47076249 -0.18350807 -0.33682621 
		-0.46797717 -0.17186674 0.33693022 -0.50144517 0.1759626 -0.34008172 -0.49865967 
		0.18760395 0.33682621 0.46797717 0.17186674 -0.34018591 0.47076249 0.18350807;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window4";
	rename -uid "4A7CAC57-F244-9AAD-E829-BC8EAE068AC4";
	setAttr ".t" -type "double3" -335.62290481607363 531.8481392229312 648.18874605415624 ;
	setAttr ".r" -type "double3" 0 270 0 ;
	setAttr ".s" -type "double3" 41.151281224598755 115.98442169280271 140.78885205848553 ;
createNode mesh -n "polySurfaceShape6" -p "Window4";
	rename -uid "0D067D7D-1640-AAA9-F6FD-9C809010A6A9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -4.6629367e-15 3.1086245e-15 
		0.14700346 -1.5543122e-15 0 0.14700346 -4.6629367e-15 0 0.14700346 -1.5543122e-15 
		-3.1086245e-15 0.14700346 -4.6629367e-15 0 -0.14700346 -1.5543122e-15 -3.1086245e-15 
		-0.14700346 -4.6629367e-15 3.1086245e-15 -0.14700346 -1.5543122e-15 0 -0.14700346 
		-3.1086245e-15 0.41597036 4.5770526e-15 -4.6629367e-15 3.1086245e-15 4.5770526e-15 
		-1.5543122e-15 0 2.4093888e-15 0 0.41597036 2.4093888e-15 0 0 -2.1175824e-22 0 0 
		4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape35" -p "Window4";
	rename -uid "E93B0BE9-FA4F-62F2-EE48-1ABF0014E5C3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 3 "f[4]" "f[8]" "f[10:21]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 34 ".uvst[0].uvsp[0:33]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25
		 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt[18:23]" -type "float3"  -0.27199844 -1.1368684e-13 
		0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 
		0 -0.27199844 -1.1368684e-13 0 -0.27199844 -1.1368684e-13 0;
	setAttr -s 24 ".vt[0:23]"  -0.5 -0.49999905 0.64700317 0.5 -0.49999905 0.64700317
		 -0.5 0.5 0.64700317 0.5 0.5 0.64700317 -0.5 0.5 -0.64700365 0.5 0.5 -0.64700365 -0.5 -0.49999905 -0.64700365
		 0.5 -0.49999905 -0.64700365 -0.5 0.91597033 0 -0.5 -0.49999905 0 0.5 -0.49999905 0
		 0.5 0.91597033 0 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963 0.5 0.71216202 0 0.5 0.41593742 0.46074963
		 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963
		 0.5 0.71216202 0 0.5 0.41593742 0.46074963 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998;
	setAttr -s 44 ".ed[0:43]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 11 8 1 10 12 0
		 1 13 0 12 13 0 11 14 0 3 15 0 15 14 0 13 15 0 7 16 0 16 12 0 5 17 0 17 16 0 14 17 0
		 12 18 0 13 19 0 18 19 0 14 20 0 18 20 1 15 21 0 21 20 0 19 21 0 16 22 0 22 18 0 17 23 0
		 23 22 0 20 23 0;
	setAttr -s 22 -ch 88 ".fc[0:21]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 18 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -34 35 -38 -39
		mu 0 4 28 29 30 31
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -36 -41 -43 -44
		mu 0 4 30 29 32 33
		f 4 -19 15 -3 -13
		mu 0 4 15 20 5 4
		f 4 -15 19 21 -21
		mu 0 4 1 19 23 22
		f 4 -8 23 24 -23
		mu 0 4 21 3 25 24
		f 4 -6 20 25 -24
		mu 0 4 3 1 22 25
		f 4 -12 26 27 -20
		mu 0 4 19 10 26 23
		f 4 -10 28 29 -27
		mu 0 4 10 11 27 26
		f 4 -16 22 30 -29
		mu 0 4 11 21 24 27
		f 4 -22 31 33 -33
		mu 0 4 22 23 29 28
		f 4 -25 36 37 -35
		mu 0 4 24 25 31 30
		f 4 -26 32 38 -37
		mu 0 4 25 22 28 31
		f 4 -28 39 40 -32
		mu 0 4 23 26 32 29
		f 4 -30 41 42 -40
		mu 0 4 26 27 33 32
		f 4 -31 34 43 -42
		mu 0 4 27 24 30 33;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface11" -p "Window4";
	rename -uid "439147EE-054F-950F-710F-7B9DC798A676";
	setAttr ".t" -type "double3" 0.096429993656757543 -1.9603811626138534e-15 -4.1000789031009739e-16 ;
	setAttr ".s" -type "double3" 1.1549305116106057 1 1 ;
createNode mesh -n "polySurfaceShape36" -p "polySurface11";
	rename -uid "87363CD6-FC49-0A54-9A72-3F83C25887DA";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.8125 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface12" -p "Window4";
	rename -uid "DA091F69-4F47-92CA-DD88-1480DB57E073";
createNode mesh -n "polySurfaceShape37" -p "polySurface12";
	rename -uid "3CABFCD3-5947-2B9E-F688-C9A76324B225";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform27" -p "Window4";
	rename -uid "1E7EEF9F-2849-1427-3713-F2AB475F8779";
	setAttr ".v" no;
createNode mesh -n "WindowShape4" -p "transform27";
	rename -uid "A293C86D-994D-13F0-8205-1F8053B84ED6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Step";
	rename -uid "4DEBEAA3-CB4C-3EC0-26E2-B5BC104B9DAA";
	setAttr ".t" -type "double3" 140.0722091407109 64.785177665434247 80.980691396917749 ;
	setAttr ".s" -type "double3" 133.27905041129637 413.92065807875696 203.05965354103196 ;
	setAttr ".rp" -type "double3" -66.639529617904572 -11.957096375020777 -101.52982434752461 ;
	setAttr ".sp" -type "double3" -0.50000003310540064 -0.028887411492145659 -0.49999998806758839 ;
	setAttr ".spt" -type "double3" -66.139529584799178 -11.928208963528631 -101.02982435945702 ;
createNode mesh -n "StepShape" -p "Step";
	rename -uid "762B8185-A349-553C-708E-3690AC318168";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder4";
	rename -uid "3599F7BA-324C-EB8E-7836-01A4693B0B92";
	setAttr ".t" -type "double3" 811.88427906972947 82.066890061923473 -181.74409393314841 ;
	setAttr ".s" -type "double3" 116.12937881018271 243.14109066622404 42.025895478062473 ;
	setAttr ".rp" -type "double3" -66.639529617904572 -11.957096375020777 -101.52982434752461 ;
	setAttr ".sp" -type "double3" -0.50000003310540064 -0.028887411492145659 -0.49999998806758839 ;
	setAttr ".spt" -type "double3" -66.139529584799178 -11.928208963528631 -101.02982435945702 ;
createNode mesh -n "ladderShape4" -p "ladder4";
	rename -uid "BACC00F0-0647-2F4D-08A0-34B3F53F7B02";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window2";
	rename -uid "AC664C86-A949-F1B7-9D33-21857D2213D1";
	setAttr ".t" -type "double3" 83.080806411241255 291.0765633193248 423.29270593068026 ;
	setAttr ".s" -type "double3" 121.61503207645676 121.61503207645676 121.61503207645676 ;
createNode mesh -n "WindowShape2" -p "Window2";
	rename -uid "8970412E-7E4C-DB7B-CACE-69AAFF3091CB";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[12:15]" -type "float3"  -0.2231399 0 0.0043684109 
		-0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109;
createNode transform -n "Door";
	rename -uid "93AA170C-6E44-63EA-20DE-E49046532BBE";
	setAttr ".t" -type "double3" 85.283565923392615 162.89947746913111 83.555715375205224 ;
	setAttr ".s" -type "double3" 76.424989907374268 76.424989907374268 76.424989907374268 ;
createNode mesh -n "DoorShape" -p "Door";
	rename -uid "1FAF6222-7045-B940-8ECA-CE92324B555D";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  0.0053279363 -1.2193214 0.27215278 
		0.0055407132 -1.2193214 0.28302145 0.0053279363 1.2193214 0.27215278 0.0055407132 
		1.2193214 0.28302145 -0.0055407202 1.2193214 -0.28302187 -0.0053279456 1.2193214 
		-0.27215326 -0.0055407202 -1.2193214 -0.28302187 -0.0053279456 -1.2193214 -0.27215326 
		-0.0037733633 -0.87051374 -0.19274467 0.0039861379 -0.87051374 0.20361325 -0.0037733633 
		0.87051374 -0.19274467 0.0039861379 0.87051374 0.20361325 -0.2269132 -0.87051374 
		-0.18837628 -0.21915372 -0.87051374 0.20798166 -0.2269132 0.87051374 -0.18837628 
		-0.21915372 0.87051374 0.20798166;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof1";
	rename -uid "0A1ADE53-B049-C42A-DB17-AB91551EB10C";
	setAttr ".t" -type "double3" -343.23346510613493 313.83258768608607 687.94934527642272 ;
	setAttr ".s" -type "double3" 402.61454521928425 315.94569750215248 261.82520538848411 ;
	setAttr ".rp" -type "double3" 5.8766535826974371 408.65526631782018 -56.981205627985197 ;
	setAttr ".sp" -type "double3" 0.014596227713275289 1.2934351363181189 -0.21763071108237764 ;
	setAttr ".spt" -type "double3" 5.8620573549841621 407.36183118150205 -56.763574916902819 ;
createNode mesh -n "Roof1Shape" -p "Roof1";
	rename -uid "78262366-B544-0470-A384-2BBAF81E6928";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape27" -p "Roof1";
	rename -uid "E3B9D385-534A-FA07-3C30-2D9BE36F68CD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -0.2141806 0.87940609 -0.78853858 
		0.18498805 0.86568207 -0.84001404 -0.46751252 -0.2820667 -0.78091228 0.48683041 -0.43762618 
		-0.84910077 -0.43831992 -0.2820667 0.84764022 0.51602292 -0.43762618 0.77945173 -0.18498804 
		0.87940609 0.84001392 0.21418063 0.86568207 0.78853846 -0.01459618 1.9630818 -0.81427634 
		-0.01459618 0.82222462 -0.81427634 0.01459618 0.82222462 0.81427634 0.01459618 1.9630818 
		0.81427634;
	setAttr -s 12 ".vt[0:11]"  -0.81733882 -0.66964674 1.031907082 0.81733882 -0.66964674 1.031907082
		 -0.81733882 0.66964674 1.031907082 0.81733882 0.66964674 1.031907082 -0.81733882 0.66964674 -1.031907082
		 0.81733882 0.66964674 -1.031907082 -0.81733882 -0.66964674 -1.031907082 0.81733882 -0.66964674 -1.031907082
		 0 -0.66964674 1.031907082 0 0.66964674 1.031907082 0 0.66964674 -1.031907082 0 -0.66964674 -1.031907082;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 14 16 2
		f 4 1 17 -3 -7
		mu 0 4 2 16 17 4
		f 4 2 18 -4 -9
		mu 0 4 4 17 18 6
		f 4 3 19 -1 -11
		mu 0 4 6 18 15 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -17 12 5 -14
		mu 0 4 16 14 1 3
		f 4 -18 13 7 -15
		mu 0 4 17 16 3 5
		f 4 -19 14 9 -16
		mu 0 4 18 17 5 7
		f 4 -20 15 11 -13
		mu 0 4 15 18 7 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof3";
	rename -uid "B9EB99EE-1C4B-A249-5C25-9994001C9728";
	setAttr ".t" -type "double3" -189.26956505276166 703.49857228015583 281.56564607866164 ;
	setAttr ".s" -type "double3" 239.64769146080641 239.64769146080641 239.64769146080641 ;
createNode mesh -n "Roof3Shape" -p "Roof3";
	rename -uid "AE2D2B35-6442-D210-F452-A198BAB4AD40";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  0.31249639 0.51012319 0.14088564 
		-0.29705468 0.51012319 0.15281904 0.31962952 -0.42991272 0.50524825 -0.28992155 -0.42991272 
		0.51718074 0.29001588 -0.4243677 -0.51236284 -0.3195352 -0.4243677 -0.50043035 0.29705468 
		0.59808004 -0.15281904 -0.31249639 0.59808004 -0.14088564 0.3047756 0.41597036 -0.0059665903 
		0.3047756 1.1669729 -0.0059665903 -0.3047756 1.1669729 0.0059665903 -0.3047756 0.41597036 
		0.0059665903 0 0 -2.1175824e-22 0 0 4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof4";
	rename -uid "C44D2A57-7644-1556-C57C-D284AEA9FB5C";
	setAttr ".t" -type "double3" -456.65705694330245 703.49857228015583 286.80029213068474 ;
	setAttr ".s" -type "double3" 239.64769146080641 239.64769146080641 239.64769146080641 ;
createNode mesh -n "Roof4Shape" -p "Roof4";
	rename -uid "F4A67CEC-1649-7729-F97A-4FB3C50E4E3F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 2 ".clst";
	setAttr ".clst[0].clsn" -type "string" "SculptFreezeColorTemp";
	setAttr ".clst[1].clsn" -type "string" "SculptMaskColorTemp";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  0.31249639 0.51012319 0.14088564 
		-0.29705468 0.51012319 0.15281904 0.31962952 -0.42991272 0.50524825 -0.28992155 -0.42991272 
		0.51718074 0.29001588 -0.4243677 -0.51236284 -0.3195352 -0.4243677 -0.50043035 0.29705468 
		0.59808004 -0.15281904 -0.31249639 0.59808004 -0.14088564 0.30477548 0.41597047 -0.0059665879 
		0.3047756 1.1669729 -0.0059665903 -0.3047756 1.1669729 0.0059665903 -0.3047756 0.41597036 
		0.0059665903 0 0 -2.1175824e-22 0 0 4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".cfsc" yes;
createNode transform -n "Step1";
	rename -uid "57E28EC4-D449-4FFB-8795-838D13E908AA";
	setAttr ".t" -type "double3" 955.23284892678441 478.61745846431069 -305.27698429791099 ;
	setAttr ".s" -type "double3" 21.343523020031185 350.19301693656485 122.03118385653062 ;
	setAttr ".rp" -type "double3" -2.8421709430404007e-14 0 0 ;
	setAttr ".spt" -type "double3" -2.8421709430404007e-14 0 0 ;
createNode mesh -n "Step1Shape" -p "Step1";
	rename -uid "49DE8289-9B41-4E45-514D-CE997DFB90DE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Step2";
	rename -uid "60A552A5-AD45-734B-2277-04A0AD5DDCF0";
	setAttr ".t" -type "double3" -82.459855161893117 96.956596380033716 829.2150579210637 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 133.27905041129637 350.17657230262608 179.51383946036034 ;
	setAttr ".rp" -type "double3" -2.4158453015843406e-13 0 7.1054273576010019e-14 ;
	setAttr ".rpt" -type "double3" 34.890294729620308 0 -168.1693539654292 ;
	setAttr ".spt" -type "double3" -2.4158453015843406e-13 0 7.1054273576010019e-14 ;
createNode mesh -n "Step2Shape" -p "Step2";
	rename -uid "F0125C33-8E42-7B4A-5A39-389DBAA52325";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Door1";
	rename -uid "5A6E5D26-D841-B3CF-12BC-7C8AB6215DCE";
	setAttr ".t" -type "double3" -336.35417482692515 157.83298563845221 649.05171061045417 ;
	setAttr ".r" -type "double3" 0 270 0 ;
	setAttr ".s" -type "double3" 76.424989907374268 76.424989907374268 76.424989907374268 ;
createNode mesh -n "Door1Shape" -p "Door1";
	rename -uid "C4EBB68B-1945-C95D-D245-27ABA9E2FF28";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  0.0053279363 -1.2193214 0.27215278 
		0.0055407132 -1.2193214 0.28302145 0.0053279363 1.2193214 0.27215278 0.0055407132 
		1.2193214 0.28302145 -0.0055407202 1.2193214 -0.28302187 -0.0053279456 1.2193214 
		-0.27215326 -0.0055407202 -1.2193214 -0.28302187 -0.0053279456 -1.2193214 -0.27215326 
		-0.0037733633 -0.87051374 -0.19274467 0.0039861379 -0.87051374 0.20361325 -0.0037733633 
		0.87051374 -0.19274467 0.0039861379 0.87051374 0.20361325 -0.2269132 -0.87051374 
		-0.18837628 -0.2191537 -0.87051374 0.20798166 -0.2269132 0.87051374 -0.18837628 -0.2191537 
		0.87051374 0.20798166;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window5";
	rename -uid "A3BBBE5B-F34F-2E12-BFFF-908D2D0562B6";
	setAttr ".t" -type "double3" -163.70149336407701 215.64291692700039 648.6717178754069 ;
	setAttr ".r" -type "double3" 0 270 0 ;
	setAttr ".s" -type "double3" 121.61503207645676 121.61503207645676 121.61503207645676 ;
createNode mesh -n "WindowShape5" -p "Window5";
	rename -uid "7878BE5F-1A42-37FA-FCAA-0F8F5FBC6CD6";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[12:15]" -type "float3"  -0.2231399 0 0.0043684109 
		-0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window6";
	rename -uid "74439F2E-084E-D3FE-7BB4-54BDE219EC26";
	setAttr ".t" -type "double3" -560.8217555152587 215.64291692700039 643.99590625663575 ;
	setAttr ".r" -type "double3" 0 270 0 ;
	setAttr ".s" -type "double3" 121.61503207645676 121.61503207645676 121.61503207645676 ;
createNode mesh -n "WindowShape6" -p "Window6";
	rename -uid "E343582B-E940-6F08-20E9-CAA46D44A926";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[12:15]" -type "float3"  -0.2231399 0 0.0043684109 
		-0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window8";
	rename -uid "E58CABC7-AC49-C32C-1D47-FF829B165792";
	setAttr ".t" -type "double3" -760.58898170408031 268.49475525544744 130.2018896155742 ;
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr ".s" -type "double3" 121.61503207645676 164.78315929642986 178.23285577981071 ;
createNode mesh -n "WindowShape8" -p "Window8";
	rename -uid "F9045C27-9F4F-8819-7220-B5A4479C9492";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt";
	setAttr ".pt[12]" -type "float3" -0.2231399 0 0.0043684109 ;
	setAttr ".pt[13]" -type "float3" -0.2231399 0 0.0043684109 ;
	setAttr ".pt[14]" -type "float3" -0.2231399 0 0.0043684109 ;
	setAttr ".pt[15]" -type "float3" -0.2231399 0 0.0043684109 ;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window9";
	rename -uid "A927EA79-E44F-9109-4F5A-B78AF9BEE298";
	setAttr ".t" -type "double3" -758.35992178994354 268.49475525544744 441.36903129792955 ;
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr ".s" -type "double3" 121.61503207645676 164.78315929642986 178.23285577981071 ;
createNode mesh -n "WindowShape9" -p "Window9";
	rename -uid "741859CE-2B42-1B58-3FDD-279AF8DD2B10";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[12:15]" -type "float3"  -0.2231399 0 0.0043684109 
		-0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109 -0.2231399 0 0.0043684109;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column3";
	rename -uid "91A098E9-8141-F548-0A5E-9EB0CC7E8B1F";
	setAttr ".t" -type "double3" 635.87922914051921 232.37354291627059 -674.5772594785974 ;
	setAttr ".s" -type "double3" 29.047237315396952 219.68520080842268 29.047237315396952 ;
createNode mesh -n "Column3Shape" -p "Column3";
	rename -uid "F0D787D3-5A4D-223E-BA23-1384AD7CF497";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape33" -p "Column3";
	rename -uid "1F580989-A64C-25F5-8978-CA9DC924BB3D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column4";
	rename -uid "26A4C17B-7446-A928-114F-F288BDFF4FC8";
	setAttr ".t" -type "double3" 971.11138737936403 229.53664739735308 -334.79941022930439 ;
	setAttr ".s" -type "double3" 29.047237315396952 219.68520080842268 29.047237315396952 ;
createNode mesh -n "Column4Shape" -p "Column4";
	rename -uid "90973F3A-6946-783D-98AF-248215695113";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column5";
	rename -uid "19983BCF-C14C-F0C2-DF26-069C141E3795";
	setAttr ".t" -type "double3" 639.26987224280515 221.53875143166931 -335.09513982983287 ;
	setAttr ".s" -type "double3" 29.047237315396952 219.68520080842268 29.047237315396952 ;
createNode mesh -n "Column5Shape" -p "Column5";
	rename -uid "75658C3B-2247-0524-7381-4ABD90BBC9CF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape32" -p "Column5";
	rename -uid "CA81CFDB-B84E-8326-DEF4-EB9C30DE209C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column6";
	rename -uid "2F3F8B6E-8C41-0102-E280-49ADE7D9D37C";
	setAttr ".t" -type "double3" 969.91192731359365 225.5335418279783 -680.88002877499014 ;
	setAttr ".s" -type "double3" 29.047237315396952 219.68520080842268 29.047237315396952 ;
	setAttr ".rp" -type "double3" 1.7313501091240184e-06 -2.8421709430404007e-14 8.6567513094535327e-07 ;
	setAttr ".sp" -type "double3" 5.9604644775390625e-08 0 2.9802322387695312e-08 ;
	setAttr ".spt" -type "double3" 1.6717454643486277e-06 -2.8421709430404007e-14 8.3587280855765796e-07 ;
createNode mesh -n "Column6Shape" -p "Column6";
	rename -uid "8DBEEC0D-8749-59B0-1050-37861D905035";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape34" -p "Column6";
	rename -uid "FF29683E-1E44-2461-ADEC-87A2AEDD0E35";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof2";
	rename -uid "CF57B106-F141-1711-53C5-63AA412C01EE";
	setAttr ".t" -type "double3" -331.48016920769737 313.8325797286393 -140.33641227653459 ;
	setAttr ".s" -type "double3" 402.61454521928425 315.94569750215248 261.82520538848411 ;
	setAttr ".rp" -type "double3" -5.8766423157401295 408.65527427526695 56.981218184737727 ;
	setAttr ".sp" -type "double3" -0.014596199728798442 1.2934351615042421 0.21763075904090956 ;
	setAttr ".spt" -type "double3" -5.8620461160113315 407.3618391137627 56.763587425696819 ;
createNode mesh -n "Roof2Shape" -p "Roof2";
	rename -uid "4184B210-094F-F343-E28F-6E9503D62992";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape28" -p "Roof2";
	rename -uid "7A6FF50D-B34B-9998-7608-3BBFF77EF78B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.75 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -0.2141806 0.87940609 -0.78853858 
		0.18498805 0.86568207 -0.84001404 -0.46751252 -0.2820667 -0.78091228 0.48683041 -0.43762618 
		-0.84910077 -0.43831992 -0.2820667 0.84764022 0.51602292 -0.43762618 0.77945173 -0.18498804 
		0.87940609 0.84001392 0.21418063 0.86568207 0.78853846 -0.01459618 1.9630818 -0.81427634 
		-0.01459618 0.82222462 -0.81427634 0.01459618 0.82222462 0.81427634 0.01459618 1.9630818 
		0.81427634;
	setAttr -s 12 ".vt[0:11]"  -0.81733882 -0.66964674 1.031907082 0.81733882 -0.66964674 1.031907082
		 -0.81733882 0.66964674 1.031907082 0.81733882 0.66964674 1.031907082 -0.81733882 0.66964674 -1.031907082
		 0.81733882 0.66964674 -1.031907082 -0.81733882 -0.66964674 -1.031907082 0.81733882 -0.66964674 -1.031907082
		 0 -0.66964674 1.031907082 0 0.66964674 1.031907082 0 0.66964674 -1.031907082 0 -0.66964674 -1.031907082;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 14 16 2
		f 4 1 17 -3 -7
		mu 0 4 2 16 17 4
		f 4 2 18 -4 -9
		mu 0 4 4 17 18 6
		f 4 3 19 -1 -11
		mu 0 4 6 18 15 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -17 12 5 -14
		mu 0 4 16 14 1 3
		f 4 -18 13 7 -15
		mu 0 4 17 16 3 5
		f 4 -19 14 9 -16
		mu 0 4 18 17 5 7
		f 4 -20 15 11 -13
		mu 0 4 15 18 7 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof6";
	rename -uid "FD723533-254C-591B-580C-BFAB609541F1";
	setAttr ".t" -type "double3" 813.67110411627834 378.43480696230392 -214.27287874635817 ;
	setAttr ".s" -type "double3" 714.10337037605029 1058.3151515124987 134.93934932269772 ;
	setAttr ".rp" -type "double3" -2.2573309602094913e-05 382.59497819394608 -67.469674964579326 ;
	setAttr ".sp" -type "double3" -3.1610703388196271e-08 0.36151327668998989 -0.50000000224716667 ;
	setAttr ".spt" -type "double3" -2.2541698898706717e-05 382.23346491725607 -66.969674962332164 ;
createNode mesh -n "polySurfaceShape8" -p "Roof6";
	rename -uid "F1E7753F-F14B-1141-7A09-02A5F5AC705B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform3" -p "Roof6";
	rename -uid "7E783AB4-D143-154C-DB3D-A49FB5474729";
	setAttr ".v" no;
createNode mesh -n "Roof6Shape" -p "transform3";
	rename -uid "07C1C130-E046-DB47-4142-61A0AD4DEF4C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  0 0.015773587 0 0 0.015773587 
		0 0 0.015773587 0 0 0.015773587 0 0 0.015773587 0 0 0.015773587 0 0 0.015773587 0 
		0 0.015773587 0 -3.5390258e-08 0.39040071 1.4901161e-08 -2.8871e-08 0.39040074 1.4901161e-08 
		-2.8871e-08 0.39040074 1.4901161e-08 -3.5390258e-08 0.39040071 1.4901161e-08;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5 0 -0.028887421 0.5 0 0.028887421 0.5
		 0 0.028887421 -0.5 0 -0.028887421 -0.5;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 14 16 2
		f 4 1 17 -3 -7
		mu 0 4 2 16 17 4
		f 4 2 18 -4 -9
		mu 0 4 4 17 18 6
		f 4 3 19 -1 -11
		mu 0 4 6 18 15 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -17 12 5 -14
		mu 0 4 16 14 1 3
		f 4 -18 13 7 -15
		mu 0 4 17 16 3 5
		f 4 -19 14 9 -16
		mu 0 4 18 17 5 7
		f 4 -20 15 11 -13
		mu 0 4 15 18 7 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof7";
	rename -uid "FA71EB3D-6C44-88FB-90DC-12850980549E";
	setAttr ".t" -type "double3" 813.67110411627846 378.43480696230381 -775.12357985245842 ;
	setAttr ".s" -type "double3" 714.10337037605029 1058.3151515124987 134.93934932269772 ;
	setAttr ".rp" -type "double3" -2.2573309760657715e-05 382.59497819394619 67.469649188395948 ;
	setAttr ".sp" -type "double3" -3.1610703610240876e-08 0.36151327668999 0.49999981122664572 ;
	setAttr ".spt" -type "double3" -2.2541699057047474e-05 382.23346491725619 66.969649377169304 ;
createNode mesh -n "polySurfaceShape8" -p "Roof7";
	rename -uid "8CB4F458-554B-224F-8CF2-D2A7FCDED18B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform1" -p "Roof7";
	rename -uid "FD04F4E5-8F4F-9D0E-298C-069F89D15C99";
	setAttr ".v" no;
createNode mesh -n "Roof7Shape" -p "transform1";
	rename -uid "AE5D1894-BD47-DEED-53DD-48BA1DAED1C2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  0 0.015773587 0 0 0.015773587 
		0 0 0.015773587 0 0 0.015773587 0 0 0.015773587 0 0 0.015773587 0 0 0.015773587 0 
		0 0.015773587 0 -3.5390258e-08 0.39040071 1.4901161e-08 -2.8871e-08 0.39040074 1.4901161e-08 
		-2.8871e-08 0.39040074 1.4901161e-08 -3.5390258e-08 0.39040071 1.4901161e-08;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5 0 -0.028887421 0.5 0 0.028887421 0.5
		 0 0.028887421 -0.5 0 -0.028887421 -0.5;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 14 16 2
		f 4 1 17 -3 -7
		mu 0 4 2 16 17 4
		f 4 2 18 -4 -9
		mu 0 4 4 17 18 6
		f 4 3 19 -1 -11
		mu 0 4 6 18 15 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -17 12 5 -14
		mu 0 4 16 14 1 3
		f 4 -18 13 7 -15
		mu 0 4 17 16 3 5
		f 4 -19 14 9 -16
		mu 0 4 18 17 5 7
		f 4 -20 15 11 -13
		mu 0 4 15 18 7 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Wood5";
	rename -uid "365A27FB-5D4C-4E34-9901-1B8D6F2A3B06";
	setAttr ".t" -type "double3" 807.03801219992533 449.73868002598005 -335.92290269517338 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 40 419.83032467085008 78.004791119230418 ;
	setAttr ".rp" -type "double3" 2.0961010704922955e-13 2.2737367544323206e-13 -3.4106051316484809e-13 ;
	setAttr ".rpt" -type "double3" -4.3698378249246156e-13 -2.9828868212177822e-13 0 ;
	setAttr ".spt" -type "double3" 2.0961010704922955e-13 2.2737367544323206e-13 -3.4106051316484809e-13 ;
createNode mesh -n "Wood5Shape" -p "Wood5";
	rename -uid "AB76B3BE-C545-6C64-3F60-96AB6F79176B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof9";
	rename -uid "D7B3F3F5-A744-8B1F-2C57-65AF45A7B597";
	setAttr ".t" -type "double3" 813.67107574987733 519.77749732283792 -494.6982283070509 ;
	setAttr ".s" -type "double3" 539.84372435284581 425.91141214617977 425.91141214617977 ;
createNode transform -n "transform2" -p "Roof9";
	rename -uid "07215D80-9640-4B70-4587-AA8065458F08";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape1" -p "transform2";
	rename -uid "4AC34480-B643-52E5-8234-ECBA8A6D361E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0.56643772 0 ;
	setAttr ".pt[4]" -type "float3" 0 0.56643772 0 ;
createNode transform -n "Roof8";
	rename -uid "326BE4DF-CB40-C467-71D2-1F972FE7CF95";
	setAttr ".t" -type "double3" -0.5156314045052568 -75.782271999692853 -9.8152583576208485 ;
	setAttr ".s" -type "double3" 1 1 0.63001666562719316 ;
	setAttr ".rp" -type "double3" 813.67110411627868 593.36502782422008 -494.69822929940761 ;
	setAttr ".sp" -type "double3" 813.67110411627868 593.36502782422008 -494.69822929940761 ;
createNode transform -n "polySurface8" -p "Roof8";
	rename -uid "AD57F12C-BC45-663D-DC2F-9EBF3567F16C";
	setAttr ".rp" -type "double3" 813.67108154296875 593.36503601074219 -214.27287292480469 ;
	setAttr ".sp" -type "double3" 813.67108154296875 593.36503601074219 -214.27287292480469 ;
createNode mesh -n "polySurfaceShape29" -p "polySurface8";
	rename -uid "904AB9E6-AC41-CEDF-FC5C-6FA1398DD2B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface9" -p "Roof8";
	rename -uid "8819C9E6-164E-A8C7-5BC7-5E97F0E57FCF";
createNode mesh -n "polySurfaceShape30" -p "polySurface9";
	rename -uid "1313086C-3141-F5E2-CD75-838491E63A44";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface10" -p "Roof8";
	rename -uid "259B8EB8-F645-4E64-FCAE-FAAF91D7FF9A";
	setAttr ".rp" -type "double3" 813.67108154296875 593.36503601074219 -775.12359619140625 ;
	setAttr ".sp" -type "double3" 813.67108154296875 593.36503601074219 -775.12359619140625 ;
createNode mesh -n "polySurfaceShape31" -p "polySurface10";
	rename -uid "B5EFC88E-BE47-775F-38E2-A8939BCE2F24";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform22" -p "Roof8";
	rename -uid "727AA86E-BD4D-FF83-FD45-CC9A85F7A967";
	setAttr ".v" no;
createNode mesh -n "Roof8Shape" -p "transform22";
	rename -uid "3AF24A8A-6D4C-4F55-99E9-65A7FEC69184";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Wood6";
	rename -uid "F1C1C422-634D-7A2C-0EDC-C19649FCE70A";
	setAttr ".t" -type "double3" 807.03801219992533 449.73868002598005 -675.86052543803657 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 40 419.83032467085008 78.004791119230418 ;
	setAttr ".rp" -type "double3" 2.0961010704922955e-13 2.2737367544323206e-13 -3.4106051316484809e-13 ;
	setAttr ".rpt" -type "double3" -4.3698378249246156e-13 -2.9828868212177822e-13 0 ;
	setAttr ".spt" -type "double3" 2.0961010704922955e-13 2.2737367544323206e-13 -3.4106051316484809e-13 ;
createNode mesh -n "Wood6Shape" -p "Wood6";
	rename -uid "8F1EC3E2-3B47-EE2E-2051-3286837BC342";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder5";
	rename -uid "A2007C78-8744-FD21-0F6E-069B0F706A73";
	setAttr ".t" -type "double3" 870.44655776958678 88.237232544155404 -256.39291454793715 ;
	setAttr ".r" -type "double3" -90 45 -90 ;
	setAttr ".s" -type "double3" 50.004325354338349 173.23599047452657 14.569967170030836 ;
createNode mesh -n "ladder5Shape" -p "ladder5";
	rename -uid "F4290653-A347-AA71-85BB-23B8DEA8ED12";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder6";
	rename -uid "89179B62-4E4A-B0E5-4A40-5AA9D5B40F7C";
	setAttr ".t" -type "double3" 749.29982031703037 88.237232544155404 -265.28532226981781 ;
	setAttr ".r" -type "double3" -90 45 -90 ;
	setAttr ".s" -type "double3" 50.004325354338349 173.23599047452657 14.569967170030836 ;
createNode mesh -n "ladder6Shape" -p "ladder6";
	rename -uid "09AA84C7-D14B-0BCD-CD0A-C48C3AA2ACC8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder7";
	rename -uid "F34854A0-7041-D611-AB81-15B9D45ACFED";
	setAttr ".t" -type "double3" 817.65473771041366 117.1483621218828 -209.21890977055244 ;
	setAttr ".s" -type "double3" 116.12937881018271 243.14109066622404 42.025895478062473 ;
	setAttr ".rp" -type "double3" -66.639529617904572 -11.957096375020777 -101.52982434752461 ;
	setAttr ".sp" -type "double3" -0.50000003310540064 -0.028887411492145659 -0.49999998806758839 ;
	setAttr ".spt" -type "double3" -66.139529584799178 -11.928208963528631 -101.02982435945702 ;
createNode mesh -n "ladderShape7" -p "ladder7";
	rename -uid "3EBB25D5-7C4F-CEDA-BFA0-B0AD13ADAD7F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "lantern";
	rename -uid "9C063344-9540-D4FC-418B-53AA71B0C948";
	setAttr ".t" -type "double3" 955.66577232989061 408.88909092743739 -247.44913061107081 ;
	setAttr ".s" -type "double3" 49.492649969752421 49.492649969752421 49.492649969752421 ;
	setAttr ".rp" -type "double3" 3.187752210384073 59.612190810843856 3.1877475544301861 ;
	setAttr ".sp" -type "double3" 0.064408598293532293 1.2044655286648833 0.064408504219888307 ;
	setAttr ".spt" -type "double3" 3.1233436120905407 58.407725282178973 3.1233390502102978 ;
createNode mesh -n "lanternShape" -p "lantern";
	rename -uid "2C606862-E146-1945-2472-F0A1C6E5F178";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 10 ".pt";
	setAttr ".pt[0]" -type "float3" 0.21441428 0 -0.15522616 ;
	setAttr ".pt[1]" -type "float3" -0.27155167 0 -0.15522616 ;
	setAttr ".pt[6]" -type "float3" 0.21441428 0 0.19485551 ;
	setAttr ".pt[7]" -type "float3" -0.27155167 0 0.19485551 ;
	setAttr ".pt[8]" -type "float3" 0.43559197 0.70446527 -0.43559164 ;
	setAttr ".pt[9]" -type "float3" -0.4355911 0.70446527 -0.43559164 ;
	setAttr ".pt[10]" -type "float3" -0.4355911 0.70446527 0.43559161 ;
	setAttr ".pt[11]" -type "float3" 0.43559197 0.70446527 0.43559161 ;
	setAttr ".pt[14]" -type "float3" 4.4703484e-08 -3.2782555e-07 1.7881393e-07 ;
createNode mesh -n "polySurfaceShape9" -p "lantern";
	rename -uid "725D7298-B147-EA3B-5381-C0BF0B584397";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".pt[14]" -type "float3"  4.4703484e-08 -3.2782555e-07 1.7881393e-07;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel1";
	rename -uid "8CE2057D-1946-EE55-A11B-2B88476C049B";
	setAttr ".t" -type "double3" 2095.5052280920459 524.21785849663274 153.46359435696274 ;
	setAttr ".r" -type "double3" 0 55 0 ;
	setAttr ".s" -type "double3" 120.86504012670733 100.32053255607232 132.21642203641434 ;
	setAttr ".rp" -type "double3" 2.1612353319125485e-05 2.2737367544323206e-13 -2.3642138606860806e-05 ;
	setAttr ".rpt" -type "double3" -100.3206607664399 100.32060450624142 -1.6660078888096928e-05 ;
	setAttr ".sp" -type "double3" 1.7881393432617188e-07 0 -1.7881393432617188e-07 ;
	setAttr ".spt" -type "double3" 2.1433539384799313e-05 2.2737367544323206e-13 -2.3463324672534635e-05 ;
createNode transform -n "transform5" -p "millwheel1";
	rename -uid "A01BF181-D549-CBAB-B80C-8A92E53992F2";
	setAttr ".v" no;
createNode mesh -n "millwheel1Shape" -p "transform5";
	rename -uid "0E33CB3C-AB42-66CF-0638-5A89FF01CE4A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:8]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:10]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:10]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:21]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[11:21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[11:21]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[9:17]";
	setAttr ".pv" -type "double2" 0.40624998509883881 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.39999998 0.3125
		 0.41249999 0.31250617 0.42499995 0.3125 0.43749997 0.31250355 0.44999993 0.3125 0.46249992
		 0.31250501 0.4749999 0.3125 0.48749989 0.31250274 0.49999988 0.3125 0.51249987 0.31250572
		 0.52499986 0.3125 0.53749985 0.31250316 0.54999983 0.3125 0.56249982 0.31250328 0.57499981
		 0.3125 0.59999979 0.3125 0.61249971 0.31250301 0.62499976 0.3125 0.39999998 0.6875
		 0.41249999 0.68749684 0.42499995 0.6875 0.43749997 0.68749422 0.44999993 0.6875 0.46249992
		 0.68749768 0.4749999 0.6875 0.48749989 0.68749452 0.49999988 0.6875 0.51249987 0.68749702
		 0.52499986 0.6875 0.53749985 0.6874941 0.54999983 0.6875 0.56249982 0.6874941 0.57499981
		 0.6875 0.59999979 0.6875 0.61249971 0.68749368 0.62499976 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".vt[0:21]"  0.95105714 -1 -0.30901718 0 -1 6.3897109e-18
		 0.5877856 -1 -0.80901748 0 -1 -1.000000476837 -0.58778548 -1 -0.8090173 -0.95105678 -1 -0.30901706
		 -0.95105678 -1 0.30901706 -0.58778536 -1 0.80901712 -2.9802322e-08 -1 1.000000119209
		 0.58778524 -1 0.80901706 0.95105654 -1 0.309017 0.95105714 1 -0.30901718 0 1 -6.3897109e-18
		 0.5877856 1 -0.80901748 0 1 -1.000000476837 -0.58778548 1 -0.8090173 -0.95105678 1 -0.30901706
		 -0.95105678 1 0.30901706 -0.58778536 1 0.80901712 -2.9802322e-08 1 1.000000119209
		 0.58778524 1 0.80901706 0.95105654 1 0.309017;
	setAttr -s 31 ".ed[0:30]"  0 1 0 1 2 0 1 3 0 5 1 0 6 1 0 7 1 0 8 1 0
		 9 1 0 10 1 0 11 12 0 12 13 0 12 14 0 16 12 0 17 12 0 18 12 0 19 12 0 20 12 0 21 12 0
		 0 11 0 1 12 0 2 13 0 3 14 0 4 15 0 5 16 0 6 17 0 7 18 0 8 19 0 9 20 0 10 21 0 1 4 0
		 15 12 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 -2 19 10 -21
		mu 0 4 0 1 19 18
		f 4 -3 19 11 -22
		mu 0 4 2 3 21 20
		f 4 -30 19 -31 -23
		mu 0 4 4 5 23 22
		f 4 3 19 -13 -24
		mu 0 4 6 7 25 24
		f 4 4 19 -14 -25
		mu 0 4 8 9 27 26
		f 4 5 19 -15 -26
		mu 0 4 10 11 29 28
		f 4 6 19 -16 -27
		mu 0 4 12 13 31 30
		f 4 -8 27 16 -20
		mu 0 4 13 14 32 31
		f 4 8 19 -18 -29
		mu 0 4 15 16 34 33
		f 4 -1 18 9 -20
		mu 0 4 16 17 35 34;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "MillWheel2";
	rename -uid "4289CC35-E946-498D-873B-618DA8CB9117";
	setAttr ".t" -type "double3" 1300.7472281138737 150.16237651633591 -502.58170282456319 ;
	setAttr ".r" -type "double3" 90 90 0 ;
	setAttr ".s" -type "double3" 111.50204339643184 99.059467827496746 111.50204339643184 ;
	setAttr ".rp" -type "double3" -9.5150536943102586e-06 -156.36388636652009 -7.0450891573987586e-06 ;
	setAttr ".rpt" -type "double3" -185.32573828382527 156.36390657325418 2.9721787449644616e-05 ;
	setAttr ".sp" -type "double3" -1.1920928955078125e-07 0 -5.9604644775390625e-08 ;
	setAttr ".spt" -type "double3" -9.3958444047594773e-06 -156.36388636652009 -6.985484512623368e-06 ;
createNode mesh -n "MillWheel2Shape" -p "MillWheel2";
	rename -uid "BACABAB5-2D4E-93A1-BE17-998873893093";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[10:19]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:9]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[20]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:9]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:19]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[10:19]" "vtx[21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[10:19]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[20:29]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[10:19]";
	setAttr ".pv" -type "double2" 0.58749991655349731 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625
		 0.37359107 0.24809146 0.4517161 0.3048526 0.54828393 0.3048526 0.62640893 0.24809146
		 0.65625 0.15625 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125
		 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995 0.3125 0.57499993 0.3125
		 0.5999999 0.3125 0.62499988 0.3125 0.375 0.6875 0.40000001 0.6875 0.42500001 0.6875
		 0.45000002 0.6875 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875
		 0.57499993 0.6875 0.5999999 0.6875 0.62499988 0.6875 0.62640893 0.75190854 0.54828387
		 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.37359107 0.93559146
		 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.5
		 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[6]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[8]" -type "float3" -5.9604645e-08 0 0.053139269 ;
	setAttr ".pt[9]" -type "float3" -8.9406967e-08 0 2.9802322e-08 ;
	setAttr ".pt[10]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[12]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[13]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[16]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[18]" -type "float3" -5.9604645e-08 0 0.053139269 ;
	setAttr ".pt[19]" -type "float3" -8.9406967e-08 0 2.9802322e-08 ;
	setAttr ".pt[22]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[23]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[24]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[25]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[26]" -type "float3" 0 0 -3.5527137e-15 ;
	setAttr ".pt[27]" -type "float3" 0 0 -3.5527137e-15 ;
	setAttr -s 22 ".vt[0:21]"  0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672
		 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524 -1.000000119209 -1 5.9604645e-08
		 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566 0.30901703 -1 0.95105654 0.809017 -1 0.58778524
		 1 -1 0 0.80901706 1 -0.58778542 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566
		 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-08 -0.80901706 1 0.58778536
		 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524 1 1 0 0 -1 0
		 0 1 0;
	setAttr -s 50 ".ed[0:49]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 10 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0
		 20 0 1 20 1 1 20 2 1 20 3 1 20 4 1 20 5 1 20 6 1 20 7 1 20 8 1 20 9 1 10 21 1 11 21 1
		 12 21 1 13 21 1 14 21 1 15 21 1 16 21 1 17 21 1 18 21 1 19 21 1;
	setAttr -s 30 -ch 100 ".fc[0:29]" -type "polyFaces" 
		f 4 0 21 -11 -21
		mu 0 4 10 11 22 21
		f 4 1 22 -12 -22
		mu 0 4 11 12 23 22
		f 4 2 23 -13 -23
		mu 0 4 12 13 24 23
		f 4 3 24 -14 -24
		mu 0 4 13 14 25 24
		f 4 4 25 -15 -25
		mu 0 4 14 15 26 25
		f 4 5 26 -16 -26
		mu 0 4 15 16 27 26
		f 4 6 27 -17 -27
		mu 0 4 16 17 28 27
		f 4 7 28 -18 -28
		mu 0 4 17 18 29 28
		f 4 8 29 -19 -29
		mu 0 4 18 19 30 29
		f 4 9 20 -20 -30
		mu 0 4 19 20 31 30
		f 3 -1 -31 31
		mu 0 3 1 0 42
		f 3 -2 -32 32
		mu 0 3 2 1 42
		f 3 -3 -33 33
		mu 0 3 3 2 42
		f 3 -4 -34 34
		mu 0 3 4 3 42
		f 3 -5 -35 35
		mu 0 3 5 4 42
		f 3 -6 -36 36
		mu 0 3 6 5 42
		f 3 -7 -37 37
		mu 0 3 7 6 42
		f 3 -8 -38 38
		mu 0 3 8 7 42
		f 3 -9 -39 39
		mu 0 3 9 8 42
		f 3 -10 -40 30
		mu 0 3 0 9 42
		f 3 10 41 -41
		mu 0 3 40 39 43
		f 3 11 42 -42
		mu 0 3 39 38 43
		f 3 12 43 -43
		mu 0 3 38 37 43
		f 3 13 44 -44
		mu 0 3 37 36 43
		f 3 14 45 -45
		mu 0 3 36 35 43
		f 3 15 46 -46
		mu 0 3 35 34 43
		f 3 16 47 -47
		mu 0 3 34 33 43
		f 3 17 48 -48
		mu 0 3 33 32 43
		f 3 18 49 -49
		mu 0 3 32 41 43
		f 3 19 40 -50
		mu 0 3 41 40 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel2";
	rename -uid "D0C9A6A7-8F41-57DD-993E-12B953A4350C";
	setAttr ".t" -type "double3" 2001.1042137292634 362.70593479483853 157.85738531457855 ;
	setAttr ".s" -type "double3" 168.74077937260532 26.766597055388214 168.74077937260532 ;
	setAttr ".rp" -type "double3" -5.348912054914217e-05 168.74078369140625 0 ;
	setAttr ".sp" -type "double3" -3.1698988678385831e-07 1.0000000255942929 8.4217074070887601e-17 ;
	setAttr ".spt" -type "double3" -5.3172130662358312e-05 167.74078366581196 -8.4217074070887662e-17 ;
createNode transform -n "transform4" -p "millwheel2";
	rename -uid "31591E9B-D04E-9795-B322-28B28AE00F68";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform4";
	rename -uid "8A592D52-DC4E-D528-7CAC-E7947C13A35C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "millwheel6";
	rename -uid "D8C3C62A-854D-19DA-3A09-B886E3125BFA";
	setAttr ".t" -type "double3" 1995.1846238016205 609.651417434118 153.46359252929685 ;
	setAttr ".s" -type "double3" 168.74077937260532 26.766597055388214 168.74077937260532 ;
	setAttr ".rp" -type "double3" -5.348912054914217e-05 115.20759135494451 2.9063158755413989e-14 ;
	setAttr ".sp" -type "double3" -3.1698988678385831e-07 -0.9999999081173172 -3.3306690738754696e-16 ;
	setAttr ".spt" -type "double3" -5.3172130662358312e-05 116.20759126306183 2.9396225662801542e-14 ;
createNode transform -n "transform6" -p "millwheel6";
	rename -uid "C90F9C24-1049-04A5-5064-EBA4A4D39F5D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape2" -p "transform6";
	rename -uid "CE3DF2EC-5648-579A-186D-6882A4609035";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[10:19]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:9]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[20]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:9]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:19]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[10:19]" "vtx[21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[10:19]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[20:29]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[10:19]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625
		 0.37359107 0.24809146 0.4517161 0.3048526 0.54828393 0.3048526 0.62640893 0.24809146
		 0.65625 0.15625 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125
		 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995 0.3125 0.57499993 0.3125
		 0.5999999 0.3125 0.62499988 0.3125 0.375 0.6875 0.40000001 0.6875 0.42500001 0.6875
		 0.45000002 0.6875 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875
		 0.57499993 0.6875 0.5999999 0.6875 0.62499988 0.6875 0.62640893 0.75190854 0.54828387
		 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.37359107 0.93559146
		 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.5
		 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".vt[0:21]"  0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672
		 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524 -1.000000119209 -1 5.9604645e-08
		 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566 0.30901703 -1 0.95105654 0.809017 -1 0.58778524
		 1 -1 0 0.80901706 1 -0.58778542 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566
		 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-08 -0.80901706 1 0.58778536
		 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524 1 1 0 0 -1 0
		 0 1 0;
	setAttr -s 50 ".ed[0:49]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 10 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0
		 20 0 1 20 1 1 20 2 1 20 3 1 20 4 1 20 5 1 20 6 1 20 7 1 20 8 1 20 9 1 10 21 1 11 21 1
		 12 21 1 13 21 1 14 21 1 15 21 1 16 21 1 17 21 1 18 21 1 19 21 1;
	setAttr -s 30 -ch 100 ".fc[0:29]" -type "polyFaces" 
		f 4 0 21 -11 -21
		mu 0 4 10 11 22 21
		f 4 1 22 -12 -22
		mu 0 4 11 12 23 22
		f 4 2 23 -13 -23
		mu 0 4 12 13 24 23
		f 4 3 24 -14 -24
		mu 0 4 13 14 25 24
		f 4 4 25 -15 -25
		mu 0 4 14 15 26 25
		f 4 5 26 -16 -26
		mu 0 4 15 16 27 26
		f 4 6 27 -17 -27
		mu 0 4 16 17 28 27
		f 4 7 28 -18 -28
		mu 0 4 17 18 29 28
		f 4 8 29 -19 -29
		mu 0 4 18 19 30 29
		f 4 9 20 -20 -30
		mu 0 4 19 20 31 30
		f 3 -1 -31 31
		mu 0 3 1 0 42
		f 3 -2 -32 32
		mu 0 3 2 1 42
		f 3 -3 -33 33
		mu 0 3 3 2 42
		f 3 -4 -34 34
		mu 0 3 4 3 42
		f 3 -5 -35 35
		mu 0 3 5 4 42
		f 3 -6 -36 36
		mu 0 3 6 5 42
		f 3 -7 -37 37
		mu 0 3 7 6 42
		f 3 -8 -38 38
		mu 0 3 8 7 42
		f 3 -9 -39 39
		mu 0 3 9 8 42
		f 3 -10 -40 30
		mu 0 3 0 9 42
		f 3 10 41 -41
		mu 0 3 40 39 43
		f 3 11 42 -42
		mu 0 3 39 38 43
		f 3 12 43 -43
		mu 0 3 38 37 43
		f 3 13 44 -44
		mu 0 3 37 36 43
		f 3 14 45 -45
		mu 0 3 36 35 43
		f 3 15 46 -46
		mu 0 3 35 34 43
		f 3 16 47 -47
		mu 0 3 34 33 43
		f 3 17 48 -48
		mu 0 3 33 32 43
		f 3 18 49 -49
		mu 0 3 32 41 43
		f 3 19 40 -50
		mu 0 3 41 40 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel4";
	rename -uid "9EF8B3D9-394C-C9E3-B08F-A58031700AC3";
	setAttr ".t" -type "double3" 1747.1108729205321 571.06640956017668 929.24574746688927 ;
	setAttr ".s" -type "double3" 168.74077937260532 26.766597055388214 168.74077937260532 ;
	setAttr ".rp" -type "double3" -5.348912054914217e-05 115.20759135494451 2.9063158755413989e-14 ;
	setAttr ".sp" -type "double3" -3.1698988678385831e-07 -0.9999999081173172 -3.3306690738754696e-16 ;
	setAttr ".spt" -type "double3" -5.3172130662358312e-05 116.20759126306183 2.9396225662801542e-14 ;
createNode transform -n "transform9" -p "millwheel4";
	rename -uid "E852AC6C-B648-19B1-5B24-0CB05E3FCA07";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape3" -p "transform9";
	rename -uid "95A6234E-834D-1185-1226-93B7DF304192";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[10:19]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:9]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[20]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:9]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:19]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[10:19]" "vtx[21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[10:19]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[20:29]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[10:19]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625
		 0.37359107 0.24809146 0.4517161 0.3048526 0.54828393 0.3048526 0.62640893 0.24809146
		 0.65625 0.15625 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125
		 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995 0.3125 0.57499993 0.3125
		 0.5999999 0.3125 0.62499988 0.3125 0.375 0.6875 0.40000001 0.6875 0.42500001 0.6875
		 0.45000002 0.6875 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875
		 0.57499993 0.6875 0.5999999 0.6875 0.62499988 0.6875 0.62640893 0.75190854 0.54828387
		 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.37359107 0.93559146
		 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.5
		 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".vt[0:21]"  0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672
		 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524 -1.000000119209 -1 5.9604645e-08
		 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566 0.30901703 -1 0.95105654 0.809017 -1 0.58778524
		 1 -1 0 0.80901706 1 -0.58778542 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566
		 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-08 -0.80901706 1 0.58778536
		 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524 1 1 0 0 -1 0
		 0 1 0;
	setAttr -s 50 ".ed[0:49]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 10 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0
		 20 0 1 20 1 1 20 2 1 20 3 1 20 4 1 20 5 1 20 6 1 20 7 1 20 8 1 20 9 1 10 21 1 11 21 1
		 12 21 1 13 21 1 14 21 1 15 21 1 16 21 1 17 21 1 18 21 1 19 21 1;
	setAttr -s 30 -ch 100 ".fc[0:29]" -type "polyFaces" 
		f 4 0 21 -11 -21
		mu 0 4 10 11 22 21
		f 4 1 22 -12 -22
		mu 0 4 11 12 23 22
		f 4 2 23 -13 -23
		mu 0 4 12 13 24 23
		f 4 3 24 -14 -24
		mu 0 4 13 14 25 24
		f 4 4 25 -15 -25
		mu 0 4 14 15 26 25
		f 4 5 26 -16 -26
		mu 0 4 15 16 27 26
		f 4 6 27 -17 -27
		mu 0 4 16 17 28 27
		f 4 7 28 -18 -28
		mu 0 4 17 18 29 28
		f 4 8 29 -19 -29
		mu 0 4 18 19 30 29
		f 4 9 20 -20 -30
		mu 0 4 19 20 31 30
		f 3 -1 -31 31
		mu 0 3 1 0 42
		f 3 -2 -32 32
		mu 0 3 2 1 42
		f 3 -3 -33 33
		mu 0 3 3 2 42
		f 3 -4 -34 34
		mu 0 3 4 3 42
		f 3 -5 -35 35
		mu 0 3 5 4 42
		f 3 -6 -36 36
		mu 0 3 6 5 42
		f 3 -7 -37 37
		mu 0 3 7 6 42
		f 3 -8 -38 38
		mu 0 3 8 7 42
		f 3 -9 -39 39
		mu 0 3 9 8 42
		f 3 -10 -40 30
		mu 0 3 0 9 42
		f 3 10 41 -41
		mu 0 3 40 39 43
		f 3 11 42 -42
		mu 0 3 39 38 43
		f 3 12 43 -43
		mu 0 3 38 37 43
		f 3 13 44 -44
		mu 0 3 37 36 43
		f 3 14 45 -45
		mu 0 3 36 35 43
		f 3 15 46 -46
		mu 0 3 35 34 43
		f 3 16 47 -47
		mu 0 3 34 33 43
		f 3 17 48 -48
		mu 0 3 33 32 43
		f 3 18 49 -49
		mu 0 3 32 41 43
		f 3 19 40 -50
		mu 0 3 41 40 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel5";
	rename -uid "77A843C8-2B4D-4240-7062-45929B1B3A02";
	setAttr ".t" -type "double3" 1609.8855143157548 816.84472024194019 661.99232762125746 ;
	setAttr ".r" -type "double3" 0 55 0 ;
	setAttr ".s" -type "double3" 120.86504012670733 100.32053255607232 132.21642203641434 ;
	setAttr ".rp" -type "double3" 2.1612353319125485e-05 2.2737367544323206e-13 -2.3642138606860806e-05 ;
	setAttr ".rpt" -type "double3" -100.3206607664399 100.32060450624142 -1.6660078888096928e-05 ;
	setAttr ".sp" -type "double3" 1.7881393432617188e-07 0 -1.7881393432617188e-07 ;
	setAttr ".spt" -type "double3" 2.1433539384799313e-05 2.2737367544323206e-13 -2.3463324672534635e-05 ;
createNode transform -n "transform7" -p "millwheel5";
	rename -uid "F2E515F5-2A49-DCB6-F8A2-E3A6F630A74F";
	setAttr ".v" no;
createNode mesh -n "millwheel3Shape" -p "transform7";
	rename -uid "223229FD-5340-0B93-5C76-F08D050C9768";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:8]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:10]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:10]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:21]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[11:21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[11:21]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[9:17]";
	setAttr ".pv" -type "double2" 0.40624998509883881 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.39999998 0.3125
		 0.41249999 0.31250617 0.42499995 0.3125 0.43749997 0.31250355 0.44999993 0.3125 0.46249992
		 0.31250501 0.4749999 0.3125 0.48749989 0.31250274 0.49999988 0.3125 0.51249987 0.31250572
		 0.52499986 0.3125 0.53749985 0.31250316 0.54999983 0.3125 0.56249982 0.31250328 0.57499981
		 0.3125 0.59999979 0.3125 0.61249971 0.31250301 0.62499976 0.3125 0.39999998 0.6875
		 0.41249999 0.68749684 0.42499995 0.6875 0.43749997 0.68749422 0.44999993 0.6875 0.46249992
		 0.68749768 0.4749999 0.6875 0.48749989 0.68749452 0.49999988 0.6875 0.51249987 0.68749702
		 0.52499986 0.6875 0.53749985 0.6874941 0.54999983 0.6875 0.56249982 0.6874941 0.57499981
		 0.6875 0.59999979 0.6875 0.61249971 0.68749368 0.62499976 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".vt[0:21]"  0.95105714 -1 -0.30901718 0 -1 6.3897109e-18
		 0.5877856 -1 -0.80901748 0 -1 -1.000000476837 -0.58778548 -1 -0.8090173 -0.95105678 -1 -0.30901706
		 -0.95105678 -1 0.30901706 -0.58778536 -1 0.80901712 -2.9802322e-08 -1 1.000000119209
		 0.58778524 -1 0.80901706 0.95105654 -1 0.309017 0.95105714 1 -0.30901718 0 1 -6.3897109e-18
		 0.5877856 1 -0.80901748 0 1 -1.000000476837 -0.58778548 1 -0.8090173 -0.95105678 1 -0.30901706
		 -0.95105678 1 0.30901706 -0.58778536 1 0.80901712 -2.9802322e-08 1 1.000000119209
		 0.58778524 1 0.80901706 0.95105654 1 0.309017;
	setAttr -s 31 ".ed[0:30]"  0 1 0 1 2 0 1 3 0 5 1 0 6 1 0 7 1 0 8 1 0
		 9 1 0 10 1 0 11 12 0 12 13 0 12 14 0 16 12 0 17 12 0 18 12 0 19 12 0 20 12 0 21 12 0
		 0 11 0 1 12 0 2 13 0 3 14 0 4 15 0 5 16 0 6 17 0 7 18 0 8 19 0 9 20 0 10 21 0 1 4 0
		 15 12 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 -2 19 10 -21
		mu 0 4 0 1 19 18
		f 4 -3 19 11 -22
		mu 0 4 2 3 21 20
		f 4 -30 19 -31 -23
		mu 0 4 4 5 23 22
		f 4 3 19 -13 -24
		mu 0 4 6 7 25 24
		f 4 4 19 -14 -25
		mu 0 4 8 9 27 26
		f 4 5 19 -15 -26
		mu 0 4 10 11 29 28
		f 4 6 19 -16 -27
		mu 0 4 12 13 31 30
		f 4 -8 27 16 -20
		mu 0 4 13 14 32 31
		f 4 8 19 -18 -29
		mu 0 4 15 16 34 33
		f 4 -1 18 9 -20
		mu 0 4 16 17 35 34;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel7";
	rename -uid "E4A7B640-844C-BE22-BBBF-CDB3B3CB5B4A";
	setAttr ".t" -type "double3" 1509.5649179121044 902.27837365443952 661.99233161101188 ;
	setAttr ".s" -type "double3" 168.74077937260532 26.766597055388214 168.74077937260532 ;
	setAttr ".rp" -type "double3" 2.3494145594054031e-05 115.2075272244667 -2.2040699413342738e-05 ;
	setAttr ".sp" -type "double3" 1.3923217423439382e-07 -1.0000023040318098 -1.3061869053032638e-07 ;
	setAttr ".spt" -type "double3" 2.3354913419819637e-05 116.20752952849853 -2.1910080722812412e-05 ;
createNode transform -n "transform8" -p "millwheel7";
	rename -uid "75C4C9DF-3A49-F4B4-ADA8-289E4AC63861";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape5" -p "transform8";
	rename -uid "CFE387FA-B74D-4883-9EE2-BAA183025613";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[10:19]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:9]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[20]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:9]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:19]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[10:19]" "vtx[21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[10:19]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[20:29]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[10:19]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625
		 0.37359107 0.24809146 0.4517161 0.3048526 0.54828393 0.3048526 0.62640893 0.24809146
		 0.65625 0.15625 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125
		 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995 0.3125 0.57499993 0.3125
		 0.5999999 0.3125 0.62499988 0.3125 0.375 0.6875 0.40000001 0.6875 0.42500001 0.6875
		 0.45000002 0.6875 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875
		 0.57499993 0.6875 0.5999999 0.6875 0.62499988 0.6875 0.62640893 0.75190854 0.54828387
		 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.37359107 0.93559146
		 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.5
		 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".vt[0:21]"  0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672
		 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524 -1.000000119209 -1 5.9604645e-08
		 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566 0.30901703 -1 0.95105654 0.809017 -1 0.58778524
		 1 -1 0 0.80901706 1 -0.58778542 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566
		 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-08 -0.80901706 1 0.58778536
		 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524 1 1 0 0 -1 0
		 0 1 0;
	setAttr -s 50 ".ed[0:49]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 10 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0
		 20 0 1 20 1 1 20 2 1 20 3 1 20 4 1 20 5 1 20 6 1 20 7 1 20 8 1 20 9 1 10 21 1 11 21 1
		 12 21 1 13 21 1 14 21 1 15 21 1 16 21 1 17 21 1 18 21 1 19 21 1;
	setAttr -s 30 -ch 100 ".fc[0:29]" -type "polyFaces" 
		f 4 0 21 -11 -21
		mu 0 4 10 11 22 21
		f 4 1 22 -12 -22
		mu 0 4 11 12 23 22
		f 4 2 23 -13 -23
		mu 0 4 12 13 24 23
		f 4 3 24 -14 -24
		mu 0 4 13 14 25 24
		f 4 4 25 -15 -25
		mu 0 4 14 15 26 25
		f 4 5 26 -16 -26
		mu 0 4 15 16 27 26
		f 4 6 27 -17 -27
		mu 0 4 16 17 28 27
		f 4 7 28 -18 -28
		mu 0 4 17 18 29 28
		f 4 8 29 -19 -29
		mu 0 4 18 19 30 29
		f 4 9 20 -20 -30
		mu 0 4 19 20 31 30
		f 3 -1 -31 31
		mu 0 3 1 0 42
		f 3 -2 -32 32
		mu 0 3 2 1 42
		f 3 -3 -33 33
		mu 0 3 3 2 42
		f 3 -4 -34 34
		mu 0 3 4 3 42
		f 3 -5 -35 35
		mu 0 3 5 4 42
		f 3 -6 -36 36
		mu 0 3 6 5 42
		f 3 -7 -37 37
		mu 0 3 7 6 42
		f 3 -8 -38 38
		mu 0 3 8 7 42
		f 3 -9 -39 39
		mu 0 3 9 8 42
		f 3 -10 -40 30
		mu 0 3 0 9 42
		f 3 10 41 -41
		mu 0 3 40 39 43
		f 3 11 42 -42
		mu 0 3 39 38 43
		f 3 12 43 -43
		mu 0 3 38 37 43
		f 3 13 44 -44
		mu 0 3 37 36 43
		f 3 14 45 -45
		mu 0 3 36 35 43
		f 3 15 46 -46
		mu 0 3 35 34 43
		f 3 16 47 -47
		mu 0 3 34 33 43
		f 3 17 48 -48
		mu 0 3 33 32 43
		f 3 18 49 -49
		mu 0 3 32 41 43
		f 3 19 40 -50
		mu 0 3 41 40 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel8";
	rename -uid "6B3E6A42-6842-11F0-DFF8-E2A3E2E13326";
	setAttr ".t" -type "double3" 237.5458984375 -77.03759765625 267.25341796875 ;
	setAttr ".rp" -type "double3" 1509.56494140625 816.84478759765625 661.9923095703125 ;
	setAttr ".sp" -type "double3" 1509.56494140625 816.84478759765625 661.9923095703125 ;
createNode transform -n "transform10" -p "millwheel8";
	rename -uid "4777E64D-7A49-1F7F-C6C9-128D2D5A013E";
	setAttr ".v" no;
createNode mesh -n "pCylinder6Shape" -p "transform10";
	rename -uid "9C239401-D844-44C4-0ADB-D5A043D4B329";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "millWheel";
	rename -uid "2B24517A-2C4F-9F58-84EE-FBB5D643FA66";
	setAttr ".t" -type "double3" -380.96697385355628 -591.12874833415594 -1418.4271176235616 ;
	setAttr ".r" -type "double3" 90 90 0 ;
	setAttr ".rp" -type "double3" 1747.1107850181047 686.27391265831375 929.24569543844632 ;
	setAttr ".rpt" -type "double3" -153.8538315739047 153.85390236157605 7.0787685672257794e-05 ;
	setAttr ".sp" -type "double3" 1747.1107850181047 686.27391265831375 929.24569543844632 ;
createNode mesh -n "millWheelShape" -p "millWheel";
	rename -uid "A2B664A5-AD4D-5400-6B6F-26B6B0F2ED3C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999743700027466 0.5000007557682693 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Wood7";
	rename -uid "FA029D9F-2D4E-EC23-AB23-B299D71A7E15";
	setAttr ".rp" -type "double3" 676.09768228312009 135.23564081725269 697.56469474897062 ;
	setAttr ".sp" -type "double3" 676.09768228312009 135.23564081725269 697.56469474897062 ;
createNode transform -n "transform16" -p "Wood7";
	rename -uid "98E68217-E646-63B5-0D20-E49CBC7CA388";
	setAttr ".v" no;
createNode mesh -n "Wood7Shape" -p "transform16";
	rename -uid "EE57C676-8648-AB6A-6FA5-9C9820CE7B6E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Wood8";
	rename -uid "B67A8527-914E-080D-4F8C-E699B39BB66F";
	setAttr ".rp" -type "double3" 676.09768228312009 135.23564081725269 399.03418018060216 ;
	setAttr ".sp" -type "double3" 676.09768228312009 135.23564081725269 399.03418018060216 ;
createNode transform -n "transform15" -p "Wood8";
	rename -uid "3C221B4B-BE46-1044-EA15-538AD0F124E8";
	setAttr ".v" no;
createNode mesh -n "Wood8Shape" -p "transform15";
	rename -uid "2ED966E0-8243-A9F3-959F-E1B8516B94CB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Trunk1";
	rename -uid "FF2FDE29-6D48-982B-6831-0198DAA13029";
	setAttr ".t" -type "double3" 583.32574256727094 -17.490011846455502 0 ;
	setAttr ".r" -type "double3" 0 33.254399165795952 0 ;
	setAttr ".rp" -type "double3" 676.09765625 142.17582698793689 553.68967466639515 ;
	setAttr ".sp" -type "double3" 676.09765625 142.17582698793689 553.68967466639515 ;
createNode transform -n "polySurface1" -p "Trunk1";
	rename -uid "06544242-704D-9D9A-D6ED-43AEE8F33399";
createNode mesh -n "polySurfaceShape10" -p "polySurface1";
	rename -uid "DDF9D391-9D4B-7F8A-C5BF-8D9CA8B16541";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface2" -p "Trunk1";
	rename -uid "43BD0D18-D14C-638F-DEE6-30A6A77699F4";
createNode transform -n "transform24" -p "polySurface2";
	rename -uid "3F8B644F-C74D-F3E6-F308-D98986A2B98E";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape11" -p "transform24";
	rename -uid "41EA7B39-454B-2A1C-7A33-2EA3C83BFFAC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface3" -p "Trunk1";
	rename -uid "C15D0034-0E47-D78C-A6FC-8C825A6FF67B";
createNode transform -n "transform23" -p "polySurface3";
	rename -uid "93D54F23-2E4B-7CF3-E591-ED93268EFFD3";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape12" -p "transform23";
	rename -uid "614DBCB6-6240-57D8-4A3E-73A329A21360";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface4" -p "Trunk1";
	rename -uid "0FE7619E-CD4D-9EE2-F7C8-D5B335C7C438";
createNode transform -n "transform25" -p "polySurface4";
	rename -uid "0E276AF4-9B4D-6FFB-C141-6B8F3F215D7D";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape13" -p "transform25";
	rename -uid "955AEE7A-BC4C-FE72-2FC5-37B0AF99EE45";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface5" -p "Trunk1";
	rename -uid "35E7D657-BA44-C826-C038-CC93046707DA";
createNode transform -n "transform26" -p "polySurface5";
	rename -uid "45C1724D-C749-E44C-6629-2C8391B6C2CA";
	setAttr ".v" no;
createNode mesh -n "polySurfaceShape14" -p "transform26";
	rename -uid "54F93DFE-A94F-CEB6-3059-9ABA6C581CB1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform18" -p "Trunk1";
	rename -uid "EB03A106-3B4E-B54D-0813-9499927C796B";
	setAttr ".v" no;
createNode mesh -n "Trunk1Shape" -p "transform18";
	rename -uid "3F620224-2A43-41E8-7F0F-979786E2252C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "TreetopBig2";
	rename -uid "13257305-914D-83A3-236F-2FA85BC62D90";
	setAttr ".t" -type "double3" -867.2022558346398 934.36137398170706 -1105.4883910337539 ;
	setAttr ".s" -type "double3" 150.02982088141485 150.02982088141485 150.02982088141485 ;
createNode mesh -n "TreetopBig2Shape" -p "TreetopBig2";
	rename -uid "755F8CF6-1748-A8AE-C5D0-6DBFA65613FA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "TreetopBig1";
	rename -uid "9183B8E4-8C42-E77E-70A3-C78A086D4D5D";
	setAttr ".t" -type "double3" -578.59715287906863 1399.6144078986999 -1160.529435296869 ;
	setAttr ".s" -type "double3" 396.22629164834768 396.22629164834768 396.22629164834768 ;
createNode mesh -n "TreetopBig1Shape" -p "TreetopBig1";
	rename -uid "F42377FD-794C-9EC7-B994-14BBD8F2C6C4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape18" -p "TreetopBig1";
	rename -uid "B3AB189B-2A45-95D9-27B2-3DB2B3C78878";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 24 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 8 16 0
		 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0 16 24 0 17 25 0 18 26 0 19 27 0
		 20 28 0 21 29 0 22 30 0 23 31 0 32 0 0 32 1 0 32 2 0 32 3 0 32 4 0 32 5 0 32 6 0
		 32 7 0 24 33 0 25 33 0 26 33 0 27 33 0 28 33 0 29 33 0 30 33 0 31 33 0;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BranchSmallTree";
	rename -uid "C3044183-B048-C273-3661-CD9717BB582E";
	setAttr ".t" -type "double3" 237.0389104069161 378.81930510903157 -868.0801349434297 ;
	setAttr ".r" -type "double3" -19.657651836070322 11.239390848330968 -48.347141580675249 ;
	setAttr ".s" -type "double3" 19.738448265983983 137.7873969836329 19.738448265983983 ;
createNode mesh -n "BranchSmallTreeShape" -p "BranchSmallTree";
	rename -uid "FDEE458E-6242-6A5F-A7A5-81BAA593B07C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape25" -p "BranchSmallTree";
	rename -uid "3D5B1EB2-A346-8238-40F9-65B442A9ABE1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.1562500074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  1.0999936 0 -1.0999936 -9.2722544e-08 
		0 -1.5556262 -1.0999936 0 -1.0999936 -1.5556262 0 -4.6361301e-08 -1.0999936 0 1.0999936 
		-9.2722544e-08 0 1.5556262 1.0999936 0 1.0999936 1.5556262 0 -4.6361301e-08;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BranchBigTree";
	rename -uid "F6166895-D24C-FBCB-F05A-40B6F3D1AE73";
	setAttr ".t" -type "double3" -699.14461121557235 794.1366598181304 -1136.7344129333424 ;
	setAttr ".r" -type "double3" 167.07172859965732 8.4557490965389643 -127.80911358858354 ;
	setAttr ".s" -type "double3" 16.53255322891691 132.29910916185534 16.53255322891691 ;
createNode mesh -n "BranchBigTreeShape" -p "BranchBigTree";
	rename -uid "A91782E1-4E4F-DB08-AC7B-B980708EBAF5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape22" -p "BranchBigTree";
	rename -uid "6436F78F-9345-EDA2-2694-1D8E6B7FFE03";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.1562500074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  1.0999936 0 -1.0999936 -9.2722544e-08 
		0 -1.5556262 -1.0999936 0 -1.0999936 -1.5556262 0 -4.6361301e-08 -1.0999936 0 1.0999936 
		-9.2722544e-08 0 1.5556262 1.0999936 0 1.0999936 1.5556262 0 -4.6361301e-08;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopSmall1";
	rename -uid "5B520012-F84B-4774-EB6B-8182DCC5EBFD";
	setAttr ".t" -type "double3" 143.02510776088042 980.31497425171665 -823.28832922587344 ;
	setAttr ".s" -type "double3" 298.91959849240834 298.91959849240834 298.91959849240834 ;
createNode mesh -n "TreetopSmall1Shape" -p "TreetopSmall1";
	rename -uid "FA488CD6-E442-FE2A-8055-2F86876E5BB7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape17" -p "TreetopSmall1";
	rename -uid "F14B8A84-2647-C0BC-C6FA-F9A5F3468652";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 24 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 8 16 0
		 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0 16 24 0 17 25 0 18 26 0 19 27 0
		 20 28 0 21 29 0 22 30 0 23 31 0 32 0 0 32 1 0 32 2 0 32 3 0 32 4 0 32 5 0 32 6 0
		 32 7 0 24 33 0 25 33 0 26 33 0 27 33 0 28 33 0 29 33 0 30 33 0 31 33 0;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopSmall2";
	rename -uid "7136C0E9-B14E-D745-5CFB-AFB17C357528";
	setAttr ".t" -type "double3" 346.02559682869492 602.76606440152 -928.95699943781733 ;
	setAttr ".s" -type "double3" 150.02982088141485 150.02982088141485 150.02982088141485 ;
createNode mesh -n "TreetopSmall2Shape" -p "TreetopSmall2";
	rename -uid "81DBA63A-F14F-A23E-3384-3D9F4DBFD500";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape19" -p "TreetopSmall2";
	rename -uid "BB755933-3C43-AC74-B9E1-85B7D093C607";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 24 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 8 16 0
		 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0 16 24 0 17 25 0 18 26 0 19 27 0
		 20 28 0 21 29 0 22 30 0 23 31 0 32 0 0 32 1 0 32 2 0 32 3 0 32 4 0 32 5 0 32 6 0
		 32 7 0 24 33 0 25 33 0 26 33 0 27 33 0 28 33 0 29 33 0 30 33 0 31 33 0;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "SmallTree";
	rename -uid "0FA8E8AA-164F-0AF7-3ED2-449226BE4E60";
	setAttr ".t" -type "double3" 143.08813056295202 397.71270014944184 -819.17452014812625 ;
	setAttr ".s" -type "double3" 38.950013115879798 360.50459967344131 38.950013115879798 ;
createNode mesh -n "SmallTreeShape" -p "SmallTree";
	rename -uid "C76A6CF7-3946-FB61-DE60-93A7902C2C29";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape24" -p "SmallTree";
	rename -uid "BC5F4C61-8845-9A7B-CE3B-7288BB8E0111";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.1562500074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  2.2611971 0 -2.2611971 -2.2890517e-07 
		0 -3.1978133 -2.2611971 0 -2.2611971 -3.1978133 0 -1.5275322e-07 -2.2611971 0 2.2611971 
		-2.2890517e-07 0 3.1978133 2.2611971 0 2.2611971 3.1978133 0 -1.5275322e-07;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BigTree";
	rename -uid "F7C5B834-C647-FB25-1835-979DFFFA1191";
	setAttr ".t" -type "double3" -576.13795186641414 617.81054753897888 -1164.7083536555776 ;
	setAttr ".s" -type "double3" 38.950013115879798 582.10507434258523 38.950013115879798 ;
createNode mesh -n "BigTreeShape" -p "BigTree";
	rename -uid "9A21E5F5-3E46-6AE9-74AC-A393EB409938";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape23" -p "BigTree";
	rename -uid "359F3D46-774D-A5B1-798E-9AB8C102723E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.1562500074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  2.2611971 0 -2.2611971 -2.2890517e-07 
		0 -3.1978133 -2.2611971 0 -2.2611971 -3.1978133 0 -1.5275322e-07 -2.2611971 0 2.2611971 
		-2.2890517e-07 0 3.1978133 2.2611971 0 2.2611971 3.1978133 0 -1.5275322e-07;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BranchSmallTree1";
	rename -uid "31B93615-2C42-3008-0567-308149C135F5";
	setAttr ".t" -type "double3" -499.57318304364139 477.45228093317712 -1215.6201592036775 ;
	setAttr ".r" -type "double3" -19.657651836070322 11.239390848330968 -48.347141580675249 ;
	setAttr ".s" -type "double3" 19.738448265983983 137.7873969836329 19.738448265983983 ;
createNode mesh -n "BranchSmallTree1Shape" -p "BranchSmallTree1";
	rename -uid "5ED173B4-6E4A-CC12-4F28-B7BD6CC3000C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape21" -p "BranchSmallTree1";
	rename -uid "56303A8F-514A-4E2E-F252-A788B990EEB9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.1562500074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  1.0999936 0 -1.0999936 -9.2722544e-08 
		0 -1.5556262 -1.0999936 0 -1.0999936 -1.5556262 0 -4.6361301e-08 -1.0999936 0 1.0999936 
		-9.2722544e-08 0 1.5556262 1.0999936 0 1.0999936 1.5556262 0 -4.6361301e-08;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopSmall3";
	rename -uid "BC739374-854D-4ED5-2F90-0AAF75D3C705";
	setAttr ".t" -type "double3" -338.44397541095998 671.97010821353581 -1243.1632810045603 ;
	setAttr ".s" -type "double3" 150.02982088141485 150.02982088141485 150.02982088141485 ;
createNode mesh -n "TreetopSmall3Shape" -p "TreetopSmall3";
	rename -uid "60908818-8D43-3DA0-65C9-8B970AC6885F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape20" -p "TreetopSmall3";
	rename -uid "9C56AFB2-F342-B9F4-0432-0E99797FF397";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 24 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 8 16 0
		 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0 16 24 0 17 25 0 18 26 0 19 27 0
		 20 28 0 21 29 0 22 30 0 23 31 0 32 0 0 32 1 0 32 2 0 32 3 0 32 4 0 32 5 0 32 6 0
		 32 7 0 24 33 0 25 33 0 26 33 0 27 33 0 28 33 0 29 33 0 30 33 0 31 33 0;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "SmallTree1";
	rename -uid "A826DC69-7F44-8FFD-55AE-26B5791BAF0B";
	setAttr ".t" -type "double3" 259.52720946576716 84.620857663352552 802.90151280049099 ;
	setAttr ".s" -type "double3" 14.08735476403279 43.57663180810659 14.08735476403279 ;
createNode mesh -n "SmallTree1Shape" -p "SmallTree1";
	rename -uid "2962951D-6146-D328-40AB-D7AB0CDB7D78";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape26" -p "SmallTree1";
	rename -uid "E945617F-DA4C-FD3A-9D8E-EF996FA1CA2A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.84375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  2.2611971 0 -2.2611971 -2.2890517e-07 
		0 -3.1978133 -2.2611971 0 -2.2611971 -3.1978133 0 -1.5275322e-07 -2.2611971 0 2.2611971 
		-2.2890517e-07 0 3.1978133 2.2611971 0 2.2611971 3.1978133 0 -1.5275322e-07 0.84235632 
		-1.110223e-16 -0.84235668 -7.1005402e-08 -1.110223e-16 -1.1912727 -0.84235692 -1.110223e-16 
		-0.84235668 -1.191273 -1.110223e-16 -3.550268e-08 -0.84235692 -1.110223e-16 0.84235668 
		-7.1005402e-08 -1.110223e-16 1.1912723 0.84235668 -1.110223e-16 0.84235668 1.191273 
		-1.110223e-16 -3.550268e-08;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "axe2";
	rename -uid "93662190-044A-C54F-9913-E5B0C9428129";
	setAttr ".t" -type "double3" 470.05385682621761 223.80545839900384 829.2150579210637 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 51.769517568538753 133.38167822483851 60.07704952338382 ;
	setAttr ".rp" -type "double3" -2.4158453015843406e-13 0 7.1054273576010019e-14 ;
	setAttr ".rpt" -type "double3" 34.890294729620308 0 -168.1693539654292 ;
	setAttr ".spt" -type "double3" -2.4158453015843406e-13 0 7.1054273576010019e-14 ;
createNode transform -n "transform19" -p "axe2";
	rename -uid "683BC38D-D440-DE76-3EA7-368226691DA3";
	setAttr ".v" no;
createNode mesh -n "Step3Shape" -p "transform19";
	rename -uid "6FCBC505-354F-C2FB-1228-238F7E1EE9E2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.49226052 0 0 0.49226052 
		0 0 -0.49226052 0 0 -0.49226052 0 1.8179417e-06 -0.43604842 0 -1.8179417e-06 -0.43604842 
		0 1.8179417e-06 0.43604842 0 -1.8179417e-06 0.43604842 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "axe1";
	rename -uid "DDC81200-6E4C-143A-8765-87B64D711127";
	setAttr ".t" -type "double3" 617.42328108281185 245.41938124599932 660.54614879587962 ;
	setAttr ".r" -type "double3" 89.999999999999929 90 0 ;
	setAttr ".s" -type "double3" 6.0837996068747646 99.59394891077325 6.0837996068747646 ;
createNode transform -n "transform20" -p "axe1";
	rename -uid "F551D959-6E4D-612F-676B-A09B1F974FB0";
	setAttr ".v" no;
createNode mesh -n "Column7Shape" -p "transform20";
	rename -uid "F1611449-7A43-A832-0780-CDA164D57E89";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "axe";
	rename -uid "33A70224-5045-FDFB-5451-52A8335FAF16";
	setAttr ".t" -type "double3" -247.44784603752021 -57.438232634171072 107.19871625963503 ;
	setAttr ".r" -type "double3" 2.1339458769996957 29.82280394671033 5.060560471502586 ;
	setAttr ".rp" -type "double3" 598.03831138257669 223.80545839900384 661.04570395563462 ;
	setAttr ".sp" -type "double3" 598.03831138257669 223.80545839900384 661.04570395563462 ;
createNode transform -n "polySurface6" -p "axe";
	rename -uid "C0B054DD-DB46-38E3-FA58-A3897435B712";
createNode mesh -n "polySurfaceShape15" -p "polySurface6";
	rename -uid "0D90FC43-7249-CCAC-7FF0-B78ED9D45CE9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface7" -p "axe";
	rename -uid "BFD652CB-7046-4264-7EBF-32925618AF72";
	setAttr ".rp" -type "double3" 504.94415283203125 223.80546569824219 661.04571533203125 ;
	setAttr ".sp" -type "double3" 504.94415283203125 223.80546569824219 661.04571533203125 ;
createNode mesh -n "polySurfaceShape16" -p "polySurface7";
	rename -uid "2544C361-0A4D-88BD-8F0C-E3B9B56B239B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform21" -p "axe";
	rename -uid "ED37EAD7-5B40-3B49-78E1-128E6AE19DC7";
	setAttr ".v" no;
createNode mesh -n "Column8Shape" -p "transform21";
	rename -uid "D03A0AC7-2D4E-B237-3CA3-3E8C9FE0E8E7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "paws";
	rename -uid "64CDE36A-AC4E-AD08-BF8B-04B43CFF8BEE";
	setAttr ".rp" -type "double3" 1338.318337586742 117.74562963303669 674.00430704537996 ;
	setAttr ".sp" -type "double3" 1338.318337586742 117.74562963303669 674.00430704537996 ;
createNode mesh -n "pawsShape" -p "paws";
	rename -uid "1B2C246A-C945-133D-A5A9-E2A36605E761";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "paws2";
	rename -uid "5FE895B8-EB4C-9080-F7F5-118C3431D693";
	setAttr ".rp" -type "double3" 1174.6169078343983 117.74562963303669 424.35993634477393 ;
	setAttr ".sp" -type "double3" 1174.6169078343983 117.74562963303669 424.35993634477393 ;
createNode mesh -n "paws2Shape" -p "paws2";
	rename -uid "435FBC1E-9A4D-72EE-1C4C-13BC5C9257FB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "table";
	rename -uid "EBB46BEB-AC4A-B224-24D9-81BED7EDB3AC";
	setAttr ".t" -type "double3" 807.73995049244377 276.97162430856838 -503.84773494821559 ;
	setAttr ".s" -type "double3" 127.05589930300309 404.86478036733666 322.96369763471091 ;
createNode mesh -n "tableShape" -p "table";
	rename -uid "A464BB55-EB4D-546A-5EF0-31AE71CF4030";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table";
	rename -uid "891370E8-4246-FC3E-63AF-6CA7B08A5919";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table1";
	rename -uid "6F76B702-3C42-6FE7-DA85-57B1D86CA89C";
	setAttr ".t" -type "double3" 851.07146009734106 227.03737698082134 -641.07249535508743 ;
	setAttr ".r" -type "double3" 270 0 0 ;
	setAttr ".s" -type "double3" 21.118922188171727 404.86478036733666 110.89128720493828 ;
createNode mesh -n "table1Shape" -p "table1";
	rename -uid "2062FF78-A548-C19E-7015-0EACD33FD70F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table1";
	rename -uid "53FC0430-7E4B-DD4B-4E57-8C9DF82C3FFC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table2";
	rename -uid "D5033CED-EC40-C89F-05A6-00AE033FB5B2";
	setAttr ".t" -type "double3" 769.25502425079412 229.1996005464261 -642.19265292586374 ;
	setAttr ".r" -type "double3" 270 0 0 ;
	setAttr ".s" -type "double3" 21.118922188171727 404.86478036733666 110.89128720493828 ;
createNode mesh -n "table2Shape" -p "table2";
	rename -uid "66BF1DAD-FB46-BEDD-A04E-F9858C213409";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table2";
	rename -uid "3BD4F9DA-0A48-2403-2843-A3B131F03917";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table3";
	rename -uid "12D91B0D-7145-E682-FE05-EAB413D792EF";
	setAttr ".t" -type "double3" 764.19293696658542 227.03737698082134 -375.83577393592321 ;
	setAttr ".r" -type "double3" 270 0 0 ;
	setAttr ".s" -type "double3" 21.118922188171727 404.86478036733666 110.89128720493828 ;
createNode mesh -n "table3Shape" -p "table3";
	rename -uid "FCF0D367-DE40-6A96-236D-F9B9370C4A6A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table3";
	rename -uid "899F3D75-9A40-1BB3-1F5F-8DA1CAECE0BB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table4";
	rename -uid "0F528764-8C45-9D90-A7B0-DC937E333598";
	setAttr ".t" -type "double3" 848.5135701048431 227.03737698082134 -375.31551484618598 ;
	setAttr ".r" -type "double3" 270 0 0 ;
	setAttr ".s" -type "double3" 21.118922188171727 404.86478036733666 110.89128720493828 ;
createNode mesh -n "table4Shape" -p "table4";
	rename -uid "4FCB8D4E-0D4F-0442-4422-FEB3AFB9E447";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table4";
	rename -uid "A561B57F-3941-3FA1-A39E-51B054B74F5D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "9C1DFFD6-B344-36D3-DF35-AC93D626719C";
	setAttr -s 25 ".lnk";
	setAttr -s 25 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "B2E00227-144B-1098-7B43-9F86FA516948";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "BB940923-A94F-C3E4-D8AD-B79D4BECF710";
createNode displayLayerManager -n "layerManager";
	rename -uid "54015AAD-D549-32ED-7736-F18AEDB32DC5";
createNode displayLayer -n "defaultLayer";
	rename -uid "F889A05C-014D-3B5D-AD75-1F92B0BECC5B";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "FBE31558-F54C-4D2D-0644-8F9424841558";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "F11D54A8-4447-1F20-A58B-C7A67CABC21B";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "A2D2E1E1-D648-7251-322D-6995CBB9BDDC";
	setAttr ".version" -type "string" "5.2.0";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "86F5A765-AB40-539D-1CD6-DE931906D92A";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "E45E70BA-7544-CBC7-FAE6-97ADA9C14B50";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "1116ACFD-E741-218C-380A-D691EB64E8E4";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode polyCube -n "polyCube2";
	rename -uid "F596A68A-3540-4867-A413-47BE89292AE8";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit1";
	rename -uid "ECC5ECE2-334C-3336-32CB-EEAEEB6F55DF";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483647 -2147483646 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "93331D53-154F-36D8-3204-9D9FC4A8B178";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -0.31733879 -0.16964675 0.53190702
		 0.31733879 -0.16964675 0.53190702 -0.31733879 0.16964675 0.53190702 0.31733879 0.16964675
		 0.53190702 -0.31733879 0.16964675 -0.53190702 0.31733879 0.16964675 -0.53190702 -0.31733879
		 -0.16964675 -0.53190702 0.31733879 -0.16964675 -0.53190702;
createNode polyCube -n "polyCube3";
	rename -uid "ED16507C-704D-05F9-4614-9E9C126B0B0D";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit2";
	rename -uid "2EAC52BC-BF4E-CBAE-0482-D3A49A86F323";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483638 -2147483637 -2147483641 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCube -n "polyCube4";
	rename -uid "74B84FC7-8342-139F-1C9D-1E83374B996D";
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "A8941C40-D447-DCA6-AAD1-E895497D73DC";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 100\n            -height 30\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 100\n            -height 30\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n"
		+ "            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 100\n            -height 30\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1119\n            -height 670\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n"
		+ "            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n"
		+ "            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n"
		+ "                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n"
		+ "                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n"
		+ "                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n"
		+ "                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1119\\n    -height 670\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1119\\n    -height 670\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "A6C36C6A-4A4E-B5AF-C537-EEAA0D1E3B15";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "EF539390-F44E-212A-2EEC-BEA4E0F2B438";
	setAttr ".sa" 8;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "3FC1FD71-7E4E-CA3A-74F6-9D8F6B70DB46";
	setAttr ".ics" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".ix" -type "matrix" 41.151281224598755 0 0 0 0 115.98442169280271 0 0 0 0 140.78885205848553 0
		 -92.825828248266475 726.58096585460521 290.48568867082111 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -72.250191 750.70398 290.48569 ;
	setAttr ".rs" 991016396;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -72.250187635967094 668.58875500820386 199.39481259101504 ;
	setAttr ".cbx" -type "double3" -72.250187635967094 832.81925434200798 381.5765647506272 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak6";
	rename -uid "CB15080E-E143-E7FC-C716-338A33D3E6CC";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[12:17]" -type "float3"  0 0.2038084 0 0 0.2038084
		 -0.18625367 0 -0.20380858 0 0 -0.084062666 -0.18625367 0 0.2038084 0.18625371 0 -0.084062666
		 0.18625371;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "1F8AEACC-2847-8B98-F93E-F796EAF9FD81";
	setAttr ".ics" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".ix" -type "matrix" 41.151281224598755 0 0 0 0 115.98442169280271 0 0 0 0 140.78885205848553 0
		 -92.825828248266475 726.58096585460521 290.48568867082111 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -72.250191 750.70404 290.48569 ;
	setAttr ".rs" 1162601877;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -72.250187635967094 692.22740938240918 225.61723575907752 ;
	setAttr ".cbx" -type "double3" -72.250187635967094 809.1806379904591 355.35411640755615 ;
	setAttr ".raf" no;
createNode polyCube -n "polyCube5";
	rename -uid "33CD5493-DA41-EE20-B5CA-6AA87643644C";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "DD984773-8D4A-4951-1F11-8798AC6CC2E4";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 76.424989907374268 0 0 0 0 76.424989907374268 0 0 0 0 76.424989907374268 0
		 386.3473545786029 447.77161702590081 467.38197221209487 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 398.98764 447.77161 468.08224 ;
	setAttr ".rs" 1785986103;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 398.28738583339094 393.52860716091908 419.6861177001951 ;
	setAttr ".cbx" -type "double3" 399.68789434875202 502.01462689088254 516.47834434992455 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak7";
	rename -uid "7C2C2E89-4E43-9FCF-DA14-B196631DB732";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0.34376797 -0.20975488 0.12408713
		 -0.3254427 -0.20975488 0.14241257 0.34376797 0.20975488 0.12408713 -0.3254427 0.20975488
		 0.14241257 0.3254427 0.20975488 -0.14241257 -0.34376797 0.20975488 -0.12408713 0.3254427
		 -0.20975488 -0.14241257 -0.34376797 -0.20975488 -0.12408713;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "BDD0F14A-1240-D890-3576-A8AD7ECF60E5";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 76.424989907374268 0 0 0 0 76.424989907374268 0 0 0 0 76.424989907374268 0
		 386.3473545786029 447.77161702590081 467.38197221209487 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 398.98764 447.77161 468.08221 ;
	setAttr ".rs" 804450133;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 398.55838197852518 409.04571419140075 433.52923516255464 ;
	setAttr ".cbx" -type "double3" 399.4168811213014 486.49751986040087 502.63519955585866 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak8";
	rename -uid "B01DBDF1-F84A-54E0-0282-44B507F486F5";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.0035460526 0.20303716 0.18113355
		 -0.0035460494 0.20303716 -0.1811334 0.0035460526 -0.20303716 0.18113355 -0.0035460494
		 -0.20303716 -0.1811334;
createNode polySplit -n "polySplit3";
	rename -uid "DE803D97-8742-8386-16C1-91B30A5A9273";
	setAttr -s 5 ".e[0:4]"  0.89999998 0.89999998 0.89999998 0.89999998
		 0.89999998;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483647 -2147483646 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "CBE30652-6640-6822-CEB9-1D8C36CE1F38";
	setAttr -s 5 ".e[0:4]"  0.1 0.1 0.1 0.1 0.1;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483647 -2147483646 -2147483645 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "1437582E-BD4A-500C-84A9-FC9D233A2CEF";
	setAttr -s 9 ".e[0:8]"  0.1 0.89999998 0.89999998 0.89999998 0.89999998
		 0.1 0.1 0.1 0.1;
	setAttr -s 9 ".d[0:8]"  -2147483642 -2147483638 -2147483621 -2147483629 -2147483637 -2147483641 
		-2147483631 -2147483623 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit6";
	rename -uid "9340709A-B04C-587C-3F5F-B1B0A9B9E0C3";
	setAttr -s 9 ".e[0:8]"  0.1 0.89999998 0.89999998 0.89999998 0.89999998
		 0.1 0.1 0.1 0.1;
	setAttr -s 9 ".d[0:8]"  -2147483638 -2147483620 -2147483613 -2147483614 -2147483615 -2147483637 
		-2147483629 -2147483621 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyPlane -n "polyPlane1";
	rename -uid "595C70F7-1B48-0870-A20C-5EB20E5E9DFE";
	setAttr ".cuv" 2;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "EB7EE43D-CC4E-B362-0D1E-A78A2B94F4B9";
	setAttr ".ics" -type "componentList" 10 "e[3]" "e[24]" "e[45]" "e[66]" "e[87]" "e[108]" "e[129]" "e[150]" "e[171]" "e[192]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "84F833A4-8D40-F4BD-A43A-D7AE0499F48A";
	setAttr ".ics" -type "componentList" 10 "e[17]" "e[36]" "e[55]" "e[74]" "e[93]" "e[112]" "e[131]" "e[150]" "e[169]" "e[188]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "DC2AE082-BC44-3B65-3943-26A3951EAD07";
	setAttr ".ics" -type "componentList" 10 "e[3]" "e[20]" "e[37]" "e[54]" "e[71]" "e[88]" "e[105]" "e[122]" "e[139]" "e[156]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "7139FB28-494C-1D39-1748-9AAF50FCDFEF";
	setAttr ".ics" -type "componentList" 10 "e[12]" "e[27]" "e[42]" "e[57]" "e[72]" "e[87]" "e[102]" "e[117]" "e[132]" "e[147]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge5";
	rename -uid "6C5A5C1D-2F4C-DC9A-5D1D-5DB203562A5F";
	setAttr ".ics" -type "componentList" 10 "e[3]" "e[16]" "e[29]" "e[42]" "e[55]" "e[68]" "e[81]" "e[94]" "e[107]" "e[120]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge6";
	rename -uid "57557437-504E-FAF0-F154-3B9A2330CAE6";
	setAttr ".ics" -type "componentList" 10 "e[8]" "e[19]" "e[30]" "e[41]" "e[52]" "e[63]" "e[74]" "e[85]" "e[96]" "e[107]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge7";
	rename -uid "27F96D08-FF49-7F88-369E-5A8824E69417";
	setAttr ".ics" -type "componentList" 10 "e[3]" "e[12]" "e[21]" "e[30]" "e[39]" "e[48]" "e[57]" "e[66]" "e[75]" "e[84]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge8";
	rename -uid "E9707C05-8145-D90E-0865-09BCD3CA6815";
	setAttr ".ics" -type "componentList" 10 "e[4]" "e[11]" "e[18]" "e[25]" "e[32]" "e[39]" "e[46]" "e[53]" "e[60]" "e[67]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge9";
	rename -uid "8EE21902-AA4C-5971-9CBB-76A5EAC5F49D";
	setAttr ".ics" -type "componentList" 2 "e[46]" "e[48]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge10";
	rename -uid "D737B353-764A-04EA-C9AC-C5BE79C8F367";
	setAttr ".ics" -type "componentList" 2 "e[41]" "e[43]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge11";
	rename -uid "2CF737B3-6740-A3FE-281D-59BAF05A7B35";
	setAttr ".ics" -type "componentList" 2 "e[36]" "e[38]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge12";
	rename -uid "7114413B-F740-DAF7-7536-ECAE18A1F54A";
	setAttr ".ics" -type "componentList" 2 "e[31]" "e[33]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge13";
	rename -uid "F9B82308-8043-B16D-912E-5EA08093BCF6";
	setAttr ".ics" -type "componentList" 2 "e[26]" "e[28]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge14";
	rename -uid "A3217B6D-E14A-07A1-893F-059F7DB6EB88";
	setAttr ".ics" -type "componentList" 2 "e[21]" "e[23]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge15";
	rename -uid "285DF425-A442-9859-2DBC-6A827510919C";
	setAttr ".ics" -type "componentList" 2 "e[16]" "e[18]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge16";
	rename -uid "FC542000-2D4A-2C23-AB59-9599974E988A";
	setAttr ".ics" -type "componentList" 2 "e[11]" "e[13]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge17";
	rename -uid "90111912-FB48-7974-0CC0-E2B1BE7DC993";
	setAttr ".ics" -type "componentList" 2 "e[6]" "e[8]";
	setAttr ".cv" yes;
createNode groupId -n "groupId3";
	rename -uid "B3D1F6F9-D14B-67A8-A565-BD80ABA7D496";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "6104FACA-D64E-3EBC-79CE-87B8E4BE2034";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1]";
createNode groupId -n "groupId4";
	rename -uid "8F45E6B2-1849-376D-5B07-0AB7D3DB746B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "0F211D20-6342-383A-EBD5-B2A32070E9F0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "65ADE586-0B4B-9FE3-5E76-7FBF27D81993";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "149FEF59-FE4E-5B11-8323-B6A474E7426C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:21]";
createNode groupId -n "groupId7";
	rename -uid "A8E8F6A9-D340-8559-9A72-AC81EBE1523C";
	setAttr ".ihi" 0;
createNode polyUnite -n "polyUnite1";
	rename -uid "AD1A7FCF-8748-674B-7FF9-CB93DDC70850";
	setAttr -s 3 ".ip";
	setAttr -s 3 ".im";
createNode polyDelEdge -n "polyDelEdge18";
	rename -uid "D9696404-AF41-6F96-BD13-778F6DCA8ED8";
	setAttr ".ics" -type "componentList" 1 "e[54]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge19";
	rename -uid "C070FE15-2F40-D478-C8F4-748461E3AC02";
	setAttr ".ics" -type "componentList" 1 "e[35]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge20";
	rename -uid "9DDBC565-CE48-360A-5232-C7AA26870A7F";
	setAttr ".ics" -type "componentList" 1 "e[51]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge21";
	rename -uid "DC9A212B-3C4E-6D08-2D6E-0EBE6CB3E95C";
	setAttr ".ics" -type "componentList" 1 "e[34]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge22";
	rename -uid "86904EA0-4140-156F-B299-949185EB82BA";
	setAttr ".ics" -type "componentList" 1 "e[49]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge23";
	rename -uid "AC46E5C2-5A41-F9F3-C90F-AB8E85F9AD46";
	setAttr ".ics" -type "componentList" 1 "e[25]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge24";
	rename -uid "38A911C5-EF44-2A5B-A6D4-2B926614575F";
	setAttr ".ics" -type "componentList" 1 "e[33:39]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge25";
	rename -uid "4F255B97-E546-F871-E06E-C5A567E83A35";
	setAttr ".ics" -type "componentList" 2 "e[15:17]" "e[30]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge26";
	rename -uid "692AEEED-2D4F-9FB5-0A7A-71B81EC376E2";
	setAttr ".ics" -type "componentList" 1 "e[24:27]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge27";
	rename -uid "724AC6CC-9840-107B-7F0E-18AB6D8E0608";
	setAttr ".ics" -type "componentList" 2 "e[13:14]" "e[18]";
	setAttr ".cv" yes;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "42F44F9E-C848-6FEF-5442-B3BA5F234E85";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 275.00798638603379 0 0 0 0 275.00798638603379 0 0 0 0 275.00798638603379 0
		 2012.1313395864904 1047.8075030813379 383.72716943775725 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2012.1313 1185.3115 383.72717 ;
	setAttr ".rs" 2108750532;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1874.6273463934735 1185.3114962743548 246.22317624474036 ;
	setAttr ".cbx" -type "double3" 2149.6353327795073 1185.3114962743548 521.23116263077418 ;
	setAttr ".raf" no;
createNode polyCylinder -n "polyCylinder4";
	rename -uid "6B27B152-B044-35B9-804C-6C9BB8B80E53";
	setAttr ".sa" 10;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode groupId -n "groupId8";
	rename -uid "61C9F3B7-3D44-EB04-49DA-BFAE2E289478";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "2BD43C56-5D48-82CC-DD84-FDB638BA6E73";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "ED89DA51-C441-0EC1-8A75-28B51733A67C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "9BCCFDE5-8842-5E7E-BCC4-01B7D447716C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "3A68F5FC-CB43-3395-7C73-D8850134F1F9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "BF8A9109-7D4A-EB77-E298-2C8D47EAE1A4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:29]";
createNode groupId -n "groupId13";
	rename -uid "53F62F72-8440-A3B2-C7DF-7C87AC120C8D";
	setAttr ".ihi" 0;
createNode polyUnite -n "polyUnite2";
	rename -uid "F21E6ABB-4642-5367-3A31-A7B24B464FA3";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId16";
	rename -uid "C1B4B072-F74C-B761-FC8A-219B59239C68";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "B712FCCD-E349-5C82-DD24-F892885D85BF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "39CA4BB7-CA46-3CD3-4CB1-A5B79B5F0E1B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "8F529536-E448-5EDF-2105-E39D6100A7DA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:39]";
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "2BDE52D1-FC44-9695-9DE6-62ABFA3B6F72";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[9]" -type "float2" -4.9471882e-06 1.8787572e-06 ;
	setAttr ".uvtk[19]" -type "float2" 1.5942803e-13 3.2724969e-09 ;
	setAttr ".uvtk[73]" -type "float2" 0.0039191931 -2.3564451e-06 ;
createNode polyMergeVert -n "polyMergeVert21";
	rename -uid "F0AE7C2D-764D-994E-DAB5-588FD49D9113";
	setAttr ".ics" -type "componentList" 2 "vtx[9]" "vtx[42]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak29";
	rename -uid "DC87CE60-F94C-8027-0101-DA9E4FD244F4";
	setAttr ".uopa" yes;
	setAttr ".tk[42]" -type "float3"  40.37145996 6.1035156e-05 -3.15802002;
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "D33E7071-1149-47D7-12AF-889AB42F8190";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[8]" -type "float2" -4.0800276e-07 1.2323964e-06 ;
	setAttr ".uvtk[18]" -type "float2" -2.8288483e-13 3.2701708e-09 ;
	setAttr ".uvtk[71]" -type "float2" -0.0034501541 -4.5401125e-06 ;
createNode polyMergeVert -n "polyMergeVert22";
	rename -uid "54948EF4-DE41-F5EA-15A2-80AC6F7315BF";
	setAttr ".ics" -type "componentList" 2 "vtx[8]" "vtx[41]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak30";
	rename -uid "5D7B2F62-0C4A-C362-A6D4-DA9BC58A4EDA";
	setAttr ".uopa" yes;
	setAttr ".tk[41]" -type "float3"  28.20874023 6.1035156e-05 23.34710693;
createNode polyTweakUV -n "polyTweakUV23";
	rename -uid "05C3506E-ED4B-6D5F-EDD8-60A477D78C1D";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[7]" -type "float2" 6.685982e-06 1.6296328e-06 ;
	setAttr ".uvtk[17]" -type "float2" -9.527934e-13 3.2713752e-09 ;
	setAttr ".uvtk[67]" -type "float2" -0.0039034551 2.2245256e-06 ;
createNode polyMergeVert -n "polyMergeVert23";
	rename -uid "B6B5E46D-BE4D-54C2-A67B-77B9DFD505BE";
	setAttr ".ics" -type "componentList" 2 "vtx[7]" "vtx[40]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak31";
	rename -uid "87A95DE3-5C46-DB11-E1C5-F0822693A07E";
	setAttr ".uopa" yes;
	setAttr ".tk[40]" -type "float3"  5.2713623 6.1035156e-05 40.93444824;
createNode polyTweakUV -n "polyTweakUV24";
	rename -uid "D496D601-AF46-E2DF-286D-BE95D14A1741";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 8.0708687e-06 9.8954149e-07 ;
	setAttr ".uvtk[10]" -type "float2" -1.3011835e-06 -6.1217811e-06 ;
	setAttr ".uvtk[20]" -type "float2" 1.5878019e-05 7.6981341e-06 ;
	setAttr ".uvtk[77]" -type "float2" -0.0047904816 -4.4248313e-06 ;
createNode polyMergeVert -n "polyMergeVert24";
	rename -uid "85BEF16D-0C46-5B1D-5FC3-0CB5C6B60D3E";
	setAttr ".ics" -type "componentList" 2 "vtx[0]" "vtx[40]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak32";
	rename -uid "6D3B6C8A-F64A-BDA4-7EAA-DEA33713D9D1";
	setAttr ".uopa" yes;
	setAttr ".tk[40]" -type "float3"  37.11364746 6.1035156e-05 -28.45690918;
createNode polyTweakUV -n "polyTweakUV25";
	rename -uid "A2CC7844-1C4C-F722-5AB4-56901F89F6A0";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" -3.3368906e-06 -8.7286071e-07 ;
	setAttr ".uvtk[11]" -type "float2" -1.6391269e-07 -7.6041312e-07 ;
	setAttr ".uvtk[79]" -type "float2" 0.0047679367 7.7676596e-06 ;
createNode polyMergeVert -n "polyMergeVert25";
	rename -uid "CBEF42A7-9F44-38BE-7BC3-86991F1CFACD";
	setAttr ".ics" -type "componentList" 2 "vtx[1]" "vtx[33]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak33";
	rename -uid "B83CD974-5149-982A-BEBB-D6B3ACDD481F";
	setAttr ".uopa" yes;
	setAttr ".tk[33]" -type "float3"  19.6796875 6.1035156e-05 -42.88616943;
createNode polyTweakUV -n "polyTweakUV26";
	rename -uid "C6CAFA64-CA41-341B-54C1-ADBBFCC46470";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" 5.9668828e-08 2.2055774e-08 ;
	setAttr ".uvtk[12]" -type "float2" -1.8626334e-08 -9.3586827e-08 ;
	setAttr ".uvtk[47]" -type "float2" -0.0038914667 -3.8967499e-07 ;
createNode polyMergeVert -n "polyMergeVert26";
	rename -uid "B82AA876-844E-320D-C771-47BCC3FA4616";
	setAttr ".ics" -type "componentList" 2 "vtx[2]" "vtx[34]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak34";
	rename -uid "CEE71C3C-4D44-31AE-E743-4FB7F8F5AE27";
	setAttr ".uopa" yes;
	setAttr ".tk[34]" -type "float3"  -5.2713623 6.1035156e-05 -40.93444824;
createNode polyTweakUV -n "polyTweakUV27";
	rename -uid "308EFC4D-884F-6EEB-0E85-6B833E672E45";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" 2.173442e-06 8.1209703e-07 ;
	setAttr ".uvtk[13]" -type "float2" -3.7254035e-09 -7.9048297e-09 ;
	setAttr ".uvtk[51]" -type "float2" -0.0034497804 -4.4789986e-06 ;
createNode polyMergeVert -n "polyMergeVert27";
	rename -uid "D63F83FA-CF41-CD7F-8592-1BA647A378BA";
	setAttr ".ics" -type "componentList" 2 "vtx[3]" "vtx[34]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak35";
	rename -uid "134E14A0-4047-38C5-DC01-C28BBB00634D";
	setAttr ".uopa" yes;
	setAttr ".tk[34]" -type "float3"  -28.20874023 6.1035156e-05 -23.34710693;
createNode polyTweakUV -n "polyTweakUV28";
	rename -uid "826E80FE-EE48-C94C-10CF-AD936AA0BB02";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" -8.9001338e-07 -1.8523176e-07 ;
	setAttr ".uvtk[14]" -type "float2" -1.1812773e-13 3.2708529e-09 ;
	setAttr ".uvtk[55]" -type "float2" -0.0039203335 -1.9833824e-06 ;
createNode polyMergeVert -n "polyMergeVert28";
	rename -uid "8D674B13-9B4D-EDAB-BF22-F7AE10B964E6";
	setAttr ".ics" -type "componentList" 2 "vtx[4]" "vtx[34]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak36";
	rename -uid "234E178E-224C-9AB9-A6BC-799E377D4B7C";
	setAttr ".uopa" yes;
	setAttr ".tk[34]" -type "float3"  -40.37145996 6.1035156e-05 3.15802002;
createNode polyTweakUV -n "polyTweakUV29";
	rename -uid "7A1EF080-1D40-A418-6EDF-9591AA3508EE";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 5.221139e-06 1.2240806e-06 ;
	setAttr ".uvtk[15]" -type "float2" -6.0562666e-14 3.2710341e-09 ;
	setAttr ".uvtk[59]" -type "float2" -0.0047942465 -4.7631315e-06 ;
createNode polyMergeVert -n "polyMergeVert29";
	rename -uid "FA7A3113-0941-9760-7064-26A17B42146A";
	setAttr ".ics" -type "componentList" 2 "vtx[5]" "vtx[34]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak37";
	rename -uid "F2CD3F60-D248-D0A3-985A-51BC4ED7FAB0";
	setAttr ".uopa" yes;
	setAttr ".tk[34]" -type "float3"  -37.11364746 6.1035156e-05 28.45690918;
createNode polyTweakUV -n "polyTweakUV30";
	rename -uid "AAD437F8-8046-0FDD-DD5E-D0925C32A01B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[6]" -type "float2" 3.592035e-06 4.2631282e-06 ;
	setAttr ".uvtk[16]" -type "float2" 2.1038726e-13 3.2714649e-09 ;
	setAttr ".uvtk[63]" -type "float2" -0.0047770943 -2.9432645e-06 ;
createNode polyMergeVert -n "polyMergeVert30";
	rename -uid "70470E03-2B47-128F-6C54-9683995581A7";
	setAttr ".ics" -type "componentList" 2 "vtx[6]" "vtx[34]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak38";
	rename -uid "15713F97-A141-28F4-75FA-579ADEB7E508";
	setAttr ".uopa" yes;
	setAttr ".tk[34]" -type "float3"  -19.6796875 6.1035156e-05 42.88623047;
createNode polyUnite -n "polyUnite3";
	rename -uid "7C9BE35C-9942-00C6-9D43-B295050C15C7";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId21";
	rename -uid "A774178D-BD47-D4F0-6963-40835A71CD16";
	setAttr ".ihi" 0;
createNode polyTweakUV -n "polyTweakUV31";
	rename -uid "E793CFD8-F343-6084-CB25-678A4A825E51";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[68]" -type "float2" -0.0034501557 4.5367351e-06 ;
	setAttr ".uvtk[97]" -type "float2" -1.4663826e-12 -3.2731755e-09 ;
	setAttr ".uvtk[122]" -type "float2" 3.0737792e-06 -4.7834742e-06 ;
createNode polyMergeVert -n "polyMergeVert31";
	rename -uid "5D87E97F-3043-EAE0-AECB-ADB1A238F936";
	setAttr ".ics" -type "componentList" 2 "vtx[30]" "vtx[52]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak39";
	rename -uid "CA3C0616-EA4D-A038-6F89-8880BCD58F2A";
	setAttr ".uopa" yes;
	setAttr ".tk[30]" -type "float3"  28.20874023 0 23.34710693;
createNode polyTweakUV -n "polyTweakUV32";
	rename -uid "ABFBF47E-EC4A-2312-3B6F-A5BC05C31653";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[64]" -type "float2" -0.0039034486 1.1153074e-05 ;
	setAttr ".uvtk[95]" -type "float2" 6.0551564e-13 -3.2707108e-09 ;
	setAttr ".uvtk[121]" -type "float2" -1.0686933e-06 2.4763276e-06 ;
createNode polyMergeVert -n "polyMergeVert32";
	rename -uid "62CD9098-3545-D130-6AFD-E187075C0194";
	setAttr ".ics" -type "componentList" 2 "vtx[29]" "vtx[51]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak40";
	rename -uid "C03E76D2-4342-7C8C-3477-828D04EC4150";
	setAttr ".uopa" yes;
	setAttr ".tk[29]" -type "float3"  5.2713623 0 40.93444824;
createNode polyTweakUV -n "polyTweakUV33";
	rename -uid "1A07A746-D249-6EA6-8DE1-7ABE7D07A299";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[72]" -type "float2" 0.0039191842 6.6825264e-06 ;
	setAttr ".uvtk[99]" -type "float2" -3.3840708e-12 -3.2718779e-09 ;
	setAttr ".uvtk[123]" -type "float2" -4.2510142e-06 4.9298787e-06 ;
createNode polyMergeVert -n "polyMergeVert33";
	rename -uid "53636CCC-BC4B-6841-43BA-D4AAB6E72287";
	setAttr ".ics" -type "componentList" 2 "vtx[31]" "vtx[51]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak41";
	rename -uid "2BF3B942-764F-8B14-03B8-F8B75C24819E";
	setAttr ".uopa" yes;
	setAttr ".tk[31]" -type "float3"  40.37145996 0 -3.15802002;
createNode polyTweakUV -n "polyTweakUV34";
	rename -uid "881A83E9-8C41-47A4-ED6E-98BCD06D5EA3";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[56]" -type "float2" -0.0047942377 3.3419904e-06 ;
	setAttr ".uvtk[91]" -type "float2" 3.0953018e-13 -3.2709409e-09 ;
	setAttr ".uvtk[119]" -type "float2" -1.3780444e-06 -3.3511922e-06 ;
createNode polyMergeVert -n "polyMergeVert34";
	rename -uid "7D55FEC9-6F47-2A81-727C-95AA6A1A8F86";
	setAttr ".ics" -type "componentList" 2 "vtx[27]" "vtx[49]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak42";
	rename -uid "3C28AE72-7942-263D-1890-EAB7C497FB05";
	setAttr ".uopa" yes;
	setAttr ".tk[27]" -type "float3"  -37.11364746 0 28.45690918;
createNode polyTweakUV -n "polyTweakUV35";
	rename -uid "6A08B844-394C-DD86-2F89-8796470693E6";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[60]" -type "float2" -0.0047771037 5.669337e-06 ;
	setAttr ".uvtk[93]" -type "float2" 1.2478907e-13 -3.2714134e-09 ;
	setAttr ".uvtk[120]" -type "float2" -1.1602393e-06 -8.268064e-07 ;
createNode polyMergeVert -n "polyMergeVert35";
	rename -uid "A4463B3A-0E43-CF1C-D1F9-598DE0F8B1A5";
	setAttr ".ics" -type "componentList" 2 "vtx[28]" "vtx[49]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak43";
	rename -uid "4DDCB3B8-B946-5B88-CC0B-DE9B7266FE6B";
	setAttr ".uopa" yes;
	setAttr ".tk[28]" -type "float3"  -19.6796875 0 42.88623047;
createNode polyTweakUV -n "polyTweakUV36";
	rename -uid "7F009947-644F-3828-680C-53A9F174183C";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[74]" -type "float2" -0.0047904863 4.8043003e-06 ;
	setAttr ".uvtk[83]" -type "float2" 4.5320771e-06 7.1421473e-06 ;
	setAttr ".uvtk[101]" -type "float2" -3.8753424e-06 5.8220223e-07 ;
	setAttr ".uvtk[113]" -type "float2" -3.9753927e-06 -2.619696e-06 ;
createNode polyMergeVert -n "polyMergeVert36";
	rename -uid "1103086F-B845-B5A5-C495-5DA969581E8F";
	setAttr ".ics" -type "componentList" 2 "vtx[32]" "vtx[44]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak44";
	rename -uid "33408877-534B-E79B-8C3E-7DBA028FABC9";
	setAttr ".uopa" yes;
	setAttr ".tk[32]" -type "float3"  37.11364746 0 -28.45690918;
createNode polyTweakUV -n "polyTweakUV37";
	rename -uid "99E78BA8-F54D-1933-1728-3490371C49A5";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[78]" -type "float2" 0.0047679543 1.6964454e-05 ;
	setAttr ".uvtk[82]" -type "float2" 5.6624413e-07 8.9079845e-07 ;
	setAttr ".uvtk[114]" -type "float2" 1.3029178e-06 2.3909031e-06 ;
createNode polyMergeVert -n "polyMergeVert37";
	rename -uid "C28F06AF-CA4A-7BEA-C5D9-2482A74F9554";
	setAttr ".ics" -type "componentList" 2 "vtx[22]" "vtx[44]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak45";
	rename -uid "26D14DD9-CE42-5FA6-49EE-07B934311D95";
	setAttr ".uopa" yes;
	setAttr ".tk[22]" -type "float3"  19.6796875 0 -42.88616943;
createNode polyTweakUV -n "polyTweakUV38";
	rename -uid "FAD25323-5346-C013-4084-62896C1D7DDD";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[44]" -type "float2" -0.003891468 8.7987164e-06 ;
	setAttr ".uvtk[85]" -type "float2" 7.0780288e-08 1.0848714e-07 ;
	setAttr ".uvtk[116]" -type "float2" -5.4033121e-06 -5.4920192e-06 ;
createNode polyMergeVert -n "polyMergeVert38";
	rename -uid "5DD708CA-C942-293E-E90D-80920F962F62";
	setAttr ".ics" -type "componentList" 2 "vtx[24]" "vtx[44]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak46";
	rename -uid "E8F59577-144E-1A95-8B25-749BDA4EA0D5";
	setAttr ".uopa" yes;
	setAttr ".tk[24]" -type "float3"  -5.2713623 0 -40.93444824;
createNode polyTweakUV -n "polyTweakUV39";
	rename -uid "7E5B44E5-C542-C0E8-26E6-C7AD8E003CD8";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[48]" -type "float2" -0.0034497678 4.7451904e-06 ;
	setAttr ".uvtk[87]" -type "float2" 7.4496653e-09 1.1629584e-08 ;
	setAttr ".uvtk[117]" -type "float2" -3.6133206e-06 3.3296012e-06 ;
createNode polyMergeVert -n "polyMergeVert39";
	rename -uid "48EAFD17-1540-BAF6-8F42-C8B2004ACC17";
	setAttr ".ics" -type "componentList" 2 "vtx[25]" "vtx[44]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak47";
	rename -uid "CD9567EF-864E-56E5-4890-929EB308756B";
	setAttr ".uopa" yes;
	setAttr ".tk[25]" -type "float3"  -28.20874023 0 -23.34710693;
createNode polyTweakUV -n "polyTweakUV40";
	rename -uid "A3AD5B54-B942-7700-1002-67AB6F59CB66";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[52]" -type "float2" -0.0039203465 5.2522005e-06 ;
	setAttr ".uvtk[89]" -type "float2" -1.1982082e-12 -3.2713903e-09 ;
	setAttr ".uvtk[118]" -type "float2" 1.1992113e-06 9.2831506e-06 ;
createNode polyMergeVert -n "polyMergeVert40";
	rename -uid "AC3F462A-4F40-FB0E-2BF8-019148E1295C";
	setAttr ".ics" -type "componentList" 2 "vtx[26]" "vtx[44]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak48";
	rename -uid "5A83C187-3448-72BA-A069-9CAFCC42FC2E";
	setAttr ".uopa" yes;
	setAttr ".tk[26]" -type "float3"  -40.37145996 0 3.15802002;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "E000229C-804F-F1D4-D4A1-18AA110DDD9E";
	setAttr ".ics" -type "componentList" 1 "f[40:49]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 0 1769.3735104583357 89.117944619788787 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1747.1108 840.12781 802.15851 ;
	setAttr ".rs" 183461273;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1578.3699951171875 679.64572725521089 775.39193143619525 ;
	setAttr ".cbx" -type "double3" 1915.8515625 1000.6098385833358 828.92513456119525 ;
	setAttr ".raf" no;
createNode polyDelEdge -n "polyDelEdge28";
	rename -uid "0CDA5718-9D45-D0FE-8810-288A25291A92";
	setAttr ".ics" -type "componentList" 1 "e[81:90]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak49";
	rename -uid "77FBEE92-A449-C179-F84E-B08014A9AADA";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[46:65]" -type "float3"  36.36290359 -7.12974405 -26.41915131
		 13.88940239 -7.12974405 -42.74708557 13.88940239 7.12974405 -42.74708557 36.36290359
		 7.12974405 -26.41915131 -13.88938236 -7.12974405 -42.74708557 -13.88938236 7.12974405
		 -42.74708557 -36.36283875 -7.12974405 -26.41915131 -36.36283875 7.12974405 -26.41915131
		 -44.94701767 -7.12974405 0 -44.94701767 7.12974405 0 -36.36283875 -7.12974405 26.41917419
		 -36.36283875 7.12974405 26.41917419 -13.88938236 -7.12974405 42.74710464 -13.88938236
		 7.12974405 42.74710464 13.88940239 -7.12974405 42.74710464 13.88940239 7.12974405
		 42.74710464 36.36290359 -7.12974405 26.41917419 36.36290359 7.12974405 26.41917419
		 44.94701767 -7.12974405 0 44.94701767 7.12974405 0;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "154865C5-564F-B9BD-94CB-9186B0C0D737";
	setAttr ".ics" -type "componentList" 1 "f[0:9]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0
		 0 1769.3735104583357 89.117944619788787 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1747.1108 840.12781 1056.3329 ;
	setAttr ".rs" 746032885;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1578.3699951171875 679.64572725521089 1029.5663088776016 ;
	setAttr ".cbx" -type "double3" 1915.8515625 1000.6098385833359 1083.0995120026016 ;
	setAttr ".raf" no;
createNode polyDelEdge -n "polyDelEdge29";
	rename -uid "FCF820A8-104B-816A-5F13-79A35CE20557";
	setAttr ".ics" -type "componentList" 1 "e[10:19]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak50";
	rename -uid "0D37A3CE-FF4F-6682-C2C3-00B83B5CA93B";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[56:75]" -type "float3"  34.84999847 -6.83310413 -25.31998825
		 13.31153202 -6.83310413 -40.96862793 13.31153202 6.83310413 -40.96862793 34.84999847
		 6.83310413 -25.31998825 -13.31149864 -6.83310413 -40.96862793 -13.31149864 6.83310413
		 -40.96862793 -34.84996414 -6.83310413 -25.31998825 -34.84996414 6.83310413 -25.31998825
		 -43.076942444 -6.83310413 0 -43.076942444 6.83310413 0 -34.84996414 -6.83310413 25.31998825
		 -34.84996414 6.83310413 25.31998825 -13.31149864 -6.83310413 40.96862411 -13.31149864
		 6.83310413 40.96862411 13.31153202 -6.83310413 40.96862411 13.31153202 6.83310413
		 40.96862411 34.84999847 -6.83310413 25.31998825 34.84999847 6.83310413 25.31998825
		 43.076942444 -6.83310413 0 43.076942444 6.83310413 0;
createNode polyUnite -n "polyUnite4";
	rename -uid "4F604E29-864B-B3C9-5ABB-799BB14FE63D";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId22";
	rename -uid "E5ECD974-A545-A9D0-8637-C789B6AF3497";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "329C8297-6F42-E557-BC52-8095BCFEA53D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "F39D40CB-7845-0323-774F-0186E684822E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "31EC97BB-1B41-CAB9-045E-51A365B3E5D8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "4833C5A5-3F4B-DBA1-E5F4-DD9F581922ED";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "4516B6F9-7642-2539-B7E2-CBA427EA6CEB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:11]";
createNode polyUnite -n "polyUnite5";
	rename -uid "7A3469FD-E441-361F-C6FF-E2A23E22B98C";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId27";
	rename -uid "C28098FD-2546-FCE2-CCF8-B8BE442A2E71";
	setAttr ".ihi" 0;
createNode groupId -n "groupId28";
	rename -uid "12AC9476-7446-A09A-C185-47BD3FC4C3F0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId29";
	rename -uid "C5430D21-9E4D-A745-4EAF-0CB266CFFFF3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId30";
	rename -uid "F0CBF4FC-A841-04E1-4879-E3881EF76E3D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId31";
	rename -uid "E7E5B9D0-A74D-0F05-7A07-4AB87F1C4F61";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "10852FF2-9348-F5F4-DB29-FB8D7B3D8086";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:11]";
createNode polyUnite -n "polyUnite6";
	rename -uid "2F7BCFD2-1A4D-A76A-A672-EF833C8BBA2E";
	setAttr -s 3 ".ip";
	setAttr -s 3 ".im";
createNode groupId -n "groupId32";
	rename -uid "B753E779-B54F-B607-A609-75AA57FE23EA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "162D1D7E-9F43-8250-901F-6ABAB9DB66B3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:23]";
createNode groupId -n "groupId33";
	rename -uid "5FA94378-414F-FA1A-C155-46BEB9155399";
	setAttr ".ihi" 0;
createNode groupId -n "groupId34";
	rename -uid "FA2C2E46-CB48-455E-412F-A7BD17D1E869";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "5974B575-6142-0CB5-EDE8-2DBD25CA2B9B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:47]";
createNode polySphere -n "polySphere1";
	rename -uid "7FCDC4E3-6545-5F37-1FB4-66ADC4DBAD87";
	setAttr ".sa" 8;
	setAttr ".sh" 5;
createNode polyTriangulate -n "polyTriangulate1";
	rename -uid "5C5AB1FF-A844-1484-E960-6C95FE40B757";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyQuad -n "polyQuad1";
	rename -uid "E6E3FF55-2E49-AECE-14C2-689CA155BD74";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:759]";
	setAttr ".ix" -type "matrix" 607.16691376561016 0 0 0 0 607.16691376561016 0 0 0 0 607.16691376561016 0
		 65.534764110856486 -55.543166558152279 -2515.5806325217472 1;
	setAttr ".ws" yes;
createNode groupId -n "groupId14";
	rename -uid "60157B33-2C46-4274-BE08-4E94928049E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "197F1058-9E4C-F0AE-782D-E892302DA490";
	setAttr ".ihi" 0;
createNode polySeparate -n "polySeparate1";
	rename -uid "9006752D-0A49-B032-B0F0-93AF9E65C975";
	setAttr ".ic" 5;
	setAttr -s 5 ".out";
createNode groupId -n "groupId36";
	rename -uid "A31591D6-F441-EB1F-28A1-9FABDD4BA769";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "C6A16931-DC44-A081-F86C-03AE57757487";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId37";
	rename -uid "F723366C-7340-EF7C-0F85-9CB1849FA458";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "5879B15E-E84B-0DD7-8A72-1BA4BC6E788D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId38";
	rename -uid "696574F9-B647-C5DF-EBA9-95B5EAFDA8BF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts13";
	rename -uid "BB61C632-3547-6108-0570-F79A93D0D4AA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId39";
	rename -uid "C4F797F2-9842-F05B-9CE5-B9978479887E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "DFB1AE60-D64F-5791-94B0-408E480D5CFA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "2977FFF9-B44B-384D-4F4C-EC909F1A3581";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 298.91959849240834 0 0 0 0 298.91959849240834 0 0 0 0 298.91959849240834 0
		 143.02510776088042 980.31497425171665 -823.28832922587344 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "CCC4D537-B943-5DFF-1975-5ABAFE09CEFB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 396.22629164834768 0 0 0 0 396.22629164834768 0 0 0 0 396.22629164834768 0
		 -578.59715287906863 1399.6144078986999 -1160.529435296869 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "2BDF55F2-1C4D-8B0C-9B32-E3AA5E02FF07";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 150.02982088141485 0 0 0 0 150.02982088141485 0 0 0 0 150.02982088141485 0
		 -867.2022558346398 934.36137398170706 -1105.4883910337539 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "137AC074-C740-01DF-805A-E7B696AF562F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 150.02982088141485 0 0 0 0 150.02982088141485 0 0 0 0 150.02982088141485 0
		 346.02559682869492 602.76606440152 -928.95699943781733 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "F3023942-CA44-DC35-947C-C5B1A2D56219";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 150.02982088141485 0 0 0 0 150.02982088141485 0 0 0 0 150.02982088141485 0
		 -311.83264359975885 849.54603473811767 -1245.4572635720426 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge6";
	rename -uid "4347D0E7-384E-89F0-A826-B88C426D078A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 12.866890517338296 -14.465426192835837 -3.8471955270527736 0
		 90.948213807448056 92.988917487211594 -45.462626379439605 0 7.369199477417717 1.7060128868634792 18.231586849224296 0
		 -499.57318304364139 703.20870071477918 -1215.6201592036775 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge7";
	rename -uid "81ED6177-6644-17CF-19E9-C3B33EE5B242";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" -10.024826038956597 -12.91968395240044 -2.4310379934742175 0
		 -104.54252182461678 75.609084444859761 29.277666658736717 0 -1.4697728835004109 4.1394863581405401 -15.938310344219872 0
		 -699.14461121557235 794.1366598181304 -1136.7344129333424 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge8";
	rename -uid "2162B2B0-C043-1C52-553D-95BFC78277C2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 38.950013115879798 0 0 0 0 582.10507434258523 0 0 0 0 38.950013115879798 0
		 -576.13795186641414 617.81054753897888 -1164.7083536555776 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge9";
	rename -uid "696455D4-454E-08CC-53DE-6093DC410DC9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 38.950013115879798 0 0 0 0 360.50459967344131 0 0 0 0 38.950013115879798 0
		 143.08813056295202 397.71270014944184 -819.17452014812625 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge10";
	rename -uid "DF4E54FE-FA44-1110-C9E0-FCA2B1FEF23C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 12.866890517338296 -14.465426192835837 -3.8471955270527736 0
		 90.948213807448056 92.988917487211594 -45.462626379439605 0 7.369199477417717 1.7060128868634792 18.231586849224296 0
		 237.0389104069161 378.81930510903157 -868.0801349434297 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge11";
	rename -uid "0C7192E4-724F-36AD-C50C-6889C16693C5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 14.08735476403279 0 0 0 0 43.57663180810659 0 0 0 0 14.08735476403279 0
		 259.52720946576716 84.620857663352552 802.90151280049099 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge12";
	rename -uid "21E83767-D446-25E1-3751-DD821D7DA076";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 402.61454521928425 0 0 0 0 315.94569750215248 0 0 0 0 261.82520538848411 0
		 -343.23346510613493 313.83258768608647 687.94934527642283 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge13";
	rename -uid "0B753D94-1B4B-FE80-8739-129F9B703A0E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 402.61454521928425 0 0 0 0 315.94569750215248 0 0 0 0 261.82520538848411 0
		 -331.48016920769749 313.83257972863925 -140.3364122765347 1;
	setAttr ".a" 75;
createNode polySeparate -n "polySeparate3";
	rename -uid "D5023E2F-DD44-0A51-F23B-FFBF068377BB";
	setAttr ".ic" 3;
	setAttr -s 3 ".out";
createNode polySoftEdge -n "polySoftEdge14";
	rename -uid "9D53DE9C-E64B-7ADB-D649-B2BBF51EBB7A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.63001666562719316 0 -0.5156314045052568 -75.782271999692853 -192.84535874213907 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge15";
	rename -uid "03772A7B-524B-5629-436A-E69736165C84";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.63001666562719316 0 -0.5156314045052568 -75.782271999692853 -192.84535874213907 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge16";
	rename -uid "1D656F06-6449-BB88-E865-DEBDD0EC3E2B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 0.63001666562719316 0 -0.5156314045052568 -75.782271999692853 -192.84535874213907 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge17";
	rename -uid "BF17ED55-C242-4FA1-F16B-5BA827F1154B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 0.8362440554683811 0 -0.54835743789411218 0 0 1 0 0
		 0.54835743789411218 0 0.8362440554683811 0 390.42090147361449 -17.490011846455502 461.41315419986444 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge18";
	rename -uid "C4031A7B-4D48-6C9B-60BC-7486B818C86D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 2.2204460492503131e-16 0 -1 0 1 2.2204460492503131e-16 2.2204460492503131e-16 0
		 2.2204460492503131e-16 -1 0 0 526.0160669323293 1178.2447621241799 1257.9294336206749 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge19";
	rename -uid "3F7DA373-E649-1CEC-0CDD-DE9519D79ECD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 29.047237315396952 0 0 0 0 219.68520080842268 0 0 0 0 29.047237315396952 0
		 639.26987224280515 221.53875143166931 -335.09513982983287 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge20";
	rename -uid "DB9C3665-9047-E00A-618D-34A2397F1B0E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 29.047237315396952 0 0 0 0 219.68520080842268 0 0 0 0 29.047237315396952 0
		 635.87922914051921 232.37354291627059 -674.5772594785974 1;
	setAttr ".a" 75;
createNode polySoftEdge -n "polySoftEdge21";
	rename -uid "5F874537-2F4D-92F1-E8CD-7BBA4AC11300";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 29.047237315396952 0 0 0 0 219.68520080842268 0 0 0 0 29.047237315396952 0
		 969.91192731359354 225.53354182797827 -680.88002877499014 1;
	setAttr ".a" 75;
createNode aiStandardSurface -n "verde2";
	rename -uid "B9750F8F-8540-8668-BADB-479A232830B0";
	setAttr ".base_color" -type "float3" 0 0.37799999 0 ;
createNode shadingEngine -n "aiStandardSurface1SG";
	rename -uid "88F4583D-AC49-36F2-1DDA-77AACCB8C6F8";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "933AD207-7043-231F-4BE2-A5ADF6C3A35F";
createNode aiStandardSurface -n "verde1";
	rename -uid "B2BEAADA-A348-1E89-EA0C-B99948ABA471";
	setAttr ".base_color" -type "float3" 0.0070000002 0.3581 0.0070000002 ;
createNode shadingEngine -n "aiStandardSurface2SG";
	rename -uid "AA8851FC-F341-93A8-81BE-D897AD959932";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "83A4FA5E-3D48-AF10-EA73-049430E11B8C";
createNode aiStandardSurface -n "marron";
	rename -uid "11538EB2-8F45-35A0-B955-B6B31381B4A7";
	setAttr ".base_color" -type "float3" 0.124 0.066067271 0.016988004 ;
createNode shadingEngine -n "aiStandardSurface3SG";
	rename -uid "2306FA81-D846-96D4-F1DE-8D82E17F00B8";
	setAttr ".ihi" 0;
	setAttr -s 6 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "3CE2620F-0148-90A0-A974-7DA59485A3FC";
createNode aiStandardHair -n "aiStandardHair1";
	rename -uid "3399B5C0-7542-3D51-E0A6-918E9FC9E326";
createNode shadingEngine -n "aiStandardHair1SG";
	rename -uid "0A3F2A2F-754A-4802-56D4-81B1C301A9BD";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
	rename -uid "AB84B206-4E4E-C302-FEFC-F89A4D85C9C1";
createNode aiStandardSurface -n "aiStandardSurface4";
	rename -uid "756C3819-3948-F080-F34B-AEB6B5A18C18";
	setAttr ".base_color" -type "float3" 0.27700001 0.17692837 0.092241004 ;
createNode shadingEngine -n "aiStandardSurface4SG";
	rename -uid "0A06D897-1C4E-ECF5-A80B-82A1E71109B2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo5";
	rename -uid "65167214-E545-1B9F-525A-BABA8EF52575";
createNode aiStandardSurface -n "marron_claro";
	rename -uid "FA3A9A0B-CE48-A1AC-A250-D78BE945E03E";
	setAttr ".base_color" -type "float3" 0.27700001 0.17692837 0.092241004 ;
createNode shadingEngine -n "aiStandardSurface5SG";
	rename -uid "DA4D4B72-CF47-2277-6086-949D134E4856";
	setAttr ".ihi" 0;
	setAttr -s 14 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 7 ".gn";
createNode materialInfo -n "materialInfo6";
	rename -uid "8167FB6A-4646-926A-BAA9-51B6664F422C";
createNode aiStandardSurface -n "verde_claro";
	rename -uid "06339AF2-A342-0CEB-62D6-DBB69E1C25A8";
	setAttr ".base_color" -type "float3" 0 1 0 ;
createNode shadingEngine -n "aiStandardSurface6SG";
	rename -uid "CCD4F4B7-5A4E-9B72-FD39-FD91342AE40D";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo7";
	rename -uid "3BBF0BD4-3F4F-D290-1303-D1937AADB3D3";
createNode polyUnite -n "polyUnite8";
	rename -uid "D4F6916E-EC49-07D6-20DB-C381BBFEF710";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode polyUnite -n "polyUnite9";
	rename -uid "B73BE2C1-FF49-1F14-A051-78BE181B7E9B";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode aiStandardSurface -n "marronClaro";
	rename -uid "9E7719E9-9143-95BB-A8E8-91894B6C802F";
	setAttr ".base_color" -type "float3" 0.5783 0.4465 0.22669999 ;
createNode shadingEngine -n "aiStandardSurface7SG";
	rename -uid "BEC5C0B0-FA4A-6F54-F87E-459BB46F125E";
	setAttr ".ihi" 0;
	setAttr -s 32 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 18 ".gn";
createNode materialInfo -n "materialInfo8";
	rename -uid "9A316835-8047-0DB1-6476-F3BE111894EF";
createNode aiStandardSurface -n "MarronMuyClaro";
	rename -uid "C9DED39B-E640-9E8C-92D0-24A955EFD63C";
	setAttr ".base_color" -type "float3" 1 0.9070372 0.75199997 ;
createNode shadingEngine -n "aiStandardSurface8SG";
	rename -uid "ACD0E6A1-8740-A609-D4D4-778035355E12";
	setAttr ".ihi" 0;
	setAttr -s 10 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo9";
	rename -uid "91559BE3-9242-0196-D27A-7197FF07D978";
createNode aiStandardSurface -n "Rojo";
	rename -uid "7AF889E8-ED40-BF93-546A-229A19594136";
	setAttr ".base_color" -type "float3" 0.5043 0.069200002 0.069200002 ;
createNode shadingEngine -n "aiStandardSurface9SG";
	rename -uid "B808CB87-2344-AD14-B6BA-BE819144CC5C";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
createNode materialInfo -n "materialInfo10";
	rename -uid "5845D34F-A64B-E362-5527-58877EED997D";
createNode groupId -n "groupId47";
	rename -uid "AC3545F0-424A-9D85-5CC7-408CBF398AC7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "AA8E8ECE-5A43-20D0-E5D5-A4A0ED06072F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".irc" -type "componentList" 2 "f[0:2]" "f[4:8]";
createNode groupId -n "groupId48";
	rename -uid "BE70F373-9546-2001-267F-0ABA48F15138";
	setAttr ".ihi" 0;
createNode groupId -n "groupId49";
	rename -uid "666DA35B-6C4F-F089-9359-62B55F4AB54B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts19";
	rename -uid "74D4ABC4-714C-EBB2-85DE-E1B96FDF3F79";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[1]" "f[7]";
createNode aiStandardSurface -n "azulClaro";
	rename -uid "E332B20F-9344-D30A-FB59-178AD4F78EB4";
	setAttr ".base_color" -type "float3" 0.55326307 0.55326307 0.82700002 ;
	setAttr ".metalness" 0.61594200134277344;
createNode shadingEngine -n "aiStandardSurface10SG";
	rename -uid "36B4A447-5343-8852-905B-F5A7A6693587";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo11";
	rename -uid "0FF87D26-CB48-EED7-DDF2-2280EB4C3146";
createNode groupId -n "groupId51";
	rename -uid "AB2EECEB-4B4F-3A71-1478-19BE3872B8DA";
	setAttr ".ihi" 0;
createNode aiShadowMatte -n "aiShadowMatte1";
	rename -uid "7D653E9A-7D4B-E30A-054B-95A4E646E048";
createNode shadingEngine -n "aiShadowMatte1SG";
	rename -uid "7C39F56F-B348-860A-B2F6-58A3B6433D85";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo12";
	rename -uid "AB9AE88F-AD4C-8479-34FC-7DAD44E0162A";
createNode groupParts -n "groupParts22";
	rename -uid "EEF67BAA-1F46-49EF-1196-9BB176A77049";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "f[0]" "f[2]" "f[4:6]" "f[8]";
createNode groupId -n "groupId53";
	rename -uid "447928A9-9E4F-61A8-3982-9C8359F7ED2F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "458BE420-C348-2871-1B0C-07B3BABE04B4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[1]" "f[9]";
createNode groupId -n "groupId52";
	rename -uid "A6DB0B6B-EC4F-26B0-7DAC-81A9B44E14BE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts20";
	rename -uid "4AB1E48D-2A48-82FC-D911-39B058C6B5CE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".irc" -type "componentList" 3 "f[0:2]" "f[4:6]" "f[8:9]";
createNode groupId -n "groupId50";
	rename -uid "02465EC7-F245-DDE1-40DF-91B98C2141C0";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "aiStandardSurface11";
	rename -uid "C9A6316E-0A4B-9FEC-371C-2E94A2A3FC3C";
createNode shadingEngine -n "aiStandardSurface11SG";
	rename -uid "C1386150-C443-20BC-B775-D59B5E7290B0";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo13";
	rename -uid "80187DB5-C64F-F29D-4B8F-A8A07C017786";
createNode groupId -n "groupId2";
	rename -uid "C376E31D-E748-DB7F-A0F2-BC8766D7A2CB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId1";
	rename -uid "7D2E2348-9D45-8CD3-F22D-589E29C0B667";
	setAttr ".ihi" 0;
createNode groupId -n "groupId41";
	rename -uid "212D4536-4D4B-9AD5-68E1-3E8F50873530";
	setAttr ".ihi" 0;
createNode groupId -n "groupId40";
	rename -uid "973FE533-4C47-BEB3-C6B7-2CB69818EFF0";
	setAttr ".ihi" 0;
createNode polySeparate -n "polySeparate2";
	rename -uid "A3B6F797-914A-E67D-62D7-989DD684D7C7";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupParts -n "groupParts15";
	rename -uid "A2B16DD7-7D4F-C3F6-7A59-0F84D471D15A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:29]";
createNode groupId -n "groupId44";
	rename -uid "4A8D2A70-934D-9C85-A810-E29631400B43";
	setAttr ".ihi" 0;
createNode polyUnite -n "polyUnite7";
	rename -uid "A368C963-3542-BCF9-33C7-FE93CDA2E61E";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId43";
	rename -uid "84C400FA-9945-A41B-CDD8-1CBF9AB2BA08";
	setAttr ".ihi" 0;
createNode groupId -n "groupId42";
	rename -uid "E804B94F-3244-10ED-DFDD-789A757C6ECD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "92234949-AA40-05B3-7225-8AB324B0E7CA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	rename -uid "2324C292-224F-1295-21C0-04B5021FAAA1";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "amarillo";
	rename -uid "D7D4269B-8E41-B63D-8AD7-4C98A7D00897";
	setAttr ".base_color" -type "float3" 1 1 0 ;
createNode shadingEngine -n "aiStandardSurface12SG";
	rename -uid "F259AB4F-0A43-A7B9-A036-6BB12A4C95EC";
	setAttr ".ihi" 0;
	setAttr -s 7 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 7 ".gn";
createNode materialInfo -n "materialInfo14";
	rename -uid "68156581-6548-6CA2-8FA1-34869F5EA010";
createNode groupId -n "groupId54";
	rename -uid "EA6E8B7E-C64B-3497-FA3E-038C24CC37E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId55";
	rename -uid "45ACEEC7-584B-82FE-CB0D-DEB336E17207";
	setAttr ".ihi" 0;
createNode groupId -n "groupId56";
	rename -uid "6F81C5CC-EB44-73D9-0A9A-E5ACDCBA6517";
	setAttr ".ihi" 0;
createNode groupId -n "groupId57";
	rename -uid "3EC11368-F04B-2E21-F585-689300340FC6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId58";
	rename -uid "3DA1E253-4742-A8FF-8750-6AA12760F560";
	setAttr ".ihi" 0;
createNode groupId -n "groupId59";
	rename -uid "290C762A-DC4B-1E5A-E0C1-FAB5B0F9AC1E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId60";
	rename -uid "35BAB460-BC4F-60A6-5C73-EFB16FCA2ACA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId61";
	rename -uid "A5DF3078-614D-5D63-D546-A098773D0891";
	setAttr ".ihi" 0;
createNode groupId -n "groupId62";
	rename -uid "57E0B783-784E-7BD3-53C5-8C95A042A059";
	setAttr ".ihi" 0;
createNode groupId -n "groupId63";
	rename -uid "9548DEA8-F54E-95D4-70EB-148C96F98788";
	setAttr ".ihi" 0;
createNode groupId -n "groupId64";
	rename -uid "1F1A3399-5B4C-5DCB-BE46-C4ADB9399D75";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "B8551FAD-C048-A833-F56A-A784277C319D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".irc" -type "componentList" 1 "f[4]";
createNode groupId -n "groupId65";
	rename -uid "7D84C69C-774F-4A23-210C-44B2CD1B821D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId66";
	rename -uid "6AF437B1-2E45-EC61-9AFE-46B00AE8F91D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts24";
	rename -uid "3EA22053-4940-6C22-C276-8EB37BF814C7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[4]";
createNode groupId -n "groupId67";
	rename -uid "70012188-D144-E81A-76DF-1D85DD2639A4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "7E80F6ED-CC4A-CFE2-DB2F-E394721650A5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[0:3]" "f[5:7]" "f[9:21]";
	setAttr ".irc" -type "componentList" 2 "f[4]" "f[8]";
createNode groupId -n "groupId68";
	rename -uid "11EA6DC5-5345-7115-FD57-C0894A6E3A87";
	setAttr ".ihi" 0;
createNode groupId -n "groupId69";
	rename -uid "892F7E33-ED4B-A330-FA84-42BB87AC46C3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts26";
	rename -uid "DA82E8CA-4345-46CC-36E2-30ACF52F352D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[4]" "f[8]";
createNode groupId -n "groupId70";
	rename -uid "40D28AFD-BE4D-173D-19FF-CF82DB051F8E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId71";
	rename -uid "EF4AD8CD-A543-0F0E-4BA4-6EB5BA6F5D35";
	setAttr ".ihi" 0;
createNode groupId -n "groupId72";
	rename -uid "D84818F1-BE4B-C016-CDEC-18A27465D475";
	setAttr ".ihi" 0;
createNode polyChipOff -n "polyChipOff1";
	rename -uid "922D0A94-C44A-C3DF-3736-858A8697E06B";
	setAttr ".ics" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".ix" -type "matrix" -9.137419981674889e-15 0 41.151281224598755 0 0 115.98442169280271 0 0
		 -140.78885205848553 0 -3.1261405033175101e-14 0 -335.62290481607363 531.8481392229312 648.18874605415624 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -335.62289 531.84814 648.18872 ;
	setAttr ".rs" 369607181;
createNode polySeparate -n "polySeparate4";
	rename -uid "277051CB-F045-FA59-3A7D-CCA28377CA60";
	setAttr ".ic" 2;
	setAttr -s 2 ".out";
createNode groupId -n "groupId73";
	rename -uid "36CCD9F8-2543-22B9-9ED0-859DFE7F2963";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts27";
	rename -uid "080E5D44-6640-03C9-0EB9-3DB647918209";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:23]";
createNode groupId -n "groupId74";
	rename -uid "869338CC-CC49-5C7C-0DDB-19A56B96EB42";
	setAttr ".ihi" 0;
createNode groupId -n "groupId75";
	rename -uid "6B1B0D24-FE44-6C91-F679-208EF2E2393E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts28";
	rename -uid "87818152-5642-045B-EE12-649F0A9A9606";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 20 "f[0]" "f[1]" "f[2]" "f[3]" "f[5]" "f[6]" "f[7]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]";
	setAttr ".irc" -type "componentList" 2 "f[4]" "f[8]";
createNode groupId -n "groupId76";
	rename -uid "661ED374-954F-FCAF-05E1-22A05E6B00A4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts29";
	rename -uid "CF3F58CC-314A-9AB2-4B89-0DA3557055DF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[0]" "f[1]";
createNode groupId -n "groupId77";
	rename -uid "0A5543D1-2E49-9994-F12D-E4BFE7281A6B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts30";
	rename -uid "E1ED1A7D-F64E-4F2D-1E88-EF8E7B2A0562";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[4]" "f[8]";
createNode groupId -n "groupId79";
	rename -uid "E3A1240D-4F4B-E6B4-E723-6494EE99ADBC";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "negro";
	rename -uid "DC36C68F-ED48-A59D-8FCE-84A0DB186876";
	setAttr ".base_color" -type "float3" 0 0 0 ;
createNode shadingEngine -n "aiStandardSurface13SG";
	rename -uid "336D6CDE-CC4B-ED42-9B3C-C4A7DE9A6A72";
	setAttr ".ihi" 0;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 4 ".gn";
createNode materialInfo -n "materialInfo15";
	rename -uid "DCE7783D-DF4A-A9E1-3B12-37BFDD967E3D";
createNode groupId -n "groupId81";
	rename -uid "551F8F9F-9A48-7033-02A8-E991476429BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId82";
	rename -uid "4975095B-2D4A-E82B-3DAE-0AB5711CF596";
	setAttr ".ihi" 0;
createNode groupId -n "groupId83";
	rename -uid "51176213-264B-6D9A-2DB4-AAB87F6EF59F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId84";
	rename -uid "AC07C83E-8946-8F70-2B05-70AB15D3CE33";
	setAttr ".ihi" 0;
createNode groupId -n "groupId85";
	rename -uid "62189BC1-3F45-B7DA-8BEB-1AB9BC070BBC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId86";
	rename -uid "2E768B2D-A441-8419-ED79-FA9EEAAB52F9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId80";
	rename -uid "92FA7050-984C-8AF4-CCB1-BA821CE4C0B5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId78";
	rename -uid "2FA1050D-4849-CF30-6ED9-44A41E685F68";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "amarilloClaro";
	rename -uid "F2A78E88-3C40-CA39-23E2-368EEF1A530D";
	setAttr ".base_color" -type "float3" 1 1 0.759 ;
createNode shadingEngine -n "aiStandardSurface14SG";
	rename -uid "88D9B080-DE4E-E6F7-6CF0-86A3EF87B6E6";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo16";
	rename -uid "578F8EB7-DD43-8846-848B-0287E408A42A";
createNode groupId -n "groupId87";
	rename -uid "ADFCE847-5F47-53E0-E60A-74AD427B0688";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts31";
	rename -uid "D8EEE268-8142-3CF4-FDDC-0EB9DB6B56DD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[1]" "f[3]" "f[6:9]";
	setAttr ".irc" -type "componentList" 3 "f[0]" "f[2]" "f[4:5]";
createNode groupId -n "groupId88";
	rename -uid "92A3AA89-EE4E-2008-E061-C9B77F6FEA5C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId89";
	rename -uid "768A437A-5442-AFCD-30F8-1A8E3C9A331E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts32";
	rename -uid "06FEA910-E147-F5CE-50BA-26B5B54BCC4E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[0]" "f[2]" "f[4:5]";
createNode aiStandardSurface -n "morado";
	rename -uid "82AB7FE5-D24D-B4CC-F4FA-20AF7B8A4F3C";
	setAttr ".base_color" -type "float3" 0.34627929 0.227835 0.41499999 ;
createNode shadingEngine -n "aiStandardSurface15SG";
	rename -uid "65DE91B7-B64A-94BF-33F4-6A9694168524";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo17";
	rename -uid "827FDDD4-5442-D1DD-09EF-94B908DF1336";
createNode groupId -n "groupId90";
	rename -uid "55DD991F-C74D-56CD-6C86-35B267784D41";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts33";
	rename -uid "037534D8-F746-DEAC-4A7B-B0B2304CB71A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[6]";
createNode aiStandardSurface -n "aiStandardSurface16";
	rename -uid "08F3F557-C845-C159-F598-3C8DC8114D34";
createNode shadingEngine -n "aiStandardSurface16SG";
	rename -uid "A164D8CA-F141-FC09-4E15-EFBBB3A699FA";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo18";
	rename -uid "BB46A6D8-7A4C-285F-9E69-5AB9F3CD0328";
createNode groupId -n "groupId91";
	rename -uid "574E2E3A-6F45-251B-88C4-0384C40E864E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts34";
	rename -uid "C7552711-2549-C010-7DC9-BC8730157BB5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[5]";
createNode aiStandardSurface -n "aiStandardSurface17";
	rename -uid "2DBB01D8-C24A-1265-CAD4-11A92182DE4B";
createNode shadingEngine -n "aiStandardSurface17SG";
	rename -uid "886145D7-A645-9DF9-0701-1EBBE1ED6B29";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo19";
	rename -uid "27929A74-B744-A21C-5DB3-C98248C28D64";
createNode aiStandardSurface -n "moradoClaro";
	rename -uid "5F56EC96-BE4B-9FAC-FCD5-94B3B49F2EC3";
	setAttr ".base_color" -type "float3" 1 0.759 0.98400003 ;
createNode shadingEngine -n "aiStandardSurface18SG";
	rename -uid "E1C007B6-2545-D335-9794-64B9EC7B9AEF";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 3 ".gn";
createNode materialInfo -n "materialInfo20";
	rename -uid "F88FB19F-B94A-6185-EA34-C7969D0ED611";
createNode groupId -n "groupId92";
	rename -uid "CF24087A-DF4F-E9FB-3D9F-B9AA25D297B7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts35";
	rename -uid "A3C396A8-2A48-D2E8-CCB0-8C805C873159";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "f[0]" "f[2]" "f[4]" "f[8]";
createNode aiStandardSurface -n "gris";
	rename -uid "FFB9C8CC-BD4B-ADBA-DE60-0DAF5D2708B0";
	setAttr ".base_color" -type "float3" 0.34099999 0.34099999 0.34099999 ;
createNode shadingEngine -n "aiStandardSurface19SG";
	rename -uid "795C1B73-7745-D89A-70D5-61BE52B45567";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo21";
	rename -uid "3E00B22B-2641-2171-A337-D6B0391FD0E0";
createNode polyAutoProj -n "polyAutoProj1";
	rename -uid "C733B182-924F-905D-5139-C98031728148";
	setAttr ".cch" yes;
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:5]";
	setAttr ".ix" -type "matrix" 3045.2611723116893 0 0 0 0 1901.8091942419278 0 0 0 0 3045.2611723116893 0
		 336.27952910362183 0 -206.61132432596347 1;
	setAttr ".s" -type "double3" 3045.2611723116893 3045.2611723116893 3045.2611723116893 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
createNode polyTweak -n "polyTweak51";
	rename -uid "7D905FE0-C94D-716A-FFC6-CAAA53B2666B";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258
		 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0
		 0 0.47111258 0;
createNode polyTweakUV -n "polyTweakUV41";
	rename -uid "528E625D-F04F-E77E-E678-7F88A5599FC4";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[12]" -type "float2" 0.1 0 ;
	setAttr ".uvtk[13]" -type "float2" 0.1 0 ;
	setAttr ".uvtk[14]" -type "float2" 0.1 0 ;
	setAttr ".uvtk[15]" -type "float2" 0.1 0 ;
createNode aiStandardSurface -n "aiStandardSurface20";
	rename -uid "4B6DE1B3-6B4A-E964-472A-27923204A27A";
createNode shadingEngine -n "aiStandardSurface20SG";
	rename -uid "5359C940-6F47-F439-29A7-179A7FFCC121";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo22";
	rename -uid "3088A09F-CB4A-973C-F77F-97B53678D4ED";
createNode file -n "file1";
	rename -uid "16BB3333-BF49-CE3C-39F7-CDB8CD535C2D";
	setAttr ".ftn" -type "string" "/Users/raquel/Desktop/suelo.psd";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "96C61487-6B44-452C-E638-7782240DBB9F";
createNode aiStandardSurface -n "aiStandardSurface21";
	rename -uid "208702D7-164A-7081-CD5A-79952C8367FD";
createNode shadingEngine -n "aiStandardSurface21SG";
	rename -uid "37D47E51-F748-E8AA-858A-648BB2F597A5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo23";
	rename -uid "C29873F5-6C46-97B7-8E75-8586E289CB23";
select -ne :time1;
	setAttr ".o" 90;
	setAttr ".unw" 90;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 25 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 28 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr -s 47 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 46 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupParts35.og" "House1Shape.i";
connectAttr "groupId47.id" "House1Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "House1Shape.iog.og[0].gco";
connectAttr "groupId49.id" "House1Shape.iog.og[1].gid";
connectAttr "aiStandardSurface9SG.mwc" "House1Shape.iog.og[1].gco";
connectAttr "groupId90.id" "House1Shape.iog.og[2].gid";
connectAttr "aiStandardSurface18SG.mwc" "House1Shape.iog.og[2].gco";
connectAttr "groupId91.id" "House1Shape.iog.og[3].gid";
connectAttr "aiStandardSurface18SG.mwc" "House1Shape.iog.og[3].gco";
connectAttr "groupId92.id" "House1Shape.iog.og[4].gid";
connectAttr "aiStandardSurface18SG.mwc" "House1Shape.iog.og[4].gco";
connectAttr "groupId48.id" "House1Shape.ciog.cog[0].cgid";
connectAttr "groupParts22.og" "HouseRoofShape.i";
connectAttr "groupId50.id" "HouseRoofShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "HouseRoofShape.iog.og[0].gco";
connectAttr "groupId52.id" "HouseRoofShape.iog.og[1].gid";
connectAttr "aiStandardSurface9SG.mwc" "HouseRoofShape.iog.og[1].gco";
connectAttr "groupId53.id" "HouseRoofShape.iog.og[2].gid";
connectAttr "aiStandardSurface15SG.mwc" "HouseRoofShape.iog.og[2].gco";
connectAttr "groupId51.id" "HouseRoofShape.ciog.cog[0].cgid";
connectAttr "polyTweakUV41.out" "FloorShape.i";
connectAttr "polyTweakUV41.uvtk[0]" "FloorShape.uvst[0].uvtw";
connectAttr "groupId22.id" "Wood1Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Wood1Shape.iog.og[0].gco";
connectAttr "groupId23.id" "Wood1Shape.ciog.cog[0].cgid";
connectAttr "groupId24.id" "Wood2Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Wood2Shape.iog.og[0].gco";
connectAttr "groupId25.id" "Wood2Shape.ciog.cog[0].cgid";
connectAttr "groupId27.id" "Wood3Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Wood3Shape.iog.og[0].gco";
connectAttr "groupId28.id" "Wood3Shape.ciog.cog[0].cgid";
connectAttr "groupId29.id" "Wood4Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Wood4Shape.iog.og[0].gco";
connectAttr "groupId30.id" "Wood4Shape.ciog.cog[0].cgid";
connectAttr "groupParts8.og" "TrunkShape.i";
connectAttr "groupId32.id" "TrunkShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "TrunkShape.iog.og[0].gco";
connectAttr "groupId33.id" "TrunkShape.ciog.cog[0].cgid";
connectAttr "polyDelEdge27.out" "FloorShape2.i";
connectAttr "groupParts26.og" "WindowShape1.i";
connectAttr "groupId67.id" "WindowShape1.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape1.iog.og[0].gco";
connectAttr "groupId69.id" "WindowShape1.iog.og[1].gid";
connectAttr "aiStandardSurface5SG.mwc" "WindowShape1.iog.og[1].gco";
connectAttr "groupId68.id" "WindowShape1.ciog.cog[0].cgid";
connectAttr "groupId60.id" "WindowShape3.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape3.iog.og[0].gco";
connectAttr "groupId62.id" "WindowShape3.iog.og[1].gid";
connectAttr "aiStandardSurface5SG.mwc" "WindowShape3.iog.og[1].gco";
connectAttr "groupId63.id" "WindowShape3.iog.og[2].gid";
connectAttr "aiStandardSurface5SG.mwc" "WindowShape3.iog.og[2].gco";
connectAttr "groupId61.id" "WindowShape3.ciog.cog[0].cgid";
connectAttr "groupParts30.og" "polySurfaceShape36.i";
connectAttr "groupId75.id" "polySurfaceShape36.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "polySurfaceShape36.iog.og[0].gco";
connectAttr "groupId77.id" "polySurfaceShape36.iog.og[2].gid";
connectAttr "aiStandardSurface12SG.mwc" "polySurfaceShape36.iog.og[2].gco";
connectAttr "groupParts29.og" "polySurfaceShape37.i";
connectAttr "groupId76.id" "polySurfaceShape37.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "polySurfaceShape37.iog.og[0].gco";
connectAttr "groupId73.id" "WindowShape4.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape4.iog.og[0].gco";
connectAttr "groupParts27.og" "WindowShape4.i";
connectAttr "groupId74.id" "WindowShape4.ciog.cog[0].cgid";
connectAttr "groupParts24.og" "WindowShape2.i";
connectAttr "groupId64.id" "WindowShape2.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape2.iog.og[0].gco";
connectAttr "groupId66.id" "WindowShape2.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape2.iog.og[1].gco";
connectAttr "groupId65.id" "WindowShape2.ciog.cog[0].cgid";
connectAttr "groupId84.id" "DoorShape.iog.og[0].gid";
connectAttr "aiStandardSurface5SG.mwc" "DoorShape.iog.og[0].gco";
connectAttr "groupId86.id" "DoorShape.iog.og[1].gid";
connectAttr "aiStandardSurface13SG.mwc" "DoorShape.iog.og[1].gco";
connectAttr "groupId85.id" "DoorShape.ciog.cog[0].cgid";
connectAttr "polySoftEdge12.out" "Roof1Shape.i";
connectAttr "groupId81.id" "Door1Shape.iog.og[0].gid";
connectAttr "aiStandardSurface5SG.mwc" "Door1Shape.iog.og[0].gco";
connectAttr "groupId83.id" "Door1Shape.iog.og[1].gid";
connectAttr "aiStandardSurface13SG.mwc" "Door1Shape.iog.og[1].gco";
connectAttr "groupId82.id" "Door1Shape.ciog.cog[0].cgid";
connectAttr "groupId70.id" "WindowShape5.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape5.iog.og[0].gco";
connectAttr "groupId72.id" "WindowShape5.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape5.iog.og[1].gco";
connectAttr "groupId71.id" "WindowShape5.ciog.cog[0].cgid";
connectAttr "groupId78.id" "WindowShape6.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape6.iog.og[0].gco";
connectAttr "groupId80.id" "WindowShape6.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape6.iog.og[1].gco";
connectAttr "groupId79.id" "WindowShape6.ciog.cog[0].cgid";
connectAttr "groupId54.id" "WindowShape8.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape8.iog.og[0].gco";
connectAttr "groupId56.id" "WindowShape8.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape8.iog.og[1].gco";
connectAttr "groupId55.id" "WindowShape8.ciog.cog[0].cgid";
connectAttr "groupId57.id" "WindowShape9.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape9.iog.og[0].gco";
connectAttr "groupId59.id" "WindowShape9.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape9.iog.og[1].gco";
connectAttr "groupId58.id" "WindowShape9.ciog.cog[0].cgid";
connectAttr "polySoftEdge20.out" "Column3Shape.i";
connectAttr "polySoftEdge19.out" "Column5Shape.i";
connectAttr "polySoftEdge21.out" "Column6Shape.i";
connectAttr "polySoftEdge13.out" "Roof2Shape.i";
connectAttr "groupId1.id" "Roof6Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "Roof6Shape.iog.og[1].gco";
connectAttr "groupId2.id" "Roof6Shape.ciog.cog[0].cgid";
connectAttr "groupId5.id" "Roof7Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "Roof7Shape.iog.og[1].gco";
connectAttr "groupId6.id" "Roof7Shape.ciog.cog[0].cgid";
connectAttr "groupId3.id" "pPlaneShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pPlaneShape1.iog.og[0].gco";
connectAttr "groupParts1.og" "pPlaneShape1.i";
connectAttr "groupId4.id" "pPlaneShape1.ciog.cog[0].cgid";
connectAttr "polySoftEdge16.out" "polySurfaceShape29.i";
connectAttr "polySoftEdge15.out" "polySurfaceShape30.i";
connectAttr "polySoftEdge14.out" "polySurfaceShape31.i";
connectAttr "groupParts2.og" "Roof8Shape.i";
connectAttr "groupId7.id" "Roof8Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Roof8Shape.iog.og[0].gco";
connectAttr "groupParts32.og" "lanternShape.i";
connectAttr "groupId87.id" "lanternShape.iog.og[0].gid";
connectAttr "aiStandardSurface13SG.mwc" "lanternShape.iog.og[0].gco";
connectAttr "groupId89.id" "lanternShape.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "lanternShape.iog.og[1].gco";
connectAttr "groupId88.id" "lanternShape.ciog.cog[0].cgid";
connectAttr "groupId10.id" "millwheel1Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "millwheel1Shape.iog.og[0].gco";
connectAttr "groupId11.id" "millwheel1Shape.ciog.cog[0].cgid";
connectAttr "groupId12.id" "pCylinderShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupParts3.og" "pCylinderShape1.i";
connectAttr "groupId13.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "groupId8.id" "pCylinderShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape2.iog.og[0].gco";
connectAttr "groupId9.id" "pCylinderShape2.ciog.cog[0].cgid";
connectAttr "groupId19.id" "pCylinderShape3.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape3.iog.og[0].gco";
connectAttr "groupId20.id" "pCylinderShape3.ciog.cog[0].cgid";
connectAttr "groupId16.id" "millwheel3Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "millwheel3Shape.iog.og[0].gco";
connectAttr "groupId17.id" "millwheel3Shape.ciog.cog[0].cgid";
connectAttr "groupId14.id" "pCylinderShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape5.iog.og[0].gco";
connectAttr "groupId15.id" "pCylinderShape5.ciog.cog[0].cgid";
connectAttr "polyMergeVert30.out" "pCylinder6Shape.i";
connectAttr "groupId18.id" "pCylinder6Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder6Shape.iog.og[0].gco";
connectAttr "polyTweakUV30.uvtk[0]" "pCylinder6Shape.uvst[0].uvtw";
connectAttr "polySoftEdge18.out" "millWheelShape.i";
connectAttr "polyTweakUV40.uvtk[0]" "millWheelShape.uvst[0].uvtw";
connectAttr "groupParts6.og" "Wood7Shape.i";
connectAttr "groupId26.id" "Wood7Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Wood7Shape.iog.og[0].gco";
connectAttr "groupParts7.og" "Wood8Shape.i";
connectAttr "groupId31.id" "Wood8Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Wood8Shape.iog.og[0].gco";
connectAttr "polySoftEdge17.out" "polySurfaceShape10.i";
connectAttr "groupParts11.og" "polySurfaceShape11.i";
connectAttr "groupId36.id" "polySurfaceShape11.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape11.iog.og[0].gco";
connectAttr "groupParts12.og" "polySurfaceShape12.i";
connectAttr "groupId37.id" "polySurfaceShape12.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape12.iog.og[0].gco";
connectAttr "groupParts13.og" "polySurfaceShape13.i";
connectAttr "groupId38.id" "polySurfaceShape13.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape13.iog.og[0].gco";
connectAttr "groupParts14.og" "polySurfaceShape14.i";
connectAttr "groupId39.id" "polySurfaceShape14.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape14.iog.og[0].gco";
connectAttr "groupParts9.og" "Trunk1Shape.i";
connectAttr "groupId34.id" "Trunk1Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Trunk1Shape.iog.og[0].gco";
connectAttr "polySoftEdge3.out" "TreetopBig2Shape.i";
connectAttr "polySoftEdge2.out" "TreetopBig1Shape.i";
connectAttr "polySoftEdge10.out" "BranchSmallTreeShape.i";
connectAttr "polySoftEdge7.out" "BranchBigTreeShape.i";
connectAttr "polySoftEdge1.out" "TreetopSmall1Shape.i";
connectAttr "polySoftEdge4.out" "TreetopSmall2Shape.i";
connectAttr "polySoftEdge9.out" "SmallTreeShape.i";
connectAttr "polySoftEdge8.out" "BigTreeShape.i";
connectAttr "polySoftEdge6.out" "BranchSmallTree1Shape.i";
connectAttr "polySoftEdge5.out" "TreetopSmall3Shape.i";
connectAttr "polySoftEdge11.out" "SmallTree1Shape.i";
connectAttr "groupId42.id" "Step3Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Step3Shape.iog.og[0].gco";
connectAttr "groupId43.id" "Step3Shape.ciog.cog[0].cgid";
connectAttr "groupId40.id" "Column7Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Column7Shape.iog.og[0].gco";
connectAttr "groupId41.id" "Column7Shape.ciog.cog[0].cgid";
connectAttr "polySeparate2.out[0]" "polySurfaceShape15.i";
connectAttr "polySeparate2.out[1]" "polySurfaceShape16.i";
connectAttr "groupParts15.og" "Column8Shape.i";
connectAttr "groupId44.id" "Column8Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Column8Shape.iog.og[0].gco";
connectAttr "polyUnite8.out" "pawsShape.i";
connectAttr "polyUnite9.out" "paws2Shape.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardHair1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface6SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface7SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface8SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface9SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface10SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiShadowMatte1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface11SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface12SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface13SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface14SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface15SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface16SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface17SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface18SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface19SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface20SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface21SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardHair1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface7SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface8SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface9SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface10SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiShadowMatte1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface11SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface12SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface13SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface14SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface15SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface16SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface17SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface18SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface19SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface20SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface21SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "polyTweak1.out" "polySplit1.ip";
connectAttr "polyCube2.out" "polyTweak1.ip";
connectAttr "polyCube3.out" "polySplit2.ip";
connectAttr "|Window1|polySurfaceShape6.o" "polyExtrudeFace5.ip";
connectAttr "WindowShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "polyExtrudeFace6.ip";
connectAttr "WindowShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyTweak7.out" "polyExtrudeFace7.ip";
connectAttr "WindowShape2.wm" "polyExtrudeFace7.mp";
connectAttr "polyCube5.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polyExtrudeFace8.ip";
connectAttr "WindowShape2.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak8.ip";
connectAttr "|Floor2|polySurfaceShape7.o" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySplit6.ip";
connectAttr "polyPlane1.out" "polyDelEdge1.ip";
connectAttr "polyDelEdge1.out" "polyDelEdge2.ip";
connectAttr "polyDelEdge2.out" "polyDelEdge3.ip";
connectAttr "polyDelEdge3.out" "polyDelEdge4.ip";
connectAttr "polyDelEdge4.out" "polyDelEdge5.ip";
connectAttr "polyDelEdge5.out" "polyDelEdge6.ip";
connectAttr "polyDelEdge6.out" "polyDelEdge7.ip";
connectAttr "polyDelEdge7.out" "polyDelEdge8.ip";
connectAttr "polyDelEdge8.out" "polyDelEdge9.ip";
connectAttr "polyDelEdge9.out" "polyDelEdge10.ip";
connectAttr "polyDelEdge10.out" "polyDelEdge11.ip";
connectAttr "polyDelEdge11.out" "polyDelEdge12.ip";
connectAttr "polyDelEdge12.out" "polyDelEdge13.ip";
connectAttr "polyDelEdge13.out" "polyDelEdge14.ip";
connectAttr "polyDelEdge14.out" "polyDelEdge15.ip";
connectAttr "polyDelEdge15.out" "polyDelEdge16.ip";
connectAttr "polyDelEdge16.out" "polyDelEdge17.ip";
connectAttr "polyDelEdge17.out" "groupParts1.ig";
connectAttr "groupId3.id" "groupParts1.gi";
connectAttr "polyUnite1.out" "groupParts2.ig";
connectAttr "groupId7.id" "groupParts2.gi";
connectAttr "Roof6Shape.o" "polyUnite1.ip[0]";
connectAttr "pPlaneShape1.o" "polyUnite1.ip[1]";
connectAttr "Roof7Shape.o" "polyUnite1.ip[2]";
connectAttr "Roof6Shape.wm" "polyUnite1.im[0]";
connectAttr "pPlaneShape1.wm" "polyUnite1.im[1]";
connectAttr "Roof7Shape.wm" "polyUnite1.im[2]";
connectAttr "polySplit6.out" "polyDelEdge18.ip";
connectAttr "polyDelEdge18.out" "polyDelEdge19.ip";
connectAttr "polyDelEdge19.out" "polyDelEdge20.ip";
connectAttr "polyDelEdge20.out" "polyDelEdge21.ip";
connectAttr "polyDelEdge21.out" "polyDelEdge22.ip";
connectAttr "polyDelEdge22.out" "polyDelEdge23.ip";
connectAttr "polyDelEdge23.out" "polyDelEdge24.ip";
connectAttr "polyDelEdge24.out" "polyDelEdge25.ip";
connectAttr "polyDelEdge25.out" "polyDelEdge26.ip";
connectAttr "polyDelEdge26.out" "polyDelEdge27.ip";
connectAttr "polySurfaceShape9.o" "polyExtrudeFace10.ip";
connectAttr "lanternShape.wm" "polyExtrudeFace10.mp";
connectAttr "polyCylinder4.out" "groupParts3.ig";
connectAttr "groupId12.id" "groupParts3.gi";
connectAttr "pCylinderShape5.o" "polyUnite2.ip[0]";
connectAttr "millwheel3Shape.o" "polyUnite2.ip[1]";
connectAttr "pCylinderShape5.wm" "polyUnite2.im[0]";
connectAttr "millwheel3Shape.wm" "polyUnite2.im[1]";
connectAttr "polyUnite2.out" "groupParts4.ig";
connectAttr "groupId18.id" "groupParts4.gi";
connectAttr "groupParts4.og" "polyTweakUV21.ip";
connectAttr "polyTweak29.out" "polyMergeVert21.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert21.mp";
connectAttr "polyTweakUV21.out" "polyTweak29.ip";
connectAttr "polyMergeVert21.out" "polyTweakUV22.ip";
connectAttr "polyTweak30.out" "polyMergeVert22.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert22.mp";
connectAttr "polyTweakUV22.out" "polyTweak30.ip";
connectAttr "polyMergeVert22.out" "polyTweakUV23.ip";
connectAttr "polyTweak31.out" "polyMergeVert23.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert23.mp";
connectAttr "polyTweakUV23.out" "polyTweak31.ip";
connectAttr "polyMergeVert23.out" "polyTweakUV24.ip";
connectAttr "polyTweak32.out" "polyMergeVert24.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert24.mp";
connectAttr "polyTweakUV24.out" "polyTweak32.ip";
connectAttr "polyMergeVert24.out" "polyTweakUV25.ip";
connectAttr "polyTweak33.out" "polyMergeVert25.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert25.mp";
connectAttr "polyTweakUV25.out" "polyTweak33.ip";
connectAttr "polyMergeVert25.out" "polyTweakUV26.ip";
connectAttr "polyTweak34.out" "polyMergeVert26.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert26.mp";
connectAttr "polyTweakUV26.out" "polyTweak34.ip";
connectAttr "polyMergeVert26.out" "polyTweakUV27.ip";
connectAttr "polyTweak35.out" "polyMergeVert27.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert27.mp";
connectAttr "polyTweakUV27.out" "polyTweak35.ip";
connectAttr "polyMergeVert27.out" "polyTweakUV28.ip";
connectAttr "polyTweak36.out" "polyMergeVert28.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert28.mp";
connectAttr "polyTweakUV28.out" "polyTweak36.ip";
connectAttr "polyMergeVert28.out" "polyTweakUV29.ip";
connectAttr "polyTweak37.out" "polyMergeVert29.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert29.mp";
connectAttr "polyTweakUV29.out" "polyTweak37.ip";
connectAttr "polyMergeVert29.out" "polyTweakUV30.ip";
connectAttr "polyTweak38.out" "polyMergeVert30.ip";
connectAttr "pCylinder6Shape.wm" "polyMergeVert30.mp";
connectAttr "polyTweakUV30.out" "polyTweak38.ip";
connectAttr "pCylinder6Shape.o" "polyUnite3.ip[0]";
connectAttr "pCylinderShape3.o" "polyUnite3.ip[1]";
connectAttr "pCylinder6Shape.wm" "polyUnite3.im[0]";
connectAttr "pCylinderShape3.wm" "polyUnite3.im[1]";
connectAttr "polyUnite3.out" "polyTweakUV31.ip";
connectAttr "polyTweak39.out" "polyMergeVert31.ip";
connectAttr "millWheelShape.wm" "polyMergeVert31.mp";
connectAttr "polyTweakUV31.out" "polyTweak39.ip";
connectAttr "polyMergeVert31.out" "polyTweakUV32.ip";
connectAttr "polyTweak40.out" "polyMergeVert32.ip";
connectAttr "millWheelShape.wm" "polyMergeVert32.mp";
connectAttr "polyTweakUV32.out" "polyTweak40.ip";
connectAttr "polyMergeVert32.out" "polyTweakUV33.ip";
connectAttr "polyTweak41.out" "polyMergeVert33.ip";
connectAttr "millWheelShape.wm" "polyMergeVert33.mp";
connectAttr "polyTweakUV33.out" "polyTweak41.ip";
connectAttr "polyMergeVert33.out" "polyTweakUV34.ip";
connectAttr "polyTweak42.out" "polyMergeVert34.ip";
connectAttr "millWheelShape.wm" "polyMergeVert34.mp";
connectAttr "polyTweakUV34.out" "polyTweak42.ip";
connectAttr "polyMergeVert34.out" "polyTweakUV35.ip";
connectAttr "polyTweak43.out" "polyMergeVert35.ip";
connectAttr "millWheelShape.wm" "polyMergeVert35.mp";
connectAttr "polyTweakUV35.out" "polyTweak43.ip";
connectAttr "polyMergeVert35.out" "polyTweakUV36.ip";
connectAttr "polyTweak44.out" "polyMergeVert36.ip";
connectAttr "millWheelShape.wm" "polyMergeVert36.mp";
connectAttr "polyTweakUV36.out" "polyTweak44.ip";
connectAttr "polyMergeVert36.out" "polyTweakUV37.ip";
connectAttr "polyTweak45.out" "polyMergeVert37.ip";
connectAttr "millWheelShape.wm" "polyMergeVert37.mp";
connectAttr "polyTweakUV37.out" "polyTweak45.ip";
connectAttr "polyMergeVert37.out" "polyTweakUV38.ip";
connectAttr "polyTweak46.out" "polyMergeVert38.ip";
connectAttr "millWheelShape.wm" "polyMergeVert38.mp";
connectAttr "polyTweakUV38.out" "polyTweak46.ip";
connectAttr "polyMergeVert38.out" "polyTweakUV39.ip";
connectAttr "polyTweak47.out" "polyMergeVert39.ip";
connectAttr "millWheelShape.wm" "polyMergeVert39.mp";
connectAttr "polyTweakUV39.out" "polyTweak47.ip";
connectAttr "polyMergeVert39.out" "polyTweakUV40.ip";
connectAttr "polyTweak48.out" "polyMergeVert40.ip";
connectAttr "millWheelShape.wm" "polyMergeVert40.mp";
connectAttr "polyTweakUV40.out" "polyTweak48.ip";
connectAttr "polyMergeVert40.out" "polyExtrudeFace11.ip";
connectAttr "millWheelShape.wm" "polyExtrudeFace11.mp";
connectAttr "polyTweak49.out" "polyDelEdge28.ip";
connectAttr "polyExtrudeFace11.out" "polyTweak49.ip";
connectAttr "polyDelEdge28.out" "polyExtrudeFace12.ip";
connectAttr "millWheelShape.wm" "polyExtrudeFace12.mp";
connectAttr "polyTweak50.out" "polyDelEdge29.ip";
connectAttr "polyExtrudeFace12.out" "polyTweak50.ip";
connectAttr "Wood1Shape.o" "polyUnite4.ip[0]";
connectAttr "Wood2Shape.o" "polyUnite4.ip[1]";
connectAttr "Wood1Shape.wm" "polyUnite4.im[0]";
connectAttr "Wood2Shape.wm" "polyUnite4.im[1]";
connectAttr "polyUnite4.out" "groupParts6.ig";
connectAttr "groupId26.id" "groupParts6.gi";
connectAttr "Wood3Shape.o" "polyUnite5.ip[0]";
connectAttr "Wood4Shape.o" "polyUnite5.ip[1]";
connectAttr "Wood3Shape.wm" "polyUnite5.im[0]";
connectAttr "Wood4Shape.wm" "polyUnite5.im[1]";
connectAttr "polyUnite5.out" "groupParts7.ig";
connectAttr "groupId31.id" "groupParts7.gi";
connectAttr "TrunkShape.o" "polyUnite6.ip[0]";
connectAttr "Wood7Shape.o" "polyUnite6.ip[1]";
connectAttr "Wood8Shape.o" "polyUnite6.ip[2]";
connectAttr "TrunkShape.wm" "polyUnite6.im[0]";
connectAttr "Wood7Shape.wm" "polyUnite6.im[1]";
connectAttr "Wood8Shape.wm" "polyUnite6.im[2]";
connectAttr "polyCylinder1.out" "groupParts8.ig";
connectAttr "groupId32.id" "groupParts8.gi";
connectAttr "polyUnite6.out" "groupParts9.ig";
connectAttr "groupId34.id" "groupParts9.gi";
connectAttr "polySphere1.out" "polyTriangulate1.ip";
connectAttr "polyTriangulate1.out" "polyQuad1.ip";
connectAttr "TreetopBig2Shape.wm" "polyQuad1.mp";
connectAttr "Trunk1Shape.o" "polySeparate1.ip";
connectAttr "polySeparate1.out[1]" "groupParts11.ig";
connectAttr "groupId36.id" "groupParts11.gi";
connectAttr "polySeparate1.out[2]" "groupParts12.ig";
connectAttr "groupId37.id" "groupParts12.gi";
connectAttr "polySeparate1.out[3]" "groupParts13.ig";
connectAttr "groupId38.id" "groupParts13.gi";
connectAttr "polySeparate1.out[4]" "groupParts14.ig";
connectAttr "groupId39.id" "groupParts14.gi";
connectAttr "polySurfaceShape17.o" "polySoftEdge1.ip";
connectAttr "TreetopSmall1Shape.wm" "polySoftEdge1.mp";
connectAttr "polySurfaceShape18.o" "polySoftEdge2.ip";
connectAttr "TreetopBig1Shape.wm" "polySoftEdge2.mp";
connectAttr "polyQuad1.out" "polySoftEdge3.ip";
connectAttr "TreetopBig2Shape.wm" "polySoftEdge3.mp";
connectAttr "polySurfaceShape19.o" "polySoftEdge4.ip";
connectAttr "TreetopSmall2Shape.wm" "polySoftEdge4.mp";
connectAttr "polySurfaceShape20.o" "polySoftEdge5.ip";
connectAttr "TreetopSmall3Shape.wm" "polySoftEdge5.mp";
connectAttr "polySurfaceShape21.o" "polySoftEdge6.ip";
connectAttr "BranchSmallTree1Shape.wm" "polySoftEdge6.mp";
connectAttr "polySurfaceShape22.o" "polySoftEdge7.ip";
connectAttr "BranchBigTreeShape.wm" "polySoftEdge7.mp";
connectAttr "polySurfaceShape23.o" "polySoftEdge8.ip";
connectAttr "BigTreeShape.wm" "polySoftEdge8.mp";
connectAttr "polySurfaceShape24.o" "polySoftEdge9.ip";
connectAttr "SmallTreeShape.wm" "polySoftEdge9.mp";
connectAttr "polySurfaceShape25.o" "polySoftEdge10.ip";
connectAttr "BranchSmallTreeShape.wm" "polySoftEdge10.mp";
connectAttr "polySurfaceShape26.o" "polySoftEdge11.ip";
connectAttr "SmallTree1Shape.wm" "polySoftEdge11.mp";
connectAttr "polySurfaceShape27.o" "polySoftEdge12.ip";
connectAttr "Roof1Shape.wm" "polySoftEdge12.mp";
connectAttr "polySurfaceShape28.o" "polySoftEdge13.ip";
connectAttr "Roof2Shape.wm" "polySoftEdge13.mp";
connectAttr "Roof8Shape.o" "polySeparate3.ip";
connectAttr "polySeparate3.out[2]" "polySoftEdge14.ip";
connectAttr "polySurfaceShape31.wm" "polySoftEdge14.mp";
connectAttr "polySeparate3.out[1]" "polySoftEdge15.ip";
connectAttr "polySurfaceShape30.wm" "polySoftEdge15.mp";
connectAttr "polySeparate3.out[0]" "polySoftEdge16.ip";
connectAttr "polySurfaceShape29.wm" "polySoftEdge16.mp";
connectAttr "polySeparate1.out[0]" "polySoftEdge17.ip";
connectAttr "polySurfaceShape10.wm" "polySoftEdge17.mp";
connectAttr "polyDelEdge29.out" "polySoftEdge18.ip";
connectAttr "millWheelShape.wm" "polySoftEdge18.mp";
connectAttr "polySurfaceShape32.o" "polySoftEdge19.ip";
connectAttr "Column5Shape.wm" "polySoftEdge19.mp";
connectAttr "polySurfaceShape33.o" "polySoftEdge20.ip";
connectAttr "Column3Shape.wm" "polySoftEdge20.mp";
connectAttr "polySurfaceShape34.o" "polySoftEdge21.ip";
connectAttr "Column6Shape.wm" "polySoftEdge21.mp";
connectAttr "verde2.out" "aiStandardSurface1SG.ss";
connectAttr "TreetopSmall1Shape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "TreetopSmall3Shape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "aiStandardSurface1SG.msg" "materialInfo1.sg";
connectAttr "verde2.msg" "materialInfo1.m";
connectAttr "verde2.msg" "materialInfo1.t" -na;
connectAttr "verde1.out" "aiStandardSurface2SG.ss";
connectAttr "TreetopBig2Shape.iog" "aiStandardSurface2SG.dsm" -na;
connectAttr "FloorShape.iog" "aiStandardSurface2SG.dsm" -na;
connectAttr "aiStandardSurface2SG.msg" "materialInfo2.sg";
connectAttr "verde1.msg" "materialInfo2.m";
connectAttr "verde1.msg" "materialInfo2.t" -na;
connectAttr "marron.out" "aiStandardSurface3SG.ss";
connectAttr "BigTreeShape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "BranchSmallTree1Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "BranchBigTreeShape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "Wood5Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "Wood6Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "Step1Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "aiStandardSurface3SG.msg" "materialInfo3.sg";
connectAttr "marron.msg" "materialInfo3.m";
connectAttr "marron.msg" "materialInfo3.t" -na;
connectAttr "aiStandardHair1.out" "aiStandardHair1SG.ss";
connectAttr "aiStandardHair1SG.msg" "materialInfo4.sg";
connectAttr "aiStandardHair1.msg" "materialInfo4.m";
connectAttr "aiStandardHair1.msg" "materialInfo4.t" -na;
connectAttr "aiStandardSurface4.out" "aiStandardSurface4SG.ss";
connectAttr "aiStandardSurface4SG.msg" "materialInfo5.sg";
connectAttr "aiStandardSurface4.msg" "materialInfo5.m";
connectAttr "aiStandardSurface4.msg" "materialInfo5.t" -na;
connectAttr "marron_claro.out" "aiStandardSurface5SG.ss";
connectAttr "SmallTreeShape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "BranchSmallTreeShape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "polySurfaceShape10.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "SmallTree1Shape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "polySurfaceShape15.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "Column2Shape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "Column1Shape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "WindowShape3.iog.og[2]" "aiStandardSurface5SG.dsm" -na;
connectAttr "WindowShape3.iog.og[1]" "aiStandardSurface5SG.dsm" -na;
connectAttr "WindowShape1.iog.og[1]" "aiStandardSurface5SG.dsm" -na;
connectAttr "Door1Shape.iog.og[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "Door1Shape.ciog.cog[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "DoorShape.iog.og[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "DoorShape.ciog.cog[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "groupId63.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId62.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId69.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId81.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId82.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId84.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId85.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "aiStandardSurface5SG.msg" "materialInfo6.sg";
connectAttr "marron_claro.msg" "materialInfo6.m";
connectAttr "marron_claro.msg" "materialInfo6.t" -na;
connectAttr "verde_claro.out" "aiStandardSurface6SG.ss";
connectAttr "TreetopSmall2Shape.iog" "aiStandardSurface6SG.dsm" -na;
connectAttr "TreetopBig1Shape.iog" "aiStandardSurface6SG.dsm" -na;
connectAttr "aiStandardSurface6SG.msg" "materialInfo7.sg";
connectAttr "verde_claro.msg" "materialInfo7.m";
connectAttr "verde_claro.msg" "materialInfo7.t" -na;
connectAttr "polySurfaceShape11.o" "polyUnite8.ip[0]";
connectAttr "polySurfaceShape12.o" "polyUnite8.ip[1]";
connectAttr "polySurfaceShape11.wm" "polyUnite8.im[0]";
connectAttr "polySurfaceShape12.wm" "polyUnite8.im[1]";
connectAttr "polySurfaceShape14.o" "polyUnite9.ip[0]";
connectAttr "polySurfaceShape13.o" "polyUnite9.ip[1]";
connectAttr "polySurfaceShape14.wm" "polyUnite9.im[0]";
connectAttr "polySurfaceShape13.wm" "polyUnite9.im[1]";
connectAttr "marronClaro.out" "aiStandardSurface7SG.ss";
connectAttr "millWheelShape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladder5Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladderShape7.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladderShape4.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladder6Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column5Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column4Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column3Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column6Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape8.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape8.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape9.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape9.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape3.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape3.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape2.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape2.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape1.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape1.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape5.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape5.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape4.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape4.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "polySurfaceShape36.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "polySurfaceShape37.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape6.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape6.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "tableShape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table4Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table3Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table2Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table1Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "groupId54.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId55.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId57.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId58.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId60.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId61.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId64.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId65.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId67.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId68.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId70.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId71.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId73.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId74.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId75.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId76.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId78.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId79.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "aiStandardSurface7SG.msg" "materialInfo8.sg";
connectAttr "marronClaro.msg" "materialInfo8.m";
connectAttr "marronClaro.msg" "materialInfo8.t" -na;
connectAttr "MarronMuyClaro.out" "aiStandardSurface8SG.ss";
connectAttr "polySurfaceShape29.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "pawsShape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "paws2Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "MillWheel2Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "FloorShape2.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "polySurfaceShape31.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof1Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof2Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof3Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof4Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "aiStandardSurface8SG.msg" "materialInfo9.sg";
connectAttr "MarronMuyClaro.msg" "materialInfo9.m";
connectAttr "MarronMuyClaro.msg" "materialInfo9.t" -na;
connectAttr "Rojo.out" "aiStandardSurface9SG.ss";
connectAttr "polySurfaceShape30.iog" "aiStandardSurface9SG.dsm" -na;
connectAttr "House1Shape.iog.og[1]" "aiStandardSurface9SG.dsm" -na;
connectAttr "HouseRoofShape.iog.og[1]" "aiStandardSurface9SG.dsm" -na;
connectAttr "groupId49.msg" "aiStandardSurface9SG.gn" -na;
connectAttr "groupId52.msg" "aiStandardSurface9SG.gn" -na;
connectAttr "aiStandardSurface9SG.msg" "materialInfo10.sg";
connectAttr "Rojo.msg" "materialInfo10.m";
connectAttr "Rojo.msg" "materialInfo10.t" -na;
connectAttr "polySplit1.out" "groupParts18.ig";
connectAttr "groupId47.id" "groupParts18.gi";
connectAttr "groupParts18.og" "groupParts19.ig";
connectAttr "groupId49.id" "groupParts19.gi";
connectAttr "azulClaro.out" "aiStandardSurface10SG.ss";
connectAttr "polySurfaceShape16.iog" "aiStandardSurface10SG.dsm" -na;
connectAttr "aiStandardSurface10SG.msg" "materialInfo11.sg";
connectAttr "azulClaro.msg" "materialInfo11.m";
connectAttr "azulClaro.msg" "materialInfo11.t" -na;
connectAttr "aiShadowMatte1.out" "aiShadowMatte1SG.ss";
connectAttr "aiShadowMatte1SG.msg" "materialInfo12.sg";
connectAttr "aiShadowMatte1.msg" "materialInfo12.m";
connectAttr "aiShadowMatte1.msg" "materialInfo12.t" -na;
connectAttr "groupParts21.og" "groupParts22.ig";
connectAttr "groupId53.id" "groupParts22.gi";
connectAttr "groupParts20.og" "groupParts21.ig";
connectAttr "groupId52.id" "groupParts21.gi";
connectAttr "polySplit2.out" "groupParts20.ig";
connectAttr "groupId50.id" "groupParts20.gi";
connectAttr "aiStandardSurface11.out" "aiStandardSurface11SG.ss";
connectAttr "aiStandardSurface11SG.msg" "materialInfo13.sg";
connectAttr "aiStandardSurface11.msg" "materialInfo13.m";
connectAttr "aiStandardSurface11.msg" "materialInfo13.t" -na;
connectAttr "Column8Shape.o" "polySeparate2.ip";
connectAttr "polyUnite7.out" "groupParts15.ig";
connectAttr "groupId44.id" "groupParts15.gi";
connectAttr "Column7Shape.o" "polyUnite7.ip[0]";
connectAttr "Step3Shape.o" "polyUnite7.ip[1]";
connectAttr "Column7Shape.wm" "polyUnite7.im[0]";
connectAttr "Step3Shape.wm" "polyUnite7.im[1]";
connectAttr "amarillo.out" "aiStandardSurface12SG.ss";
connectAttr "WindowShape8.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape9.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape2.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape5.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "polySurfaceShape36.iog.og[2]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape6.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "lanternShape.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "groupId56.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId59.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId66.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId72.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId77.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId80.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId89.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "aiStandardSurface12SG.msg" "materialInfo14.sg";
connectAttr "amarillo.msg" "materialInfo14.m";
connectAttr "amarillo.msg" "materialInfo14.t" -na;
connectAttr "polyExtrudeFace8.out" "groupParts23.ig";
connectAttr "groupId64.id" "groupParts23.gi";
connectAttr "groupParts23.og" "groupParts24.ig";
connectAttr "groupId66.id" "groupParts24.gi";
connectAttr "polyExtrudeFace6.out" "groupParts25.ig";
connectAttr "groupId67.id" "groupParts25.gi";
connectAttr "groupParts25.og" "groupParts26.ig";
connectAttr "groupId69.id" "groupParts26.gi";
connectAttr "polySurfaceShape35.o" "polyChipOff1.ip";
connectAttr "WindowShape4.wm" "polyChipOff1.mp";
connectAttr "WindowShape4.o" "polySeparate4.ip";
connectAttr "polyChipOff1.out" "groupParts27.ig";
connectAttr "groupId73.id" "groupParts27.gi";
connectAttr "polySeparate4.out[0]" "groupParts28.ig";
connectAttr "groupId75.id" "groupParts28.gi";
connectAttr "polySeparate4.out[1]" "groupParts29.ig";
connectAttr "groupId76.id" "groupParts29.gi";
connectAttr "groupParts28.og" "groupParts30.ig";
connectAttr "groupId77.id" "groupParts30.gi";
connectAttr "negro.out" "aiStandardSurface13SG.ss";
connectAttr "groupId83.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "groupId86.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "groupId87.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "groupId88.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "Door1Shape.iog.og[1]" "aiStandardSurface13SG.dsm" -na;
connectAttr "DoorShape.iog.og[1]" "aiStandardSurface13SG.dsm" -na;
connectAttr "lanternShape.iog.og[0]" "aiStandardSurface13SG.dsm" -na;
connectAttr "lanternShape.ciog.cog[0]" "aiStandardSurface13SG.dsm" -na;
connectAttr "aiStandardSurface13SG.msg" "materialInfo15.sg";
connectAttr "negro.msg" "materialInfo15.m";
connectAttr "negro.msg" "materialInfo15.t" -na;
connectAttr "amarilloClaro.out" "aiStandardSurface14SG.ss";
connectAttr "CoverShape1.iog" "aiStandardSurface14SG.dsm" -na;
connectAttr "CoverShape2.iog" "aiStandardSurface14SG.dsm" -na;
connectAttr "aiStandardSurface14SG.msg" "materialInfo16.sg";
connectAttr "amarilloClaro.msg" "materialInfo16.m";
connectAttr "amarilloClaro.msg" "materialInfo16.t" -na;
connectAttr "polyExtrudeFace10.out" "groupParts31.ig";
connectAttr "groupId87.id" "groupParts31.gi";
connectAttr "groupParts31.og" "groupParts32.ig";
connectAttr "groupId89.id" "groupParts32.gi";
connectAttr "morado.out" "aiStandardSurface15SG.ss";
connectAttr "groupId53.msg" "aiStandardSurface15SG.gn" -na;
connectAttr "HouseRoofShape.iog.og[2]" "aiStandardSurface15SG.dsm" -na;
connectAttr "aiStandardSurface15SG.msg" "materialInfo17.sg";
connectAttr "morado.msg" "materialInfo17.m";
connectAttr "morado.msg" "materialInfo17.t" -na;
connectAttr "groupParts19.og" "groupParts33.ig";
connectAttr "groupId90.id" "groupParts33.gi";
connectAttr "aiStandardSurface16.out" "aiStandardSurface16SG.ss";
connectAttr "aiStandardSurface16SG.msg" "materialInfo18.sg";
connectAttr "aiStandardSurface16.msg" "materialInfo18.m";
connectAttr "aiStandardSurface16.msg" "materialInfo18.t" -na;
connectAttr "groupParts33.og" "groupParts34.ig";
connectAttr "groupId91.id" "groupParts34.gi";
connectAttr "aiStandardSurface17.out" "aiStandardSurface17SG.ss";
connectAttr "aiStandardSurface17SG.msg" "materialInfo19.sg";
connectAttr "aiStandardSurface17.msg" "materialInfo19.m";
connectAttr "aiStandardSurface17.msg" "materialInfo19.t" -na;
connectAttr "moradoClaro.out" "aiStandardSurface18SG.ss";
connectAttr "House1Shape.iog.og[4]" "aiStandardSurface18SG.dsm" -na;
connectAttr "House1Shape.iog.og[2]" "aiStandardSurface18SG.dsm" -na;
connectAttr "House1Shape.iog.og[3]" "aiStandardSurface18SG.dsm" -na;
connectAttr "groupId92.msg" "aiStandardSurface18SG.gn" -na;
connectAttr "groupId90.msg" "aiStandardSurface18SG.gn" -na;
connectAttr "groupId91.msg" "aiStandardSurface18SG.gn" -na;
connectAttr "aiStandardSurface18SG.msg" "materialInfo20.sg";
connectAttr "moradoClaro.msg" "materialInfo20.m";
connectAttr "moradoClaro.msg" "materialInfo20.t" -na;
connectAttr "groupParts34.og" "groupParts35.ig";
connectAttr "groupId92.id" "groupParts35.gi";
connectAttr "gris.out" "aiStandardSurface19SG.ss";
connectAttr "StepShape.iog" "aiStandardSurface19SG.dsm" -na;
connectAttr "Step2Shape.iog" "aiStandardSurface19SG.dsm" -na;
connectAttr "aiStandardSurface19SG.msg" "materialInfo21.sg";
connectAttr "gris.msg" "materialInfo21.m";
connectAttr "gris.msg" "materialInfo21.t" -na;
connectAttr "polyTweak51.out" "polyAutoProj1.ip";
connectAttr "FloorShape.wm" "polyAutoProj1.mp";
connectAttr "polyCube4.out" "polyTweak51.ip";
connectAttr "polyAutoProj1.out" "polyTweakUV41.ip";
connectAttr "file1.oc" "aiStandardSurface20.base_color";
connectAttr "aiStandardSurface20.out" "aiStandardSurface20SG.ss";
connectAttr "aiStandardSurface20SG.msg" "materialInfo22.sg";
connectAttr "aiStandardSurface20.msg" "materialInfo22.m";
connectAttr "file1.msg" "materialInfo22.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "aiStandardSurface21.out" "aiStandardSurface21SG.ss";
connectAttr "aiStandardSurface21SG.msg" "materialInfo23.sg";
connectAttr "aiStandardSurface21.msg" "materialInfo23.m";
connectAttr "aiStandardSurface21.msg" "materialInfo23.t" -na;
connectAttr "aiStandardSurface1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface2SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface3SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardHair1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface4SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface5SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface6SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface7SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface8SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface9SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface10SG.pa" ":renderPartition.st" -na;
connectAttr "aiShadowMatte1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface11SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface12SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface13SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface14SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface15SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface16SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface17SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface18SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface19SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface20SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface21SG.pa" ":renderPartition.st" -na;
connectAttr "verde2.msg" ":defaultShaderList1.s" -na;
connectAttr "verde1.msg" ":defaultShaderList1.s" -na;
connectAttr "marron.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardHair1.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface4.msg" ":defaultShaderList1.s" -na;
connectAttr "marron_claro.msg" ":defaultShaderList1.s" -na;
connectAttr "verde_claro.msg" ":defaultShaderList1.s" -na;
connectAttr "marronClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "MarronMuyClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "Rojo.msg" ":defaultShaderList1.s" -na;
connectAttr "azulClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "aiShadowMatte1.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface11.msg" ":defaultShaderList1.s" -na;
connectAttr "amarillo.msg" ":defaultShaderList1.s" -na;
connectAttr "negro.msg" ":defaultShaderList1.s" -na;
connectAttr "amarilloClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "morado.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface16.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface17.msg" ":defaultShaderList1.s" -na;
connectAttr "moradoClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "gris.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface20.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface21.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "HumanShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "Roof6Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "Roof6Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Roof7Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "Roof7Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Roof8Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "millwheel1Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "millwheel1Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "millwheel3Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "millwheel3Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder6Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape3.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape3.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood1Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood1Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood2Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood2Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood7Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood3Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood3Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood4Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood4Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Wood8Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "TrunkShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "TrunkShape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Trunk1Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape11.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape12.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape13.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape14.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Column7Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Column7Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Step3Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Step3Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Column8Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "House1Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "House1Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "HouseRoofShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "HouseRoofShape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId11.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId12.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId13.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId14.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId15.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId16.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId17.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId18.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId19.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId20.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId22.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId23.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId24.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId25.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId26.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId27.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId28.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId29.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId30.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId31.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId32.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId33.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId34.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId36.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId37.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId38.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId39.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId40.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId41.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId42.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId43.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId44.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId47.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId48.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId50.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId51.msg" ":initialShadingGroup.gn" -na;
// End of actividad.ma
